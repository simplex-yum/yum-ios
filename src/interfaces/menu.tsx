export interface HomeState {
    group_id: any,
    width:any,
    Address: any, searchText: any
}
export interface HomeProps {
    navigation:any,
    combos: any[],
    topDeals: any[],
    heroItems: any[],
    groups: any[],
    cart: any[],
    deliveryaddress:any,
    selectedStore: any,
    orderperiod: any,
    loading: any,
    notifications: any[],
    NotificationList: () => {};
    menuCombosList: () => {},
    menuTopDeals: () => {},
    allGroupsList: () => {},
    menuHeroItems: () => {},
    homeGroupsList: () => {},
    favouriteAdd: (param1: any, param2: any) => {},
}
export interface GroupsProps {
    navigation:any,
    route:any,
    groups: any[],
    groupsItems: any[],
    orderperiod: any,
    loading: any,
    cart: any[],
    saveCart: (cart: any[]) => {};
   allGroupsList:()=>{};
    getCart: () => {};
    favouriteAdd: (param1: any) => {},
}
export interface favouriteAdd {
    data: any,
}