export interface SigninState {
    [x: number]: any,
    email: string,
    password: any,
    loading: any,
    visible: any
}
export interface SigninProps {
    navigation:any,
    loginCustomer: (email: any, password: any,navigation:any) => {}
    loginCustomerWithSocial: (data: any,navigation:any) => {}
}
export interface SignupState {
    firstname: string;
    lastname: string;
    phone: any,
    email: string;
    isValidEmail: any,
    address: any,
    password: any;
    agreecheck: any;
    region: any,
    coordinate: any
}
export interface SignupProps {
    navigation:any,
    saveDeliveryAddress: (address: any) => {}
    signupCustomer: (data: any,guestdata:any,navigation:any) => {}
}
export interface ForgotPassState {
    [x: number]: any,
    email: string,
    isModalVisible: any,
}
export interface ForgotPassProp {
    navigation:any,
    isReset: any,
    resetPassword: (email: any) => {}
}
export interface FavState {
    loading: any,
    isLoggedIn: any
}
export interface FavProps {
    navigation:any,
    isLoggedIn: any,
    favItems: any[],
    favoritesList: () => {},
    deletefavourites: (favItems: any, id: any) => {}
}
export interface OrderProps {
    navigation:any,
    orders: any[],
    orderDetail: (order_id: any,navigation:any) => {},
    ordersList: () => {},
    loading: any
}
export interface OrderDetailProps {
    navigation:any,
    loading: any,
    cart: any[],
    stores: any[],
    taxdata: any,
    selectedStore: any,
    getStoreById: (store_id: any) => {},
    saveDeliveryFee: (deliverfee: any) => {},
    saveOrderType: (type: any) => {},
    orderData: any,
    getCart: () => {}, saveCart: (cart: any) => {}
    getTax: (state: any) => {}
}
export interface ProfileState {

}
export interface ProfileProps {
    navigation:any,
    userInfo: any,
    name: any,
    email_address: any,
    phone_number: any,
    profile_pic: any,
    fileUri: any,
    loading: any,
    fcmStatus: any,
    isLoggedIn: any,
    addressesList: () => {},
    ordersList: (navigation:any) => {},
    setloginCustomer: () => {},
    UpdateFCMDeviceStatus: (FCM: any) => {},
    logoutCustomer: (navigation:any) => {}
}
export interface EditProfileState {
    firstname: any,
    lastname: any,
    phone: any,
    dob: any,
    gender: any,
    email: any,
    loading: any,
    newPass: any,
    confirmPass: any,
    changePassMsg: any,
    profile_pic: any,
    fileUri: any
}
export interface EditProfileProps {
    navigation:any,
    userInfo: any,
    getcustomerProfile: () => {},
    changePassword: (newPass: any,navigation:any) => {},
    editProfile: (data: any,navigation:any) => {},
    uploadAvatar: (fileData: any) => {}
}
export interface ManageAddressState {
    address_id: any,
    address: any,
    extraDetails: any,
    landmark: any,
    is_default: any,
    saveAs: any,
    loading: any,
}
export interface ManageAddressProps {
    navigation:any,
    addresses: any,
    addressesList: () => {},
    addAddress: (data: any,navigation:any) => {},
    editAddress: (id: any, data: any) => {},
    setDefaultAddress: (data: any) => {},
    deleteAddress: (addresses:any,id: any) => {}
}
export interface AddAddressState {
    lat: any, lng: any,
    address: any,
    extraDetails: any,
    landmark: any,
    is_default: any,
    saveAs: any,
    loading: any,
    region: any,
    coordinate: any
}
export interface AddAddressProps {
    navigation:any,
    currentlat: any,
    currentlng: any,
    addresses: any[],
    addAddress: (data: any,loginStatus:any,navigation:any) => {},
}
export interface EditAddressState {
    address: any,
    extraDetails: any,
    landmark: any,
    is_default: any,
    region: any,
    coordinate: any,
    saveAs: any,
    loading: any,
}
export interface EditAddressProps {
    navigation:any,
    currentlat: any,
    currentlng: any,
    route: any,
    addresses: any,
    editAddress: (id: any, data: any,navigation:any) => {},
    deleteAddress: (id: any) => {}
}
export interface PaymentMethodState {
    loading: any,
}
export interface PaymentMethodProps {
    navigation:any,
    cards: any[],
    paymentCardsList: () => {},
}
export interface PaymentProps {
    navigation:any,
    route: any,
    order_id: any,
    orderperiod: any,
    datetime: any,
    orderType: any,
    selectedPaymentMethod: any,
    orderInfo: any,
    loading: any,
    saveSelectPaymentMethod: (method: any) => {},
    paymentProcessByCard: (data: any, order_id: any, orderperiod: any, orderInfo: any, date: any, time: any) => {},
    paymentProcess: (data: any,navigation:any) => {}
    laterPaymentProcess: (data: any,navigation:any) => {}
}
export interface AddcardState {
    cardHolder: any,
    cardNumber: any,
    cardExpiry: any,
    cvc: any,
    loading: any,
}
export interface AddcardProps {
    addCard: (data: any) => {},
}
export interface OfferProps {
    navigation:any,
    loading: any,
    offers: any[];
    offersList: () => {};
}
export interface AddGuestState {
    firstname: string;
    lastname: string;
    phone: any,
    email: string;
    isValidEmail: any,
    agreecheck: any,
    loading: any;
}
