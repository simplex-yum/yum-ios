import 'react-native-gesture-handler';
import React from 'react';
// import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import store from '../redux/store';
import Signin from '../pages/signin';
import Signup from '../pages/signup';
// import ForgotPass from '../pages/forgotPass';
import Home from '../pages/home';
import ItemDetail from '../pages/item-detail';
import Favourite from '../pages/favourites';
import Cart from '../pages/cart';
import Order from '../pages/orders';
import OrderDetail from '../pages/order-detail';
import Profile from '../pages/profile';
// import EditProfile from '../pages/edit-profile';
// import addressManager from '../pages/manageAddress';
import help from '../pages/help';
// import Payment from '../pages/payment';
// import Offers from '../pages/offers';
// import SelectStore from '../pages/select-store';
// import ConfirmLocation from '../pages/confirmlocation'
import Review from '../pages/review'
import Groups from '../pages/groups';
// import guestDetail from '../pages/guest-detail';
// import addAddress from '../pages/addAddress';
// import editAddress from '../pages/editAddress';
// import PushController from './PushController';
// import Toast from 'react-native-toast-message';
// import pushNotification from '../pages/pushNotification';
import { Root } from 'native-base';
const Stack = createStackNavigator();
// let tracker = new GoogleAnalyticsTracker("UA-185886264-1");
//   for Scene key="root"
// title="Sign In" => title is used for title of default header
//For Toast to work, you need to wrap your topmost component inside <Root> from native-base
// tracker.trackScreenView("home");
// tracker.trackScreenView("groups");
// tracker.trackScreenView("itemdetail");
// tracker.trackScreenView("signin");
// tracker.trackScreenView("signup");
// tracker.trackScreenView("forgotpass");
// tracker.trackScreenView("favourites");
// tracker.trackScreenView("cart");
// tracker.trackScreenView("guestDetail");
// tracker.trackScreenView("order");
// tracker.trackScreenView("orderdetail");
// tracker.trackScreenView("profile");
// tracker.trackScreenView("editprofile");
// tracker.trackScreenView("address");
// tracker.trackScreenView("addaddress");
// tracker.trackScreenView("editaddress");
// tracker.trackScreenView("payment");
// tracker.trackScreenView("offers");
// tracker.trackScreenView("selectstore");
// tracker.trackScreenView("confirmlocation");
// tracker.trackScreenView("review")
// tracker.trackScreenView("notification")

const Routes = () => (
  <NavigationContainer >
    <Provider store={store}>
      <Root>
        <Stack.Navigator screenOptions={{
          headerShown: false
        }} initialRouteName="home">
         <Stack.Screen name="home" component={Home} />
         <Stack.Screen name="itemdetail" component={ItemDetail} />
         <Stack.Screen name="favourites" component={Favourite} />
         <Stack.Screen name="profile" component={Profile} />
         <Stack.Screen name="help" component={help} />
         <Stack.Screen name="order" component={Order} />
         <Stack.Screen name="orderdetail" component={OrderDetail} />
         <Stack.Screen name="review" component={Review} />
         <Stack.Screen name="groups" component={Groups} />
         <Stack.Screen name="cart" component={Cart} />
           {/* 
          <Stack.Screen name="notification" component={pushNotification} />
          <Stack.Screen name="confirmlocation" component={ConfirmLocation} />
          
          
          <Stack.Screen name="editprofile" component={EditProfile} />
          <Stack.Screen name="address" component={addressManager} />
          <Stack.Screen name="addaddress" component={addAddress} />
          <Stack.Screen name="editaddress" component={editAddress} />
          <Stack.Screen name="payment" component={Payment} />
          
          <Stack.Screen name="guestDetail" component={guestDetail} />
          <Stack.Screen name="offers" component={Offers} />
          <Stack.Screen name="selectstore" component={SelectStore} />
          
          
          <Stack.Screen name="signin" component={Signin} />*/}
          <Stack.Screen name="signup" component={Signup} />
          {/*<Stack.Screen name="forgotpass" component={ForgotPass} />
          <Stack.Screen name="push" component={PushController} /> */}
          <Stack.Screen name="signin" component={Signin} />
        </Stack.Navigator>
      </Root>
      {/* <PushController /> */}
      {/* <Toast ref={(ref) => Toast.setRef(ref)} /> */}
    </Provider>
  </NavigationContainer >
);

export default Routes;
