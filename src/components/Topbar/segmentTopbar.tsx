import React from 'react';
import {Header, Left, Right, Button, Icon, Body, Title} from 'native-base';
import styles from '../../assets/css/header';
import {connect} from 'react-redux';
interface topbarProps {
  navigation?:any
  title?: any;
}
class TopBar extends React.PureComponent<topbarProps, {}> {
  constructor(props: any) {
    super(props);
  }
  render() {
    const {title,navigation} = this.props;
    return (
      <Header transparent hasSegment androidStatusBarColor="#bc2c3d">
        <Left
          // style={{
          //   flex: 1,
          // }}
          >
          <Button transparent onPress={() => navigation.goBack()}>
            <Icon name="arrow-back" style={styles.headerIcon} />
          </Button>
        </Left>
        <Body
          // style={{
          //   flex: 2,
          // }}
          >
          <Title style={styles.headerTitle}>{title}</Title>
        </Body>
        <Right
          // style={{
          //   flex: 1,
          // }}
          ></Right>
      </Header>
    );
  }
}
export default TopBar;
