import React from 'react';
import {Header, Left, Right, Button, Icon, Body, Title} from 'native-base';
import styles from '../../assets/css/header';

const TopBar = (props: any) => {
  return (
    <Header
      style={styles.headerStyle}
      transparent
      androidStatusBarColor="#bc2c3d">
      <Left>
        <Button transparent onPress={() => props.navigation.goBack()}>
          <Icon name="arrow-back" style={{color: 'black'}} />
        </Button>
      </Left>
      <Body>
        <Title style={styles.headerTitle}>{props.title}</Title>
      </Body>
      <Right/>
    </Header>
  );
};

export default TopBar;
