import React from 'react';
import {
  Header,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Title,
  Badge,
  Text,
} from 'native-base';
import styles from '../../assets/css/header';
import { connect } from 'react-redux';
import { getCart, NotificationList } from '../../redux';
interface topbarProps {
  title: any;
  tabs?: any;
  cart: any[];
  notifications: any[],
  navigation?: any,
  getCart: () => {};
  NotificationList: () => {};
}
class TopBar extends React.PureComponent<topbarProps, {}> {
  constructor(props: any) {
    super(props);
    this.isEmpty = this.isEmpty.bind(this);
    this.notification = this.notification.bind(this);
    this.renderCartButton = this.renderCartButton.bind(this);
    this.renderScreen = this.renderScreen.bind(this);
    this.refreshPage = this.refreshPage.bind(this)
  }
  componentDidMount() {
    this.props.getCart();
    this.props.NotificationList();
  }
  renderScreen(screen: any) {
    this.props.navigation.navigate(screen);
  }
  isEmpty(obj: any) {
    if (obj === null) {
      return true;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
      return false;
    }
    if (obj && Object.keys(obj).length === 0) {
      return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  renderCartButton(data: any) {
    if (!this.isEmpty(data)) {
      let cart = JSON.parse(data);
      let totalItemQuantity = 0;
      cart.map((item:any) => {
        totalItemQuantity += item.quantity; 
      })
      if (cart.length > 0) {
        return (
          <Button
            transparent
            style={{ marginHorizontal: 15 }}
            onPress={() => {
              this.renderScreen("cart")
            }}>
            <Icon
              type="FontAwesome"
              name="shopping-cart"
              style={{ color: 'black' }}
            />
            <Badge
              style={{
                marginLeft: 30,
                width: 35,
                height: 23,
                position: 'absolute',
              }}>
              <Text style={{ color: '#fff' }}>{totalItemQuantity > 9 ? `${9}+` : totalItemQuantity}</Text>
            </Badge>
          </Button>
        );
      } else {
        return (
          <Button
            transparent
            style={{ marginHorizontal: 15 }}
            onPress={() => {
              this.renderScreen("cart")
            }}>
            <Icon
              type="FontAwesome"
              name="shopping-cart"
              style={{ color: 'black' }}
            />
          </Button>
        );
      }
    } else {
      return (
        <Button
          transparent
          style={{ marginHorizontal: 15 }}
          onPress={() => {
            this.renderScreen("cart")
          }}>
          <Icon
            type="FontAwesome"
            name="shopping-cart"
            style={{ color: 'black' }}
          />
        </Button>
      );
    }
  };
  notification() {
    const { cart, notifications } = this.props;
    return (
      <Right>
        {this.renderCartButton(cart)}
        <Button transparent style={{ marginRight: 10 }} onPress={() => { this.renderScreen("notification") }}>
          <Icon name="bell-o" type="FontAwesome" style={styles.headerIcon} />
          {notifications.length > 0 && <Badge
            style={{
              marginLeft: 30,
              width: notifications.length + 25,
              height: 23,
              position: 'absolute',
            }}>
            <Text style={{ color: '#fff' }}>{notifications.length}</Text>
          </Badge>}
        </Button>
      </Right>
    );
  };
  refreshPage() {
    this.props.navigation.goBack();
  }
  render() {
    const { title, tabs } = this.props;
    return (
      <Header
        {...(tabs && { hasTabs: true })}
        style={styles.headerStyle}
        transparent
        androidStatusBarColor="#bc2c3d">
        <Left>
          <Button transparent onPress={this.refreshPage}>
            <Icon name="arrow-back" style={styles.headerIcon} />
          </Button>
        </Left>
        <Body>
          <Title style={styles.headerTitle}>{title}</Title>
        </Body>
        {this.notification()}
      </Header>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    cart: state.cart.cartData,
    notifications: state.customer.notificationList
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    getCart: function () {
      dispatch(getCart());
    },
    NotificationList: function () {
      dispatch(NotificationList());
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TopBar);
