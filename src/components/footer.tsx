import React, { Component } from 'react';
import { View } from 'react-native';
import styles from '../assets/css/footer';
import {
  Button,
  Text,
  Icon,
  Footer,
  FooterTab,
  StyleProvider
} from 'native-base';
// import getTheme from '../../native-base-theme/components';
// import platform from '../../native-base-theme/variables/platform';
import { ordersList } from '../redux';
import { connect } from 'react-redux';
class footer extends Component<{ active: any; ordersList: () => {},navigation?:any }, {}> {
  constructor(readonly props: any) {
    super(props);
    this.ordersPage = this.ordersPage.bind(this);
    this.renderScreen = this.renderScreen.bind(this);
  }
  renderScreen(screen:any){
    this.props.navigation.push(screen);
  }
  ordersPage() {
    this.props.ordersList(this.props.navigation)
  }
  render() {
    let { active } = this.props;
    return (
    //   <StyleProvider style={getTheme(platform)}>
        <View>
          <Footer>
            <FooterTab style={styles.footerStyle}>
              <Button
                style={styles.footerButtons}
                vertical
                active={active == 'home' ? true : false}
                onPress={()=>this.renderScreen('home')}>
                <Icon
                  type="FontAwesome"
                  name="compass"
                />
                <Text style={{ fontSize: 8, textTransform: 'capitalize' }}>
                  Explore
                </Text>
              </Button>
              <Button
                style={styles.footerButtons}
                vertical
                active={active == 'favourites' ? true : false}
                onPress={()=>this.renderScreen('favourites')}>
                <Icon
                  type="FontAwesome"
                  name="heart-o"
                />
                <Text style={{ fontSize: 8, textTransform: 'capitalize' }}>
                  Favourite
                </Text>
              </Button>
              <Button
                style={styles.footerButtons}
                vertical
                active={active == 'cart' ? true : false}
                onPress={()=>this.renderScreen('cart')}>
                <Icon
                  type="FontAwesome"
                  name="shopping-cart"
                />
                <Text style={{ fontSize: 8, textTransform: 'capitalize' }}>
                  Cart
                </Text>
              </Button>
              <Button
                style={styles.footerButtons}
                vertical
                active={active == 'profile' ? true : false}
                onPress={()=>this.renderScreen('profile')}>
                <Icon
                  type="FontAwesome"
                  name="user-o"
                />
                <Text style={{ fontSize: 8, textTransform: 'capitalize' }}>
                  Profile
                </Text>
              </Button>
              <Button
                style={styles.footerButtons}
                vertical
                active={active == 'orders' ? true : false}
                onPress={this.ordersPage}>
                <Icon
                  type="FontAwesome"
                  name="server"
                />
                <Text style={{ fontSize: 8, textTransform: 'capitalize' }}>
                  Orders
                </Text>
              </Button>
            </FooterTab>
          </Footer>
        </View>
    //   </StyleProvider>
    );
  }
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    ordersList: function (navigation:any) {
      dispatch(ordersList(navigation));
    }
  };
};
export default connect(null, mapDispatchToProps)(footer);
