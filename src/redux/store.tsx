import {createStore, applyMiddleware, combineReducers} from 'redux';
import menuReducer from './reducers/menuReducer';
import cartReducer from './reducers/cartReducer';
import customerReducer from './reducers/customerReducer';
const thunkMiddleware = require('redux-thunk').default;

const mainReducer = combineReducers({
  customer: customerReducer,
  cart: cartReducer,
  menu: menuReducer,
});
const store = createStore(mainReducer, applyMiddleware(thunkMiddleware));

export default store;
