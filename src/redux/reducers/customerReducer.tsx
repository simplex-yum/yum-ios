import {
  LOGINOUT_CUSTOMER,
  RESET_PASS,
  FAV_MENU_ITEM,
  ADDRESS_LIST,
  PAYMENT_CARDS_LIST,
  GET_PROFILE,
  ORDER_LIST,
  ORDER_DETAIL,
  ORDER_TYPE,
  SELECT_STORE,
  STORES_LIST,
  DELIVERY_FEE,
  ORDER_PERIOD,
  ORDER_DATE_TIME,
  DELIVERY_ADDRESS,
  SELECT_STORE_ID,
  SELECT_PAYMENT_METHOD,
  ADD_GUEST,
  SAVE_AS_PLACE,
  OFFERS_LIST,
  PROFILE_ADDRESS,
  NOTIFICATIONS,
  UPDATE_FCM_STATUS
} from '../actions/customerType';

const initialState: any = {
  userInfo: {},
  name: "",
  email_address: "",
  phone_number: "",
  profile_pic: "",
  fileUri: "",
  profileLoad: true,
  guestInfo: {},
  favItemData: [] || '',
  addressData: [] || '',
  ordersData: [] || '',
  cardsData: [] || '',
  storesList: [] || '',
  offers: [] || '',
  notificationList: [] || '',
  orderDetail: {},
  datetime: {},
  saveAs: "Home",
  orderType: '',
  orderperiod: 'now',
  selectedStore: {},
  selectStoreId: undefined,
  loading: true,
  orderDetailloading: true,
  deliveryfee: '',
  deliveryaddress: '',
  profileaddress: '',
  storeStatus: 0,
  selectedPaymentMethod: "",
  fcmStatus: "",
  isLoggedIn: false,
  isReset: '',
};

const customerReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case LOGINOUT_CUSTOMER:
      return {
        ...state,
        name: action.name,
        email_address: action.email_address,
        phone_number: action.phone_number,
        profile_pic: action.profile_pic,
        fileUri: action.fileUri,
        profileLoad: action.loading,
        isLoggedIn: action.isLoggedIn
      };
    case RESET_PASS:
      return {
        ...state,
        isReset: action.isReset,
      };
    case FAV_MENU_ITEM:
      return {
        ...state,
        favItemData: action.payload,
      };
    case ADDRESS_LIST:
      return {
        ...state,
        addressData: action.payload,
      };
    case ORDER_LIST:
      return {
        ...state,
        ordersData: action.payload,
        loading: action.loading,
      };
    case ORDER_DETAIL:
      return {
        ...state,
        orderDetail: action.payload,
        orderDetailloading: action.loading
      };
    case PAYMENT_CARDS_LIST:
      return {
        ...state,
        cardsData: action.payload,
      };
    case GET_PROFILE:
      return {
        ...state,
        userInfo: action.payload,
      };
    case ADD_GUEST:
      return {
        ...state,
        guestInfo: action.payload,
      };
    case ORDER_TYPE:
      return {
        ...state,
        orderType: action.orderType,
      };
    case SAVE_AS_PLACE:
      return {
        ...state,
        saveAs: action.saveAs,
      };
    case SELECT_STORE:
      return {
        ...state,
        selectedStore: action.selectedStore,
      };
    case SELECT_STORE_ID:
      return {
        ...state,
        selectStoreId: action.selectStoreId,
      };
    case DELIVERY_FEE:
      return {
        ...state,
        deliveryfee: action.deliveryfee,
      };
    case DELIVERY_ADDRESS:
      return {
        ...state,
        deliveryaddress: action.deliveryaddress,
      };
    case PROFILE_ADDRESS:
      return {
        ...state,
        profileaddress: action.profileaddress,
      };
    case ORDER_PERIOD:
      return {
        ...state,
        orderperiod: action.orderperiod,
      };
    case ORDER_DATE_TIME:
      return {
        ...state,
        datetime: action.datetime,
      };
    case STORES_LIST:
      return {
        ...state,
        deliveryfee: action.deliveryfee,
        storesList: action.payload,
        selectStoreId: action.selectStoreId,
        storeStatus: action.storeStatus,
        loading: action.loading
      };
    case SELECT_PAYMENT_METHOD:
      return {
        ...state,
        selectedPaymentMethod: action.selectedPaymentMethod,
      };
    case OFFERS_LIST:
      return {
        ...state,
        offers: action.payload,
        loading: action.loading
      }
    case NOTIFICATIONS:
      return {
        ...state,
        notificationList: action.payload,
        loading: action.loading
      }
    case UPDATE_FCM_STATUS:
      return {
        ...state,
        fcmStatus: action.fcmStatus
      }
    default:
      return state;
  }
};
export default customerReducer;
