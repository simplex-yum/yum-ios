import {
  MENU_COMBO_LIST,
  COMBO_DETAIL,
  ITEM_DETAIL,
  ALL_GROUPS_LIST,
  MENU_ITEMS,
  ITEM_LIST,
  DETAIL_COUNT,
  HANDLE_MODIFIERS,
  HOME_GROUPS_LIST,
  HANDLE_SIZE,
  UPDATE_MODIFIERS,
  MENU_TOP_DEALS,
  HERO_ITEMS
} from '../actions/menuType';

const initialState: any = {
  menuCombosData: [] || '',
  topDeals: [] || '',
  heroItems: [] || '',
  comboData: {},
  homeGroupsData: [] || '',
  allGroups: [] || '',
  allGroupsData: [] || '',
  menuItemsData: [] || '',
  itemsList: [] || '',
  itemData: {},
  countData: {},
  itemPrice: '',
  actualPrice: '',
  modifiers: {},
  selectedsize: {},
  sizearray: [] || '',
  loading: true
};

const menuReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case MENU_COMBO_LIST:
      return {
        ...state,
        menuCombosData: action.payload,
        loading: action.loading
      };
    case MENU_TOP_DEALS:
      return {
        ...state,
        topDeals: action.payload,
        loading: action.loading
      };
    case HERO_ITEMS:
      return {
        ...state,
        heroItems: action.payload,
        loading: action.loading
      };
    case COMBO_DETAIL:
      return {
        ...state,
        comboData: action.payload,
        itemPrice: action.itemPrice,
        actualPrice: action.actualPrice,
        modifiers: action.modifiers
      };
    case HANDLE_MODIFIERS:
      return {
        ...state,
        itemPrice: action.itemPrice,
        actualPrice: action.actualPrice,
        modifiers: action.modifiers
      };
    case UPDATE_MODIFIERS:
      return {
        ...state,
        modifiers: action.modifiers
      }
    case HOME_GROUPS_LIST:
      return {
        ...state,
        homeGroupsData: action.payload,
      };
    case ALL_GROUPS_LIST:
      return {
        ...state,
        allGroups: action.groups,
        allGroupsData: action.groupsData
      };
    case ITEM_DETAIL:
      return {
        ...state,
        itemData: action.payload,
        sizearray: action.sizearray,
        selectedsize: action.selectedsize,
        itemPrice: action.itemPrice,
        actualPrice: action.actualPrice,
        modifiers: action.modifiers
      };
    case HANDLE_SIZE:
      return {
        ...state,
        selectedsize: action.selectedsize,
        itemPrice: action.itemPrice,
        actualPrice: action.actualPrice
      };
    case MENU_ITEMS:
      return {
        ...state,
        menuItemsData: action.payload,
        loading: action.loading
      };
    case ITEM_LIST:
      return {
        ...state,
        itemsList: action.payload,
      };
    case DETAIL_COUNT:
      return {
        ...state,
        countData: action.payload,
      };
    default:
      return state;
  }
};
export default menuReducer;
