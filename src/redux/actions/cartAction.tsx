import {
  GET_CART,
  CLEAR_CART,
  SAVE_CART,
  COUPON_DATA,
  GET_TAX,
  GET_INST,
} from './cartType';
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../../components/Api';
import { Toast } from 'native-base';

export const getCart = () => {
  return async (dispatch: any) => {
    let cart: any = await AsyncStorage.getItem('cart');
    if (cart === null || cart.length <= 0) {
      cart = [];
    }
    dispatch({
      type: GET_CART,
      payload: cart,
      loader: false,
    });
  };
};
export const clearCart = () => {
  return async (dispatch: any) => {
    AsyncStorage.removeItem('cart');
    // dispatch({
    //   type: CLEAR_CART,
    //   success: true,
    // });
  };
};
export const saveCart = (cart: any) => {
  //type of cart is array
  return async (dispatch: any) => {
    //convert cart to string
    let strCart = JSON.stringify(cart);
    //save cart
    AsyncStorage.setItem('cart', strCart);
    dispatch({
      type: SAVE_CART,
      success: true,
      loader: true,
    });
    setTimeout(() => {
      dispatch(getCart());
    }, 500);
  };
};
export const applyCoupon = (data: any) => {
  //type of cart is array
  return async (dispatch: any) => {
    Api.post(`/menu/apply_coupon`,data)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: COUPON_DATA,
            payload: response.data.coupon,
          });
          Toast.show({
            text: response.data.successResponse,
            buttonText: 'OK',
            duration: 5000,
            type: 'success',
          });
          // Actions.refresh({ key: Math.random() });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'warning',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const getTaxValue = (state_id: any) => {
  return async (dispatch: any) => {
    Api.get(`menu/tax_value/${state_id}`)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: GET_TAX,
            payload: response.data.successResponse,
          });
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  };
};
export const getInst = (text: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: GET_INST,
      instructions: text
    });
  };
};
