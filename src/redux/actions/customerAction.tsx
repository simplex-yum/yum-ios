import {
  FAV_MENU_ITEM,
  ADDRESS_LIST,
  ORDER_LIST,
  ORDER_DETAIL,
  PAYMENT_CARDS_LIST,
  GET_PROFILE,
  LOGINOUT_CUSTOMER,
  ORDER_TYPE,
  SELECT_STORE,
  STORES_LIST,
  DELIVERY_FEE,
  ORDER_PERIOD,
  ORDER_DATE_TIME,
  DELIVERY_ADDRESS,
  SELECT_STORE_ID,
  SELECT_PAYMENT_METHOD,
  ADD_GUEST,
  OFFERS_LIST,
  SAVE_AS_PLACE,
  PROFILE_ADDRESS,
  NOTIFICATIONS,
  UPDATE_FCM_STATUS
} from './customerType';
var jwtDecode = require('jwt-decode');
import Api from '../../components/Api';
import AsyncStorage from '@react-native-community/async-storage';
import { Toast } from 'native-base';
import moment from 'moment';
import * as geolib from 'geolib';
// import { LoginManager } from 'react-native-fbsdk';
// import { GoogleSignin } from 'react-native-google-signin';
import { COUPON_DATA } from './cartType';

export const saveFCMToken = (fcmToken: any) => {
  return async function (dispatch: any) {
    let fcm: any = {
      token: fcmToken,
      is_active: 1
    }
    Api.post(`/customer/save_fcmToken`, fcm)
      .then(async (response) => {
        if (response.data.success) {
          await AsyncStorage.setItem('fcm_status', 'true');
          // console.log('token saved')
        }
      })
      .catch((err) => {
        let error;
        if (typeof err.response.data.message === 'string') {
          if (err.response.data.code === 'ER_DUP_ENTRY') {
            error = 'Token Already Exist';
          } else {
            error = err.response.data.message;
          }
        } else if (typeof err.response.data.message === 'object') {
          error = err.response.data.message[0].replace(/_/g, ' ');
        }
        // console.log(error)
      });
  };
};
export const UpdateFmcStatus = () => {
  return async (dispatch: any) => {
    let Status = await AsyncStorage.getItem('fcm_status');
    dispatch({
      type: UPDATE_FCM_STATUS,
      fcmStatus: Status == 'true' ? true : false
    });
  };
}
export const NotificationList = () => {
  return async (dispatch: any) => {
    Api.get('/customer/notifications/')
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: NOTIFICATIONS,
            payload: response.data.successResponse,
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch({
            type: NOTIFICATIONS,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  }
};
export const UpdateFCMDeviceStatus = (FCM: any) => {
  return async (dispatch: any) => {
    Api.put('/customer/updateFCMToken', FCM)
      .then(async (response) => {
        if (response.data.success) {
          if (FCM.is_active == 1) {
            await AsyncStorage.setItem('fcm_status', 'true');
            dispatch({
              type: UPDATE_FCM_STATUS,
              fcmStatus: true
            })
          } else {
            await AsyncStorage.setItem('fcm_status', 'false');
            dispatch({
              type: UPDATE_FCM_STATUS,
              fcmStatus: false
            })
          }
          Toast.show({
            text: response.data.successResponse,
            buttonText: 'OK',
          });
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  }
};
export const findPickupStores = (lat: any, lng: any, searchString: any, navigation: any) => {
  return async (dispatch: any) => {
    Api.get('menu/stores')
      .then((response) => {
        if (response.data.success) {
          let Responedata = response.data.successResponse;
          const filteredStores = Responedata.filter((store: any) => {
            return (
              store.address.toLowerCase().includes(searchString.toLowerCase()) ||
              store.city.toLowerCase().includes(searchString.toLowerCase()) ||
              store.store_name.toLowerCase().includes(searchString.toLowerCase())
            )
          })
          // console.log(filteredStores.length)
          if (filteredStores.length === 0) {
            let data: any = [];
            for (let i = 0; i < Responedata.length; i++) {
              if (Responedata[i].zone_json) {
                if (geolib.isPointInPolygon({ latitude: lat, longitude: lng }, JSON.parse(Responedata[i].zone_json))) {
                  data.push(Responedata[i]);
                }
              }
            }
            if (data.length == 0) {
              dispatch({
                type: STORES_LIST,
                payload: [],
                selectStoreId: "",
                deliveryfee: 0,
                storeStatus: 1,
              });
              navigation.navigate("selectstore")
            } else {
              dispatch({
                type: STORES_LIST,
                payload: data,
                selectStoreId: data[0].store_id,
                deliveryfee: 0,
                storeStatus: 0, // 1 for out of Range
              });
              navigation.navigate("selectstore")
            }
          } else {
            dispatch({
              type: STORES_LIST,
              payload: filteredStores,
              selectStoreId: filteredStores[0].store_id,
              deliveryfee: 0,
              storeStatus: 0, // 1 for out of Range
            });
            navigation.navigate("selectstore")
          }
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          let data: any = [];
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          navigation.navigate("selectstore")
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
//localStore with kml Tradezone
export const findLocalStores = (lat: any, lng: any, navigation: any) => {
  return async (dispatch: any) => {
    Api.get('menu/stores')
      .then((response) => {
        if (response.data.success) {
          let Responedata = response.data.successResponse;

          let data: any = [];
          for (let i = 0; i < Responedata.length; i++) {
            if (Responedata[i].zone_json) {
              if (geolib.isPointInPolygon({ latitude: lat, longitude: lng }, JSON.parse(Responedata[i].zone_json))) {
                data.push(Responedata[i]);
              }
            }
          }
          dispatch({
            type: STORES_LIST,
            payload: data,
            selectStoreId: data.length > 0 && data[0].store_id,
            loading: false,
            deliveryfee: data.length > 0 && data[0].delivery_fee,
            storeStatus: 0,
          });
          navigation.navigate("selectstore");
          // }
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          let data: any = [];
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          navigation.navigate("selectstore");
          dispatch({
            type: STORES_LIST,
            payload: data,
            deliveryfee: 0,
            storeStatus: 0,
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
//Local Stores with circle radius
// export const findLocalStores = (lat: any, lng: any) => {
//   return async (dispatch: any) => {
//     Api.get('menu/stores')
//       .then((response) => {
//         if (response.data.success) {
//           let Responedata = response.data.successResponse;
//           //find distance of two coordinates
//           function calcCrow(
//             locationLat: any,
//             locationLong: any,
//             storeLat: any,
//             storeLong: any,
//           ) {
//             var R = 6371; // km
//             var differenceLat = toRad(storeLat - locationLat);
//             var differenceLon = toRad(storeLong - locationLong);
//             var locationLatRadian = toRad(locationLat);
//             var storeLatRadian = toRad(storeLat);
//             var a =
//               Math.sin(differenceLat / 2) * Math.sin(differenceLat / 2) +
//               Math.sin(differenceLon / 2) *
//               Math.sin(differenceLon / 2) *
//               Math.cos(locationLatRadian) *
//               Math.cos(storeLatRadian);
//             var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//             var distanceInMeter = R * c * 1000;
//             return distanceInMeter;
//           }
//           // Converts numeric degrees to radians
//           function toRad(Value: any) {
//             return (Value * Math.PI) / 180;
//           }
//           let data: any = [];
//           let store_list: any = [];
//           for (let i = 0; i < Responedata.length; i++) {
//             store_list.push(
//               calcCrow(lat, lng, Responedata[i].lat, Responedata[i].lng),
//             );
//             if (
//               calcCrow(lat, lng, Responedata[i].lat, Responedata[i].lng) <=
//               Responedata[i].trade_zone_coverage
//             ) {
//               data.push(Responedata[i]);
//             }
//           }
//           // if (data.length == 0) {
//           //   let nearest = Math.min.apply(Math, store_list);
//           //   for (let i = 0; i < Responedata.length; i++) {
//           //     if (
//           //       calcCrow(lat, lng, Responedata[i].lat, Responedata[i].lng) ==
//           //       nearest
//           //     ) {
//           //       data.push(Responedata[i]);
//           //     }
//           //   }
//           //   dispatch({
//           //     type: STORES_LIST,
//           //     payload: data,
//           //     deliveryfee: '',
//           //     storeStatus: 1,
//           //   });
//           //   Actions.selectstore();
//           // } else {
//           dispatch({
//             type: STORES_LIST,
//             payload: data,
//             selectStoreId: data.length > 0 && data[0].store_id,
//             loading: false,
//             deliveryfee: 0,
//             storeStatus: 0,
//           });
//           Actions.selectstore();
//           // }
//         }
//       })
//       .catch((err) => {
//         if (err.response) {
//           let error;
//           let data: any = [];
//           if (typeof err.response.data.message === 'string') {
//             error = err.response.data.message;
//           } else if (typeof err.response.data.message === 'object') {
//             error = err.response.data.message[0].replace(/_/g, ' ');
//           }
//           Actions.selectstore();
//           dispatch({
//             type: STORES_LIST,
//             payload: data,
//             deliveryfee: 0,
//             storeStatus: 0,
//           });
//         } else {
//           Toast.show({
//             text: err.message,
//             buttonText: 'OK',
//           });
//         }
//       });
//   };
// };
export const loginCustomer = (email: any, password: any, navigation: any) => {
  return async (dispatch: any) => {
    // console.log(email, password)
    Api.post('/customer/auth', {
      email: email,
      password: password
    })
      .then(async (response) => {
        if (response.data.success) {
          let token = response.data.accessToken;
          AsyncStorage.removeItem('guestname');
          AsyncStorage.removeItem('guestemail');
          AsyncStorage.removeItem('guestphone');
          await AsyncStorage.setItem('token', token);
          await AsyncStorage.setItem(
            'profile_pic',
            response.data.customer.profile_pic,
          );
          await AsyncStorage.setItem('name', response.data.customer.login_name);
          await AsyncStorage.setItem(
            'email',
            response.data.customer.email_address,
          );
          await AsyncStorage.setItem(
            'phone',
            response.data.customer.phone_number,
          );
          navigation.push("profile");
          // setImmediate(() => {
          //   Actions.refresh({ key: Math.random() }); //It is used to refesh the page after pop the screen
          // }); //Both will doing same work
          // setTimeout(() => { Actions.refresh({ key: Math.random() }); }, 0);
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const loginCustomerWithSocial = (data: any, navigation: any) => {
  return async (dispatch: any) => {
    Api.post('/customer/signinwithsocial', data)
      .then(async (response) => {
        if (response.data.success) {
          AsyncStorage.removeItem('guestname');
          AsyncStorage.removeItem('guestemail');
          AsyncStorage.removeItem('guestphone');
          let token = response.data.accessToken;
          await AsyncStorage.setItem('token', token);
          await AsyncStorage.setItem('name', response.data.customer.login_name);
          await AsyncStorage.setItem(
            'email',
            response.data.customer.email_address,
          );
          await AsyncStorage.setItem(
            'phone',
            response.data.customer.phone_number,
          );
          // console.log(response.data.customer)
          var str = response.data.customer.profile_pic;
          var pos = str.search('https:');
          // console.log(pos)
          if (pos == '-1') {
            // console.log('not found')
            await AsyncStorage.setItem(
              'profile_pic',
              response.data.customer.profile_pic,
            );
          } else {
            // console.log('found')
            await AsyncStorage.setItem(
              'fileUri',
              response.data.customer.profile_pic,
            );
          }
          dispatch({
            type: LOGINOUT_CUSTOMER,
            isLoggedIn: true,
          });
          navigation.push("profile");
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const signupCustomer = (signUpData: any, addressData?: any, navigation?: any) => {
  return function (dispatch: any) {
    Api.post(`/customer/signup`, signUpData)
      .then((response) => {
        if (response.data.success) {
          let responseData = response.data.successResponse;
          if(addressData){
            if (addressData.full_address !== "") {
              addressData.customer_id = responseData.customer_id;
              dispatch(
                addGuestDeliveryAddress(addressData, navigation, true),
              );
            }
          }
          setTimeout(() => {
            dispatch(
              loginCustomer(responseData.email_address, signUpData.login_password, navigation),
            );
          }, 1000);
          Toast.show({
            text: 'Customer Registered Successfully',
            duration: 5000,
            type: 'success',
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            if (err.response.data.code === 'ER_DUP_ENTRY') {
              if (err.response.data.message.indexOf("phone_number") > -1) {
                error = 'Phone Already Exist';
              } else {
                error = 'Email Already Exist';
              }
            } else {
              error = err.response.data.message;
            }
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const addGuest = (data: any, navigation?: any) => {
  return function (dispatch: any) {
    Api.post(`/customer/addGuest`, data)
      .then(async (response) => {
        if (response.data.success) {
          dispatch({
            type: ADD_GUEST,
            payload: response.data.successResponse,
          });
          await AsyncStorage.setItem('guestname', response.data.successResponse.login_name);
          if (response.data.successResponse.email_address) {
            console.log("alkdjaskl")
            await AsyncStorage.setItem(
              'guestemail',
              response.data.successResponse.email_address,
            );
          }
          await AsyncStorage.setItem(
            'guestphone',
            response.data.successResponse.phone_number,
          );
          navigation.goBack()
          // setImmediate(() => {
          //   Actions.refresh({ key: Math.random() }); //It is used to refesh the page after pop the screen
          // });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            if (err.response.data.code === 'ER_DUP_ENTRY') {
              if (err.response.data.message.indexOf("phone_number") > -1) {
                error = 'Phone Already Exist';
              } else {
                error = 'Email Already Exist';
              }
            } else {
              error = err.response.data.message;
            }
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const changePassword = (newPass: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    var decoded: any = jwtDecode(token);
    if (token) {
      Api.put(`/customer/change_password`, {
        customer_id: decoded.customer_id,
        login_password: newPass
      }, {
        headers: { 'Authorization': 'Bearer ' + token }
      })
        .then((response) => {
          if (response.data.success) {
            navigation.navigate("profile")
            dispatch(logoutCustomer(navigation));

          }
        }).catch(err => {
          // console.log(err.response.data.message)
        });
    }
  }
}
export const resetPassword = (email: any) => {
  return function (dispatch: any) {
    Api.post('/customer/forgot-password', {
      email: email,
    })
      .then((response) => {
        if (response.data.success) {
          Toast.show({
            text: 'Password has been reset. Please check your email.',
            buttonText: 'OK',
            duration: 5000,
            type: 'success',
          });

          // dispatch({
          //     type: RESET_PASS,
          //     isReset: true
          // })
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const favoritesList = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.get(`/customer/wishlist/${decoded.customer_id}`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            dispatch({
              type: FAV_MENU_ITEM,
              payload: response.data.successResponse,
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            dispatch({
              type: FAV_MENU_ITEM,
              payload: [],
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }else{
      dispatch({
        type: FAV_MENU_ITEM,
        payload: [],
      });
    }
  };
};
export const deletefavourites = (favData: any, wish_id: number) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    Api.delete(`/customer/del_wish/${wish_id}`, {
      headers: { Authorization: 'Bearer ' + token },
    })
      .then((response) => {
        if (response.data.success) {
          var filtered = favData.filter((element: any, index: any) => {
            return element.wish_id !== wish_id;
          })
          dispatch({
            type: FAV_MENU_ITEM,
            payload: filtered,
          });
          Toast.show({
            text: 'Removed From Favourite items',
            type: 'success',
            duration: 2000,
          });
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };
};
export const addressesList = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.get(`/customer/address/${decoded.customer_id}`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            dispatch({
              type: ADDRESS_LIST,
              payload: response.data.successResponse,
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            dispatch({
              type: ADDRESS_LIST,
              payload: [],
            });
          } else {
            // console.log(err.message)
          }
        });
    }
  };
};
export const addAddress = (data: any, loginStatus?: any, navigation?: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
      Api.post(`/customer/add_address`, data, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            if (!loginStatus) {
              navigation.push("address")
              Toast.show({
                text: response.data.successResponse,
                duration: 2000,
                type: 'success',
              });
            }
            // else{
            //   dispatch(addressesList())
            // }
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            } else if (err.response.data.error == "Unauthorized") {
              error = "Please login again,your session has been expired.";
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const addGuestDeliveryAddress = (data: any, navigation: any, signup?: any) => {
  return async (dispatch: any) => {
    Api.post(`/customer/add_deliveryaddress`, data)
      .then((response) => {
        if (response.data.success) {
          console.log(signup)
          if (signup !== true) {
            navigation.push("address")
          }
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  }
};
export const editAddress = (address_id: any, data: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      Api.put(`/customer/edit_address/${address_id}`, data, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
        .then((response) => {
          if (response.data.success) {
            navigation.goBack();
            Toast.show({
              text: response.data.successResponse,
              buttonText: 'OK',
              duration: 5000,
              type: 'success',
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const setDefaultAddress = (data: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      Api.put(`/customer/set_default_address`, data, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
        .then((response) => {
          if (response.data.success) {
            navigation.push("address")
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const deleteAddress = (addressData: any, address_id: number) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    Api.delete(`/customer/del_address/${address_id}`, {
      headers: { Authorization: 'Bearer ' + token },
    })
      .then((response) => {
        if (response.data.success) {
          var filtered = addressData.filter((element: any, index: any) => {
            return element.address_id !== address_id;
          })
          dispatch({
            type: ADDRESS_LIST,
            payload: filtered,
          });
          Toast.show({
            text: 'Removed Successfully',
            type: 'success',
            duration: 2000,
          });
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  };
};
export const paymentCardsList = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.get(`/customer/payment_methods/${decoded.customer_id}`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            dispatch({
              type: PAYMENT_CARDS_LIST,
              payload: response.data.successResponse,
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            dispatch({
              type: PAYMENT_CARDS_LIST,
              payload: [],
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const addCard = (data: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
      Api.post(`/customer/add_payment_method`, data, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            // Actions.refresh({ key: Math.random() });
            // Toast.show({
            //   text: response.data.successResponse,
            //   buttonText: 'OK',
            //   duration: 5000,
            //   type: 'success',
            // });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const ordersList = (navigation: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: ORDER_LIST,
      payload: [],
      loading: true,
    });
    navigation.navigate("order");
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.get(`/customer/orders/${decoded.customer_id}`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            setTimeout(() => {
              dispatch({
                type: ORDER_LIST,
                payload: response.data.successResponse.slice(0,20),
                loading: false
              });
            }, 800);
          }
        })
        .catch((err) => {
          if (err.response) {
            dispatch({
              type: ORDER_LIST,
              payload: [],
              loading: false
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }else{
      dispatch({
        type: ORDER_LIST,
        payload: [],
        loading: false,
      });
    }
  };
};
export const orderDetail = (order_id: any, navigation?: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: ORDER_DETAIL,
      payload: [],
      loading: true
    });
    navigation.navigate("orderdetail")
    let token: any = await AsyncStorage.getItem('token');
    Api.get(`/customer/order/${order_id}`, {
      headers: { Authorization: 'Bearer ' + token },
    })
      .then((response) => {
        if (response.data.success) {
          // dispatch(getStoreById(response.data.successResponse.order.store_id))
          setTimeout(() => {
            dispatch({
              type: ORDER_DETAIL,
              payload: response.data.successResponse,
              loading: false
            });
          }, 500);
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch({
            type: ORDER_DETAIL,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const getcustomerProfile = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.get(`/customer/profile/${decoded.customer_id}`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            dispatch({
              type: GET_PROFILE,
              payload: response.data.successResponse,
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            dispatch({
              type: GET_PROFILE,
              payload: [],
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const editProfile = (data: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.put(`/customer/edit_profile/${decoded.customer_id}`, data, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
        .then((response) => {
          if (response.data.success) {
            Toast.show({
              text: 'Profile Updated Successfully',
              duration: 2000,
              type: 'success',
            });
            navigation.navigate("profile");
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              if (err.response.data.code === 'ER_DUP_ENTRY') {
                if (err.response.data.message.indexOf("phone_number") > -1) {
                  error = 'Phone number already exist';
                }
              } else {
                error = err.response.data.message;
              }
            } else if (typeof (err.response.data.message) === "object") {
              error = err.response.data.message[0].replace(/_/g, " ");
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const uploadAvatar = (fileData: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      var decoded: any = jwtDecode(token);
      Api.post(`/customer/upload_avatar/${decoded.customer_id}`, fileData, {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'multipart/form-data',
        },
      })
        .then((response) => {
          if (response.data.success) {
            Toast.show({
              text: 'Avatar Uploaded',
              duration: 5000,
              type: 'success',
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const saveOrder = (data: any, store: any, tax: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    let guest = true;
    if (!data.customer_id) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
      guest = false;
    }
    Api.post(`/customer/save_order`, data)
      .then(async (response) => {
        if (response.data.success) {
          //for invoice email
          if (tax) { response.data.order.tax = tax };
          if (store) {
            response.data.order.store = store.store_name;
            response.data.order.store_address = store.address;
          }
          if (guest === false) {
            response.data.order.name = await AsyncStorage.getItem('name');
            response.data.order.email = await AsyncStorage.getItem('email');
            response.data.order.phone = await AsyncStorage.getItem('phone');
          } else {
            response.data.order.name = await AsyncStorage.getItem('guestname');
            response.data.order.email = await AsyncStorage.getItem('guestemail');
            response.data.order.phone = await AsyncStorage.getItem('guestphone');
          }
          navigation.navigate("payment", {
            order_id: response.data.order.order_id,
            amount: data.order_grossprice,
            orderInfo: response.data.order //for invoice email
          })
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const saveLaterOrder = (data: any, store: any, tax: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (!data.customer_id) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
    }
    Api.post(`/customer/save_order_later`, data)
      .then(async (response) => {
        if (response.data.success) {
          //for invoice email
          response.data.order.tax = tax;
          if (store) {
            response.data.order.store = store.store_name;
            response.data.order.store_address = store.address;
          }
          if (!data.customer_id) {
            response.data.order.name = await AsyncStorage.getItem('name');
            response.data.order.email = await AsyncStorage.getItem('email');
            response.data.order.phone = await AsyncStorage.getItem('phone');
          } else {
            response.data.order.name = await AsyncStorage.getItem('guestname');;
            response.data.order.email = await AsyncStorage.getItem('guestemail');;
            response.data.order.phone = await AsyncStorage.getItem('guestphone');;
          }
          navigation.navigate("payment", {
            order_id: response.data.order_id,
            amount: data.order_grossprice,
            orderInfo: response.data.order //for invoice emai
          })
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
// export const forgotPassword = (email: any) => {
//     return function (dispatch: any) {
//         Api.post('/admin/forgot-password', {
//             email: email
//         })
//             .then((response) => {
//                 if (response.data.success) {
//                     dispatch({
//                         type: FORGOT_PASS,
//                         payload: "Your password has been updated and We have sent the email on given email address",
//                         isReset: true
//                     })
//                 }
//             }).catch(err => {
//                 if (err.response) {
//                     let error;
//                     if (typeof (err.response.data.message) === "string") {
//                         error = err.response.data.message;
//                     } else if (typeof (err.response.data.message) === "object") {
//                         error = err.response.data.message[0];
//                     }
//                     dispatch({
//                         type: FORGOT_PASS,
//                         payload: error,
//                         isReset: false
//                     })
//                 } else {
//                     alert(err.message)
//                 }
//             });
//     }
// }
export const saveOrderType = (type: any) => {
  return function (dispatch: any) {
    dispatch({
      type: ORDER_TYPE,
      orderType: type,
    });
  };
};
export const paymentProcess = (data: any, navigation: any) => {
  return async (dispatch: any) => {
    let GuestName = await AsyncStorage.getItem('guestname');
    Api.post(`/customer/save_payment`, data)
      .then(async (response) => {
        if (response.data.success) {
          await AsyncStorage.removeItem('cart');
          dispatch({
            type: COUPON_DATA,
            payload: '',
          });
          Toast.show({
            text: `Thank You! Your order has been succesfully placed , Your Order tracking ID is ${data.orderInfo.order_id}`,
            duration: 5000,
            type: 'success'
          });
          setTimeout(() => {
            navigation.navigate("home")
          }, 400)
          // if (GuestName !== null) {
          //   Actions.home();
          // } else {
          //   dispatch(ordersList());
          //   Actions.order();
          // }

        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const laterPaymentProcess = (data: any, navigation: any) => {
  return async (dispatch: any) => {
    Api.post(`/customer/save_payment_later`, data)
      .then(async (response) => {
        if (response.data.success) {
          await AsyncStorage.removeItem('cart');
          dispatch({
            type: COUPON_DATA,
            payload: '',
          });
          Toast.show({
            text: 'Thank You! You have placed order successfully',
            duration: 5000,
            type: 'success'
          });
          setTimeout(() => {
            navigation.navigate("home")
          }, 400)
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const paymentProcessByCard = (
  data: any,
  order_id: any,
  orderperiod: any,
  orderInfo: any,
  date: any,
  time: any,
) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      Api.post(`/customer/api/doPayment`, data)
        .then((response) => {
          let obj: any = {
            payment_token: data.tokenId,
            payment_amount: response.data.amount,
            payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
            payment_processor: response.data.calculated_statement_descriptor,
            card_last_digits: response.data.payment_method_details.card.last4,
            payment_method: response.data.payment_method_details.type,
            card_type: response.data.payment_method_details.card.funding,
            card_brand: response.data.payment_method_details.card.brand,
            payment_status: response.data.status,
            order_id: order_id,
            orderInfo: orderInfo
          };
          if (orderperiod == 'later') {
            let splitTime = time.split(':');
            let splitDate = date.split('-');
            obj.datetime = new Date(
              splitDate[0],
              splitDate[1] - 1,
              splitDate[2],
              splitTime[0],
              splitTime[1],
            );
            // dispatch(laterPaymentProcess(obj));
          } else if (orderperiod == 'now') {
            // dispatch(paymentProcess(obj));
          }
        })
        .catch((err) => {
          // console.log(err);
        });
    }
  };
};
export const saveStore = (store: any, navigation: any) => {
  return function (dispatch: any) {
    dispatch({
      type: SELECT_STORE,
      selectedStore: store,
    });
    navigation.push("cart");
  };
};
export const saveSelectStoreId = (storeId: any) => {
  return function (dispatch: any) {
    dispatch({
      type: SELECT_STORE_ID,
      selectStoreId: storeId,
    });
  };
};
export const saveDeliveryFee = (delivery_fee: any) => {
  return function (dispatch: any) {
    dispatch({
      type: DELIVERY_FEE,
      deliveryfee: delivery_fee,
    });
  };
};
export const saveOrderPeriod = (period: any) => {
  return function (dispatch: any) {
    dispatch({
      type: ORDER_PERIOD,
      orderperiod: period,
    });
  };
};
export const saveDateTime = (DateTime: any) => {
  return function (dispatch: any) {
    dispatch({
      type: ORDER_DATE_TIME,
      datetime: DateTime,
    });
  };
};
export const getStores = () => {
  return async (dispatch: any) => {
    Api.get('menu/stores')
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: STORES_LIST,
            payload: response.data.successResponse,
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const getStoreById = (store_id: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      Api.get(`customer/store/${store_id}`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            dispatch({
              type: SELECT_STORE,
              selectedStore: response.data.successResponse,
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            Toast.show({
              text: error,
              buttonText: 'OK',
              duration: 5000,
              type: 'danger',
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const saveReview = (orderId: any, data: any, navigation: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    Api.put(`/customer/save_review/${orderId}`, data, {
      headers: { Authorization: 'Bearer ' + token },
    })
      .then((response) => {
        if (response.data.success) {
          navigation.goBack();
          // setImmediate(() => {
          //   Actions.refresh({ key: Math.random() }); //It is used to refesh the page after pop the screen
          // });
          Toast.show({
            text: "Thanks for your feedback",
            buttonText: 'OK'
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          Toast.show({
            text: error,
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const saveDeliveryAddress = (address: any) => {
  return function (dispatch: any) {
    dispatch({
      type: DELIVERY_ADDRESS,
      deliveryaddress: address,
    });
  };
};
export const saveAddressToProfile = (address: any) => {
  return function (dispatch: any) {
    dispatch({
      type: PROFILE_ADDRESS,
      profileaddress: address,
    });
  };
};

export const saveAsPlace = (place: any) => {
  return function (dispatch: any) {
    dispatch({
      type: SAVE_AS_PLACE,
      saveAs: place,
    });
  };
};
export const saveSelectPaymentMethod = (paymentMethod: any) => {
  return function (dispatch: any) {
    dispatch({
      type: SELECT_PAYMENT_METHOD,
      selectedPaymentMethod: paymentMethod,
    });
  };
};
export const offersList = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    if (token) {
      Api.get(`customer/promos`, {
        headers: { Authorization: 'Bearer ' + token },
      })
        .then((response) => {
          if (response.data.success) {
            dispatch({
              type: OFFERS_LIST,
              payload: response.data.successResponse,
              loading: false
            });
          }
        })
        .catch((err) => {
          if (err.response) {
            let error;
            if (typeof err.response.data.message === 'string') {
              error = err.response.data.message;
            } else if (typeof err.response.data.message === 'object') {
              error = err.response.data.message[0].replace(/_/g, ' ');
            }
            dispatch({
              type: OFFERS_LIST,
              payload: [],
              loading: false
            });
          } else {
            Toast.show({
              text: err.message,
              buttonText: 'OK',
            });
          }
        });
    }
  };
};
export const addContact = (data: any) => {
  return function (dispatch: any) {
    Api.post(`/customer/add_contact`, data)
      .then((response) => {
        if (response.data.success) {
          Toast.show({
            text: 'Your contact has been passed on to the customer service team.',
            duration: 3000,
          });
        }
      }).catch(err => {
        if (err.response) {
          let error;
          if (typeof (err.response.data) === "string") {
            error = err.response.data;
          } else if (typeof (err.response.data) === "object") {
            error = err.response.data;
          }
          console.log(error)
        }
      });
  }
}
export const logoutCustomer = (navigation: any) => {
  return function (dispatch: any) {
    AsyncStorage.removeItem('token');
    AsyncStorage.removeItem('profile_pic');
    AsyncStorage.removeItem('fileUri');
    AsyncStorage.removeItem('phone');
    AsyncStorage.removeItem('name');
    AsyncStorage.removeItem('email');
    // const _isSignedIn = async () => {
    //   const isSignedIn = await GoogleSignin.isSignedIn();
    //   if (isSignedIn) {
    //     await GoogleSignin.revokeAccess();
    //     //revoke Access method is used before the Signout method because SignIn Required for revokeAccess Method
    //     await GoogleSignin.signOut();
    //   }
    // };
    // _isSignedIn();
    // LoginManager.logOut();
    navigation.push("profile");
    Toast.show({
      text: 'You have been logged out successfully',
      duration: 3000,
    });
    dispatch({
      type: LOGINOUT_CUSTOMER,
      name: "",
      email_address: "",
      phone_number: "",
      profile_pic: "",
      fileUri: "",
      loading: false,
      isLoggedIn: false
    });
  };
};
export const setloginCustomer = () => {
  return async (dispatch: any) => {
    const token = await AsyncStorage.getItem('token');
    const name = await AsyncStorage.getItem('name');
    const email_address = await AsyncStorage.getItem('email');
    const phone_number = await AsyncStorage.getItem('phone');
    const profile_pic = await AsyncStorage.getItem('profile_pic');
    const fileUri = await AsyncStorage.getItem('fileUri');
    if (token) {
      // if (jwtDecode(token).exp >= Date.now() / 1000) {
      dispatch({
        type: LOGINOUT_CUSTOMER,
        name: name,
        email_address: email_address,
        phone_number: phone_number,
        profile_pic: profile_pic,
        fileUri: fileUri,
        loading: false,
        isLoggedIn: true,
      });
      // } else {
      //   AsyncStorage.clear();
      // }
    } else {
      dispatch({
        type: LOGINOUT_CUSTOMER,
        name: "",
        email_address: "",
        phone_number: "",
        profile_pic: "",
        fileUri: "",
        isLoggedIn: false,
        loading: false
      });
    }
  };
};
