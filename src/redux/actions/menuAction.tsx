import {
  MENU_COMBO_LIST,
  COMBO_DETAIL,
  MENU_TOP_DEALS,
  HERO_ITEMS,
  ITEM_DETAIL,
  ALL_GROUPS_LIST,
  HOME_GROUPS_LIST,
  MENU_ITEMS,
  ITEM_LIST,
  DETAIL_COUNT,
  HANDLE_MODIFIERS,
  UPDATE_MODIFIERS,
  HANDLE_SIZE
} from './menuType';
import Api from '../../components/Api';
import { Toast } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { NotificationList } from './customerAction';
var jwtDecode = require('jwt-decode');
export const menuCombosList = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    dispatch(menuTopDeals())
    dispatch(menuHeroItems())
    dispatch(homeGroupsList())
    dispatch(NotificationList())
    dispatch(allGroupsList())
    let data: any = {}
    if (token) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
    }
    Api.post('menu/combos', data)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: MENU_COMBO_LIST,
            payload: response.data.successResponse,
            loading: false
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: MENU_COMBO_LIST,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const menuTopDeals = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    let data: any = {}
    if (token) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
    }
    Api.post('menu/topDeals', data)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: MENU_TOP_DEALS,
            payload: response.data.successResponse,
            loading: false
          });
          dispatch({
            type: MENU_COMBO_LIST,
            payload: [],
            loading: false
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: MENU_TOP_DEALS,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const menuHeroItems = () => {
  return async (dispatch: any) => {
    Api.get('menu/hero_items')
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: HERO_ITEMS,
            payload: response.data.successResponse,
            loading: false
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: HERO_ITEMS,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const getComboDetail = (combo_id: any) => {
  return async (dispatch: any) => {
    Api.post(`menu/combo/${combo_id}`)
      .then((response) => {
        dispatch({
          type: ITEM_DETAIL,
          payload: [],
        });
        if (response.data.success) {
          dispatch({
            type: COMBO_DETAIL,
            payload: response.data.successResponse,
            itemPrice: (response.data.successResponse.discount_price && (response.data.successResponse.order_channel == 'all' || response.data.successResponse.order_channel == 'mobile')) ? Math.round(response.data.successResponse.discount_price) : Math.round(response.data.successResponse.combo_mrp_price),
            actualPrice: Math.round(response.data.successResponse.combo_sales_price),
            modifiers: response.data.successResponse.modifiers
          });
          dispatch(getComboCounts(combo_id))
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: COMBO_DETAIL,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const getComboCounts = (combo_id: any) => {
  return async (dispatch: any) => {
    Api.get(`menu/combo_counts/${combo_id}`)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: DETAIL_COUNT,
            payload: response.data.successResponse,
          });
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  };
};
export const getItemDetail = (item_id: any) => {
  return async (dispatch: any) => {
    Api.post(`menu/item/${item_id}`)
      .then((response) => {
        dispatch({
          type: COMBO_DETAIL,
          payload: [],
        });
        if (response.data.success) {
          let sizearray: any = JSON.parse(response.data.successResponse.item_size);
          dispatch({
            type: ITEM_DETAIL,
            payload: response.data.successResponse,
            sizearray: sizearray,
            selectedsize: sizearray[0],
            itemPrice: (sizearray[0].discount_price && (sizearray[0].order_channel == "all" || sizearray[0].order_channel == "mobile")) ? Math.round(sizearray[0].discount_price) : Math.round(sizearray[0].mrp),
            actualPrice: Math.round(sizearray[0].price),
            modifiers: response.data.successResponse.modifiers
          });
          dispatch(getItemCounts(item_id))
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: ITEM_DETAIL,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const getItemCounts = (item_id: any) => {
  return async (dispatch: any) => {
    Api.get(`menu/item_counts/${item_id}`)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: DETAIL_COUNT,
            payload: response.data.successResponse,
          });
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  };
};
export const homeGroupsList = () => {
  return async (dispatch: any) => {
    Api.post(`menu/groups`)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: HOME_GROUPS_LIST,
            payload: response.data.successResponse,
          });
          dispatch({
            type: MENU_ITEMS,
            payload: [],
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: MENU_ITEMS,
            payload: [],
          });
          dispatch({
            type: HOME_GROUPS_LIST,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const allGroupsList = () => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    let data: any = {}
    if (token) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
    }
    Api.post(`menu/allGroups`, data)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: ALL_GROUPS_LIST,
            groups: response.data.groups,
            groupsData: response.data.groupsData
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: MENU_ITEMS,
            payload: [],
          });
          dispatch({
            type: ALL_GROUPS_LIST,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};
export const menuItemsListByGroupsID = (group_id: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');
    let data: any = {};
    if (token) {
      var decoded: any = jwtDecode(token);
      data.customer_id = decoded.customer_id;
    }
    Api.post(`menu/items/${group_id}`, data)
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: MENU_ITEMS,
            payload: response.data.successResponse,
            loading: false
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch({
            type: MENU_ITEMS,
            payload: [],
          });
        } else {
          // console.log(err);
        }
      });
  };
};
export const menuItemsList = () => {
  return async (dispatch: any) => {
    Api.get('menu/items')
      .then((response) => {
        if (response.data.success) {
          dispatch({
            type: ITEM_LIST,
            payload: response.data.successResponse,
          });
        }
      })
      .catch((err) => {
        if (err.response) {
          let error;
          if (typeof err.response.data.message === 'string') {
            error = err.response.data.message;
          } else if (typeof err.response.data.message === 'object') {
            error = err.response.data.message[0].replace(/_/g, ' ');
          }
          dispatch({
            type: ITEM_LIST,
            payload: [],
          });
        } else {
          Toast.show({
            text: err.message,
            buttonText: 'OK',
          });
        }
      });
  };
};

export const favouriteAdd = (param1: any) => {
  return async (dispatch: any) => {
    let token: any = await AsyncStorage.getItem('token');

    var decoded: any = jwtDecode(token);
    param1.customer_id = decoded.customer_id;
    Api.post('customer/add_wishlist', param1, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((response) => {
        if (response.data.success) {
          if (param1.combo_id && !param1.group_id) {
            Toast.show({
              text: "Added to Favourite items",
              duration: 2000
            });
            if (!param1.topDeal) {
              dispatch(menuCombosList());
            } else {
              dispatch(menuTopDeals());
            }
          } else if (param1.group_id) {
            Toast.show({
              text: "Added to Favourite items",
              duration: 2000
            });
            dispatch(allGroupsList());
          }
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  };
};
export const handleModifiers = (itemPrice: any, actualPrice: any, modifiers: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: HANDLE_MODIFIERS,
      itemPrice: itemPrice,
      actualPrice: actualPrice,
      modifiers: modifiers
    });
  };
};
export const updateModifiers = (modifiers: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: UPDATE_MODIFIERS,
      modifiers: modifiers
    });
  };
};

export const handleSize = (item: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: HANDLE_SIZE,
      selectedsize: item,
      itemPrice: (item.discount_price && (item.order_channel == "all" || item.order_channel == "mobile")) ? item.discount_price : item.mrp,
      actualPrice: item.price
    });
  };
};

