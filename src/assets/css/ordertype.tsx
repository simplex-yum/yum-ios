import { StyleSheet } from "react-native";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        paddingBottom: 10,
    },
    group2: {
        paddingHorizontal: 20
    },
    group3: {
        paddingVertical: 25,
        paddingHorizontal: 20,
        borderRadius: 42,
        flex: 1,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10
    },
    selectordertype: {
        color: "#2c2627",
        fontSize: 20,
        lineHeight: 60,
        textTransform: 'uppercase'
    },
    SavedOrdersButton: {
        borderRadius: 7,
        backgroundColor: "#31e627",
        padding: 40,
        marginVertical: 5,
        height: 90
    },
    iagreetoEa: {
        color: '#2c2627',
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 15,
        marginHorizontal:15
    },
    DeliveryButton: {
        borderRadius: 7,
        backgroundColor: "#3d9ad9",
        padding: 40,
        marginVertical: 5,
        height: 90
    },
    PickupButton: {
        borderRadius: 7,
        backgroundColor: "#bc2c3d",
        padding: 10,
        marginVertical: 5,
        height: 90
    },
    saveOrderText: {
        lineHeight: 25,
        color: '#fff',
        textTransform: 'uppercase'
    },
    saveOrderDesc: {
        color: '#fff'
    },
    registermember: {
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textAlign: 'center',
        paddingVertical: 25,
        textTransform: "uppercase"
    },
    linkText: {
        fontSize: 15,
        fontWeight: "400",
        color: '#bc2c3d',
        textTransform: "uppercase"
    },
    selecStore: {
        color: "#2c2627",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    saveButton: {
        margin: 20,
        borderRadius: 17,
        backgroundColor: "#bc2c3d",
        shadowColor: "#bc2c3d",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.5,
        shadowRadius: 6.27,
        elevation: 8
    },
    disableSaveButton: {
        margin: 20,
        borderRadius: 17,
        backgroundColor: "lightgrey",
        shadowColor: "#bc2c3d",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.5,
        shadowRadius: 6.27,
        elevation: 8
    },
    saveAddress: {
        color: "#fff",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
        textTransform: 'uppercase'
    },
    mainLogout: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flexDirection: 'row'
    },
    mainLogoutDiv: {
        justifyContent: 'flex-end'
    },
    range: {
        color: 'red',
        fontWeight: '400',
        fontSize: 12
    },
    note: {
        marginLeft: 15
    },
    noteText: {
        color: '#bc2c3d'
    },
    deliveryLocation: {
        color: "#2c2627",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 25,
    },
    place: {
        color: "#2c2627",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
    },
    address: {
        color: "#aba5a5",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
    },
    editDelete: {
        color: "#bc2c3d",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textTransform: 'uppercase'
    },
    addButton: {
        margin: 20,
        borderRadius: 7,
        backgroundColor: "#bc2c3d",
        shadowColor: "#bc2c3d",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.5,
        shadowRadius: 6.27,
        elevation: 8
    },
    saveAsText: {
        color: "#2c2627",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
        marginTop:15
    },
    savedLocat: {
        color: "#2c2627",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    saveAsButton: { borderRadius: 10, marginRight: 5 }
})