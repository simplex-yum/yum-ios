import { StyleSheet } from "react-native";
export default StyleSheet.create({
    orderId: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    orderPrice: {
        color: "#bc2c3d",
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 19,
    },
    itemsQuantity: {
        color: "#aba5a5",
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
        textTransform: 'uppercase'
    },
    productThumbnail: {
        borderRadius: 14, width: 66, height: 66
    },
    productTitle: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Bold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 22
    },
    deliveredOrder: {
        color: '#31ab5f',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 19,
        textTransform:"uppercase"
    },
    cancelledOrder: {
        color: '#e0302d',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 19,
        textTransform:"uppercase"
    },
    itemsTotaldiscount:{
        color: '#747474',
        // fontFamily: "ProximaNova-Regular",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    totalvaldisval:{
        color: '#2c2627',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    deliveryfee:{
        color: '#2f9f59',
        // fontFamily: "ProximaNova-Regular",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    total: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 38,
        fontWeight: '400',
        lineHeight: 51
    },
    repeatButton: {
        borderRadius: 17,
        backgroundColor: "#bc2c3d",
        shadowColor: "rgb(188,44,61)",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 6.27,
        elevation: 10,
        margin:15,
        padding:40        
    },
    repeatText: {
        color: '#ffffff',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
        textAlign:'center',
        textTransform:'uppercase',
    },
    review:{
        color: '#bc2c3d',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 20,
        fontWeight: '400',
        lineHeight: 20,
        textAlign:'center',
        textTransform:'uppercase',
        marginVertical:35
    },
    itemCartTax:{
        margin:-20
    }
})