import { StyleSheet } from "react-native";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        paddingBottom: 30,
    },
    group2: {
        marginHorizontal: 15,
        borderRadius: 22,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    avatar: {
        width: 81,
        height: 81,
        borderRadius: 81 / 2
    },
    editProfile: {
        alignSelf: 'center', marginVertical: 20
    },
    editButton: { position: 'relative',width:60,height:30, marginLeft: -20, backgroundColor: '#fff', borderRadius: 4, borderWidth: 2, borderColor: '#bc2c3d' },
    editText: { color: '#bc2c3d' },
    name: {
        // fontFamily: "ProximaNova-Bold",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    email: {
        // fontFamily: "ProximaNova-Regular",
        fontSize: 13,
        fontWeight: '400',
        lineHeight: 17
    },
    location: {
        // fontFamily: "ProximaNova-Regular",
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 30
    },
    dividerTitle: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29
    },
    menuIcon: {
        color: "#bc2c3d"
    },
    menuTitle: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Regular",
        fontSize: 14,
        fontWeight: '400',
    },
    percentIconButton: { borderWidth: 1, borderColor: '#bc2c3d' },
    percentIcon: { color: '#bc2c3d', fontSize: 15 },

})