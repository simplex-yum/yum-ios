import { StyleSheet } from "react-native";
import {
    widthPercentageToDP as dw,
    heightPercentageToDP as dh
}
    from '../../components/responsiveness';
export default StyleSheet.create({
    group1: {
        paddingHorizontal: dw(3),
        borderTopRightRadius: 42,
        borderTopLeftRadius: 42,
        flex: 1,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    imagesView: {
        height: 300
    },
    itemName: {
        color: '#2c2627',
        
        fontSize: dw(4.5),
        fontWeight: '400',
        lineHeight: dh(4),
    },
    counterStyle: {
        width: 50,
        height: 50,
        alignItems: "center",
        borderRadius: 50 / 2
    },
    iconStyle: { color: '#fff', padding: 10 },
    itemPrice: {
        color: "#bc2c3d",
        
        fontSize: dw(4),
        fontWeight: '400',
        lineHeight: 27
    },
    addCart: {
        margin: 15,
        padding: 40,
        borderRadius: 7,
        backgroundColor: "#bc2c3d",
        shadowColor: "#bc2c3d",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.5,
        shadowRadius: 6.27,
        elevation: 8
    },
    addCartText: {
        color: "#fff",
        fontSize: dw(3),
        fontWeight: '400',
        lineHeight: 21,
        textTransform: 'uppercase'
    },
    detailHead: {
        marginHorizontal: 15,
        color: "#2c2627",
        
        fontSize: 17,
        fontWeight: '400',
        lineHeight: 23,
        marginTop: 8,
        textTransform: 'uppercase',
        textDecorationLine: "underline"
    },
    details: {
        marginHorizontal: 15,
        color: "#6b6868",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 25,
    },
    backButtonStyle: {
        position: 'absolute', left: 20, marginVertical: 40, borderBottomWidth: 0
    },
    modifiers:{
        height: 22, width: 22
    },
    modifierText:{
        color: '#2c2627',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 25,
    }
})