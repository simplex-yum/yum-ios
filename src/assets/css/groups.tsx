import { StyleSheet } from "react-native";
import {
    widthPercentageToDP as dw,
    heightPercentageToDP as dh
}
    from '../../components/responsiveness';
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        padding: 20
    },
    foodImage: {
        width: dw(40),
        height: dh(25)
    },
    noOrderAva: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: dw(6),
        fontWeight: '400',
        lineHeight: dh(10),
    },
    goodfoodis: {
        textAlign: "center",
        color: "#7a7a7a",
        // fontFamily: "ProximaNova-Light",
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
        padding: 15
    },
    itemImage: {
        width: dw(20), height: dh(13)
    },
    itemPrice: {
        // fontFamily: "ProximaNova-Regular",
        fontSize: 13,
        marginVertical:15,
        fontWeight: '400'
    },
    addCart: {
        marginVertical:20,
        borderRadius: 7,
        backgroundColor: "#bc2c3d",
        shadowColor: "#bc2c3d",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.5,
        shadowRadius: 6.27,
        elevation: 8
    },
    ingredients: {
        color: '#999999',
        // fontFamily: "ProximaNova-Regular",
        fontSize: 11,
        fontWeight: '400',
        lineHeight: 21,
        textAlign:"justify",
    },
})