import { StyleSheet } from "react-native";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        paddingBottom: 10,
    },
    group2: {
        padding: 15
    },
    group3: {
        paddingHorizontal: 40
    },
    welcomeBac: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Regular",
        fontSize: 45,
        lineHeight: 60,
        marginTop:5
    },
    signintoco: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Regular",
        fontSize: 18,
        lineHeight: 24
    },
    forgotPass: {
        color: "#bc2c3d",
        // fontFamily: "ProximaNova-Regular",
        fontSize: 14,
        fontWeight: '400',
        padding: 15
    },
    SignInButton: {
        borderRadius: 7,
        backgroundColor: "#bc2c3d",
        padding: 10,
        marginVertical: 5
    },
    FacebookButton: {
        borderRadius: 7,
        backgroundColor: "#3b5998",
        padding: 10,
        marginVertical: 5
    },
    GoogleButton: {
        borderRadius: 7,
        backgroundColor: "#ffffff",
        padding: 10,
        marginVertical: 5
    },
    SignUpButton: {
        width: 300,
        marginTop: 30,
        backgroundColor: "lightgrey",
        padding: 10,
        borderRadius: 5,
    },
    signinText: {
        lineHeight: 21,
        color: '#fff',
    },
    googleText: {
        lineHeight: 21,
        color: '#bc2c3d'
    },
    donthavean: {
        // fontFamily: "ProximaNova-Regular",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textAlign:'center',
        paddingVertical: 25
    },
    linkText: {
        fontSize: 15,
        fontWeight: "400",
        color: '#bc2c3d',
        paddingVertical: 25
    },
    byloggingin: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Regular",
        fontSize: 13,
        fontWeight: '400',
        lineHeight: 17,
        textAlign: 'center'
    }
})