import { StyleSheet } from "react-native";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        paddingBottom: 30,
    },
    group2: { 
        marginHorizontal: 5,
        borderRadius: 22,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    signup: {
              fontSize: 40,
        fontWeight: '400',
        paddingHorizontal: 15
    },
    itsgreatto: {
        
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
    },
    iagreetoEa: {
        color: '#2c2627',
        
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 15,
        paddingVertical: 30
    },
    linkText: {
        fontSize: 18,
        fontWeight: "400",
        color: '#bc2c3d',
        paddingVertical: 25
    },
    SignUpButton: {
        width: 225,
        marginTop: -20, 
        alignSelf:'center',
        borderRadius: 7,
        backgroundColor: "#bc2c3d",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    disableSaveButton: {
        width: 225,
        marginTop: -20, 
        alignSelf:'center',
        borderRadius: 7,
        backgroundColor: "lightgrey",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    signupText: {
        color: "#ffffff",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
        textTransform: 'uppercase',
        letterSpacing: 0.53333336
    },
    alreadyhav: {
        color: "#2c2627",
        lineHeight:20,
        fontSize: 15,
        fontWeight: '400',
        paddingVertical: 25,
        textAlign:'center'
        // marginLeft: 50
    },
    note:{
        marginLeft:15
    },
    noteText:{
        color:'#bc2c3d'
    }

})