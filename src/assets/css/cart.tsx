import { StyleSheet } from "react-native";
import PickerAndroid from "../../../native-base-theme/components/Picker.android";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        padding: 20
    },
    cartImage: {
        width: 300,
        height: 250,
        marginVertical: 30
    },
    cartEmpty: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 31,
        fontWeight: '400',
        lineHeight: 40,
    },
    goodfoodis: {
        textAlign: "center",
        color: "#7a7a7a",
        // fontFamily: "ProximaNova-Light",
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
        padding: 15
    },
    productThumbnail: {
        borderRadius: 14, width: 66, height: 66
    },
    productTitle: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Bold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 22
    },
    percentIconButton: { borderWidth: 1, borderColor: 'black' },
    percentIcon: { color: 'black', fontSize: 15 },
    productPrice: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
    },
    itemsTotaldiscount: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Regular",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    totalvaldisval: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    totaldiscount: {
        color: '#2f9f59',
        // fontFamily: "ProximaNova-Regular",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    totaldisval: {
        color: '#2f9f59',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    deliveryfee: {
        color: '#2f9f59',
        // fontFamily: "ProximaNova-Regular",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21
    },
    total: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 38,
        fontWeight: '400',
        lineHeight: 51
    },
    deliverat: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Bold",
        fontSize: 16,
        fontWeight: '400',
        paddingRight: 25,
        lineHeight: 21
    },
    change: {
        color: '#bc2c3d',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textTransform: "uppercase"
    },
    confirmButton: {
        borderRadius: 17,
        backgroundColor: "#bc2c3d",
        shadowColor: "rgb(188,44,61)",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 6.27,
        elevation: 10,
        margin: 15,
        padding: 40
    },
    confirmText: {
        color: '#ffffff',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 13,
        fontWeight: '400',
        lineHeight: 21,
        textTransform: 'uppercase',
    },
    itemCartTax: {
        margin: -20
    }
})