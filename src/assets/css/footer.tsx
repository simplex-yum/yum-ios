import { StyleSheet } from "react-native";
export default StyleSheet.create({
    footerStyle: {
        paddingHorizontal: 10,
        borderTopLeftRadius: 43,
        borderTopRightRadius: 43,
        shadowColor: "#bc2c3d",
        shadowOffset: {
            width: 0,
            height: -15,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    footerButtons: {
        borderRadius: 43,
    },
    group1: {
        paddingHorizontal: 10,
        borderRadius: 42,
        flex: 1,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
});