import { StyleSheet } from "react-native";
export default StyleSheet.create({
    container: {
        alignItems: 'center',
        zIndex: -1
    },
    group1: {
        paddingHorizontal: 15,
    },
    group2: {
        marginHorizontal: 25,
        borderRadius: 22,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    selectLocationStyle: {
        position: 'absolute', left: 20, marginVertical: 25, borderBottomWidth: 0
    },
    topRightCorner: {
       position: 'absolute', right: 0, marginVertical: 15
    },
    cartButton: {
        bottom: 45, right: 50
    },
    discoverNe: {
        // color: "#fff",
        fontSize: 32,
        fontWeight: '400',
        lineHeight: 32,
    },
    searchView: { flexDirection: "row", backgroundColor: "#F0EEEE", marginTop: 15 },
    searchResult: {
        backgroundColor: "#F0EEEE", marginTop: 15, padding: 10
    },
    itemImage: {
        width: 200, height: 200, flex: 1
    },
    itemPrice: {        fontSize: 15,
        fontWeight: '400',
        marginVertical: 15
    },
    menuHeadings: {        fontSize: 13,
        fontWeight: '400'
    },
    rating: {        fontSize: 10,
        fontWeight: '400'
    },
    orderBadge: {
        backgroundColor: '#bc2c3d',
        borderRadius: 20
    },
    orderBadgeText: {
        fontSize: 10,
        fontWeight: '400',
        lineHeight: 12,
        color: '#fff',
        marginHorizontal: 20
    },
    hearticon: {
        backgroundColor: '#fff',
        color: '#bc2c3d',
        margin: 15,
        padding: 15,
        borderRadius: 55 / 2,
        position: "absolute",
        top: 0,
        right: 0,
        width: 55,
        height: 55
    },
    ingredients: {
        color: '#999999',        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
    },
    orderButton: {
        fontSize: 10,
        fontWeight: '400',
        lineHeight: 12,
        color: '#fff',
        marginHorizontal: 20
    },
    // groupThumbnail: {
    //     borderRadius: 7, width: 70, height: 70,
    //     padding: 8,
    //     backgroundColor: "#fff",
    //     shadowColor: "#bc2c3d",
    //     shadowOffset: {
    //         width: 0,
    //         height: 5,
    //     },
    //     shadowOpacity: 0.34,
    //     shadowRadius: 6.27,
    //     elevation: 10,
    // },
    groupThumbnail: {
        width: 100, height: 120, flex: 1
    },
    notfound: {
        fontWeight: 'bold',
        textAlign: 'center'
    }
})