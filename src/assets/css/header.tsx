import { StyleSheet } from "react-native";
export default StyleSheet.create({
    headerStyle: {
        marginVertical: 10
        // marginHorizontal:10
    },
    headerTitle: {
        // alignSelf: 'center',
        color: '#2c2627',
        fontSize: 18,
        fontWeight: 'bold',
        lineHeight: 32
    },
    headerIcon: {
        color: "black"
    }
});