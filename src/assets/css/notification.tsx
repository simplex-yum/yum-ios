import { StyleSheet } from "react-native";
import PickerAndroid from "../../../native-base-theme/components/Picker.android";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        padding: 20
    },
    savedLocat: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    emptytext: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 31,
        fontWeight: '400',
        lineHeight: 40,
    },
    clickonaddnew: {
        textAlign: "center",
        color: "#7a7a7a",
        // fontFamily: "ProximaNova-Light",
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
        padding: 15
    },
    deliveryLocation: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    title: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
    },
    body: {
        color: "#aba5a5",
        // fontFamily: "ProximaNova-Regular",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
    },
    foodImage: {
        width: 300,
        height: 250,
        marginVertical: 30
    },
    noOrderAva: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 31,
        fontWeight: '400',
        lineHeight: 40,
    },
    goodfoodis: {
        textAlign: "center",
        color: "#7a7a7a",
        // fontFamily: "ProximaNova-Light",
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
        padding: 15
    },
})