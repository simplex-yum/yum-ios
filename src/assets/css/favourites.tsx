import { StyleSheet } from "react-native";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        // padding: 20,
    },
    group2: {
        paddingHorizontal: 15
    },
    group3: {
        paddingHorizontal: 40
    },
    heartImage: {
        width: 300,
        height: 250,
        marginVertical: 30
    },
    imgButton:{
        flex: 1,
        flexDirection: "row",
        alignItems: "stretch"
    },
    favImage: {  width: 200, height: 200, flex: 1 },
    heartIcon: {
        color: '#bc2c3d', fontSize: 25
    },
    // heartIcon: {
    //     backgroundColor: '#fff',
    //     color: '#bc2c3d',
    //     margin: 10,
    //     padding: 10,
    //     borderRadius: 45 / 2,
    //     position: "absolute",
    //     top: 0,
    //     right: 0,
    //     width: 45,
    //     height: 43
    // },
    whereisthe: {
        color: "#2c2627",      fontSize: 31,
        fontWeight: '400',
        lineHeight: 40,
    },
    onceyoufav: {
        textAlign: "center",
        color: "#7a7a7a",       fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
        padding: 15
    },
    favTitle: {
        color: "#2c2627",      fontSize: 10,
        fontWeight: '400',
        lineHeight: 19
    },
    favSize: {
        color: '#999999',
        fontSize: 13,
        fontWeight: '400',
        lineHeight: 17
    },
    ratings: {
        fontSize: 10,
        fontWeight: '400'
    }
})