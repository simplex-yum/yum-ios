import { StyleSheet } from "react-native";
export default StyleSheet.create({
    group1: {
        alignItems: 'center',
        padding: 20
    },
    foodImage: {
        width: 300,
        height: 250,
        marginVertical: 30
    },
    noOrderAva: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 31,
        fontWeight: '400',
        lineHeight: 40,
    },
    goodfoodis: {
        textAlign: "center",
        color: "#7a7a7a",
        // fontFamily: "ProximaNova-Light",
        fontSize: 18,
        fontWeight: '400',
        lineHeight: 24,
        padding: 15
    },
    orderId: {
        color: "#2c2627",
        // fontFamily: "ProximaNova-Bold",
        fontSize: 22,
        fontWeight: '400',
        lineHeight: 29,
    },
    orderPrice: {
        color: "#bc2c3d",
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 19,
    },
    itemsQuantity: {
        color: "#aba5a5",
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 21,
        textTransform: 'uppercase'
    },
    productThumbnail: {
        borderRadius: 14, width: 50, height: 50
    },
    productTitle: {
        color: '#2c2627',
        // fontFamily: "ProximaNova-Bold",
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 22
    },
    orderFooter: {
        borderTopWidth: 0.5, marginTop: 5, borderColor: 'black'
    },
    trackOrder: {
        color: '#bc2c3d',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textTransform:"capitalize"
    },
    deliveredOrder: {
        color: '#31ab5f',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textTransform:"capitalize"
    },
    cancelledOrder: {
        color: '#e0302d',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textTransform:"capitalize"
    },
    recievedOrder: {
        color: 'lightblue',
        // fontFamily: "ProximaNova-Semibold",
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 20,
        textTransform:"capitalize"
    }
})