import React from 'react';
import styles from '../assets/css/signup';
import Topbar from '../components/Topbar/simpleTopbar';
import {
  Container,
  Button,
  Text,
  Content,
  Form,
  Item,
  Input,
  Label,
  CheckBox,
  ListItem,
  Body,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { saveDeliveryAddress, signupCustomer } from '../redux';
import { SignupState, SignupProps } from '../interfaces/customer';
import { Alert, Dimensions, PermissionsAndroid, Platform, TouchableOpacity } from 'react-native';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from 'react-native-geolocation-service';
// import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class SignUp extends React.PureComponent<SignupProps, SignupState> {
  constructor(props: any) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      phone: '',
      email: '',
      password: '',
      address: '',
      isValidEmail: true,
      agreecheck: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      coordinate: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
      }
    };
    this.handleChangeChk = this.handleChangeChk.bind(this);
    this.isSignupReady = this.isSignupReady.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.signUp = this.signUp.bind(this);
    this.isEmpty = this.isEmpty.bind(this);
  }
  componentDidMount = async () => {
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }
    // Geolocation.getCurrentPosition(
    //   (position: any) => {
    //     Geocoder.from(position.coords.latitude, position.coords.longitude)
    //       .then((json: any) => {
    //         var addressComponent = json.results[0].formatted_address;
    //         this.setState({
    //           address: addressComponent, region: {
    //             latitude: Number(position.coords.latitude),
    //             longitude: Number(position.coords.longitude),
    //             latitudeDelta: LATITUDE_DELTA,
    //             longitudeDelta: LONGITUDE_DELTA
    //           },
    //           coordinate: {
    //             latitude: Number(position.coords.latitude),
    //             longitude: Number(position.coords.longitude)
    //           },
    //         });
    //         this.props.saveDeliveryAddress(addressComponent);
    //         // console.log(addressComponent);
    //       })
    //       .catch((error: any) => {
    //         // console.warn(error));
    //       })
    //   },
    //   (error: any) => {
    //     // console.warn(error));
    //   },
    //   {
    //     enableHighAccuracy: true,
    //     timeout: 4000,
    //     maximumAge: 1000,
    //   },
    // );
  }
  handleChangeChk() {
    this.setState({ agreecheck: !this.state.agreecheck });
  };
  isEmpty(obj: any) {
    if (obj === null) {
      return true;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
      return false;
    }
    if (obj && Object.keys(obj).length === 0) {
      return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  isSignupReady() {
    let { firstname, lastname, phone, email, password, isValidEmail, agreecheck } = this.state;
    return (firstname !== "" && lastname !== "" && phone !== "" && email !== "" && password !== "" && isValidEmail === true && agreecheck == true);
  }
  validateEmail(email: any) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false) {
      this.setState({ email: email, isValidEmail: false })
    } else {
      this.setState({ email: email, isValidEmail: true });
    }
  };
  handleSubmit() {
    const {
      firstname,
      lastname,
      phone,
      email,
      password,
      agreecheck,
      address
    } = this.state;
    const data: any = {
      first_name: firstname,
      last_name: lastname,
      login_name: firstname + ' ' + lastname,
      phone_number: phone,
      email_address: email,
      geo_address: address,
      login_password: password,
      is_active: 1,
    };
    if (agreecheck == false) {
      Alert.alert(
        'Please Check',
        'Please indicate that you have read and agree to the terms and Conditions and Privacy Policy',
      );
    } else {
      this.props.signupCustomer(data,null,this.props.navigation);
    }
  };
  // onMarkerDragEnd = (event: any) => {
  //   let newLat = event.nativeEvent.coordinate.latitude,
  //     newLng = event.nativeEvent.coordinate.longitude;

  //   Geocoder.from(newLat, newLng)
  //     .then((json: any) => {
  //       var addressComponent = json.results[0].formatted_address;
  //       this.setState({ address: addressComponent });
  //       this.props.saveDeliveryAddress(addressComponent);
  //     })
  //     .catch((error: any) => {
  //       // console.warn(error));
  //     })
  // };
  signUp() {
    let { isValidEmail, email } = this.state;
    return (
      <Grid>
        <Row>
          <Col style={styles.group1}>
            <Text style={styles.signup}>Signup</Text>
            <Text style={styles.itsgreatto}>It’s great to see you</Text>
          </Col>
        </Row>
        <Row>
          <Col style={styles.group2}>
            <Form>
              <Row>
                <Col>
                  <Item stackedLabel>
                    <Label>First Name <Text style={styles.noteText}>*</Text></Label>
                    <Input
                      value={this.state.firstname}
                      onChangeText={(firstname) => this.setState({ firstname })}
                    />
                  </Item>
                </Col>
                <Col>
                  <Item stackedLabel>
                    <Label>Last Name <Text style={styles.noteText}>*</Text></Label>
                    <Input
                      value={this.state.lastname}
                      onChangeText={(lastname) => this.setState({ lastname })}
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Item stackedLabel>
                    <Label>Phone <Text style={styles.noteText}>*</Text></Label>
                    <Input
                      value={this.state.phone}
                      autoCapitalize="none"
                      keyboardType="numeric"
                      maxLength={11}
                      onChangeText={(phone) =>
                        this.setState({ phone: phone.replace(/[^0-9]/g, '') })
                      }
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Item stackedLabel>
                    <Label>Address</Label>
                    <Text note>{this.state.address}</Text>
                    <Item style={{ marginRight: 20 }}>
                      {/* <MapView
                        style={{ flex: 1, height: 250, marginTop: 20 }}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        zoomTapEnabled
                        minZoomLevel={5}
                        region={this.state.region}
                        onRegionChangeComplete={region => this.setState({ region })}
                      >
                        <Marker
                          // onDrag={(e) => console.log('onDrag', e)}
                          // onDragStart={(e) => console.log('onDragStart', e)}
                          onDragEnd={this.onMarkerDragEnd}
                          draggable
                          coordinate={this.state.coordinate}
                        />

                      </MapView> */}
                      {/* <GooglePlacesAutocomplete
                        placeholder='Search'
                        onPress={(data: any, details: any = null) => {
                          // 'details' is provided when fetchDetails = true
                          // console.log(data, details);
                            Geocoder.from(data.description)
                              .then((json: any) => {
                                var location = json.results[0].geometry.location;
                                this.setState({ address: data.description })
                                this.props.saveDeliveryAddress(data.description);
                              })
                              .catch((error: any) => {
                                // console.warn(error));
                              })
                        }}
                        query={{
                          key: GOOGLE_API,
                          language: 'en',
                          components: 'country:pk',
                        }}
                      /> */}
                    </Item>
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Item stackedLabel>
                    <Label>Email <Text style={styles.noteText}>*</Text></Label>
                    <Input
                      autoCapitalize="none"
                      value={this.state.email}
                      onChangeText={(email) => this.validateEmail(email)}
                    />
                  </Item>
                </Col>
              </Row>
              {(isValidEmail == false && email !== "") &&
                <Row style={styles.note}>
                  <Col>
                    <Text note style={styles.noteText}>email is not correct</Text>
                  </Col>
                </Row>
              }
              <Row>
                <Col>
                  <Item stackedLabel>
                    <Label>Password <Text style={styles.noteText}>*</Text></Label>
                    <Input
                      autoCapitalize="none"
                      secureTextEntry={true}
                      value={this.state.password}
                      onChangeText={(password) => this.setState({ password })}
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <ListItem onPress={this.handleChangeChk}>
                    <CheckBox
                      checked={this.state.agreecheck}
                      color={'#bc2c3d'}
                    />
                    <Body>
                      <Text style={styles.iagreetoEa}>
                        I agree to{' '}
                        <Text>Terms and services</Text>,
                        <Text>Privacy policy</Text>, and{' '}
                        <Text>Content policy</Text>
                      </Text>
                    </Body>
                  </ListItem>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button
              block
              disabled={!this.isSignupReady()}
              style={!this.isSignupReady() ? styles.disableSaveButton : styles.SignUpButton}
              onPress={this.handleSubmit}>
              <Text style={styles.signupText}>SIGN UP</Text>
            </Button>
            <Text style={styles.alreadyhav}>
              Already have an account?{' '}
              <TouchableOpacity onPress={() => this.props.navigation.navigate("signin")}>
                <Text style={styles.linkText}>  Sign In</Text>
              </TouchableOpacity>
            </Text>
          </Col>
        </Row>
      </Grid>
    );
  };
  render() {
    return (
      <Container>
        <Topbar navigation={this.props.navigation} />
        <Content padder>{this.signUp()}</Content>
      </Container>
    );
  }
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    signupCustomer: function (data: any,guestdata:any,navigation:any) {
      dispatch(signupCustomer(data,guestdata,navigation));
    },
    saveDeliveryAddress: function (address: any) {
      dispatch(saveDeliveryAddress(address));
    },
  };
};
export default connect(null, mapDispatchToProps)(SignUp);
