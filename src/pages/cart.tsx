import React, { PureComponent } from 'react';
import styles from '../assets/css/cart';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import { PermissionsAndroid, Platform, ActivityIndicator } from 'react-native';
import {
  Container,
  Text,
  Content,
  Body,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Button,
  Input,
  Textarea,
  View,
  Toast,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { CartState, CartProps } from '../interfaces/cart';
import { connect } from 'react-redux';
import {
  getCart,
  saveCart,
  saveOrder,
  applyCoupon,
  getTaxValue,
  saveLaterOrder,
  getInst,
} from '../redux';
import NumericInput from 'react-native-numeric-input';
var jwtDecode = require('jwt-decode');
import AsyncStorage from '@react-native-community/async-storage';
// import Geolocation from 'react-native-geolocation-service';
// import Geocoder from 'react-native-geocoding';
import { GOOGLE_API } from '../client-config';
import Dialog, { DialogButton, DialogContent, DialogFooter, DialogTitle } from 'react-native-popup-dialog';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import {BASE_URL} from '../client-config'
// Geocoder.init(GOOGLE_API);

class Cart extends PureComponent<CartProps, CartState> {
  discount: number;
  discountPrice: number;
  cartTotal: any;
  itemTotal: any;
  tax: any;
  constructor(props: any) {
    super(props);
    this.state = {
      couponCode: '',
      showAlert: false,
      discountFlag: ""
    };
    this.itemTotal = 0;
    this.cartTotal = 0;
    this.discount = 0;
    this.discountPrice = 0;
    this.tax = 0;
    this.handleDelete = this.handleDelete.bind(this);
    this.calcItemTotal = this.calcItemTotal.bind(this);
    this.calcTotal = this.calcTotal.bind(this);
    this.renderTax = this.renderTax.bind(this);
    this.changeQuantity = this.changeQuantity.bind(this);
    this.handleCoupons = this.handleCoupons.bind(this);
    this.confirmOrder = this.confirmOrder.bind(this);
    this.isEmpty = this.isEmpty.bind(this)
    // this.confirmLocation = this.confirmLocation.bind(this);
    // this.changeLocation = this.changeLocation.bind(this);
    this.renderCart = this.renderCart.bind(this);
    this.renderCart = this.renderCart.bind(this);
    this.showverifyPhoneNumber = this.showverifyPhoneNumber.bind(this);
    this.validatePhone = this.validatePhone.bind(this);
    this.renderScreen = this.renderScreen.bind(this);
  }
  componentDidMount() {
    this.props.getCart();
    this.props.selectedStore && this.props.getTax(this.props.selectedStore.state_id);
  }
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return;
    };
  }
  renderScreen(screen: any) {
    this.props.navigation.navigate(screen)
  }
  isEmpty(obj: any) {
    if (obj === null) {
      return true;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
      return false;
    }
    if (obj && Object.keys(obj).length === 0) {
      return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  handleDelete(item: any, index: any) {
    let data: any = this.props.cart;
    let cart = JSON.parse(data);
    cart.splice(index, 1);
    this.props.saveCart(cart);
  };
  calcItemTotal(data: any) {
    let sum = 0;
    data.forEach((item: any) => {
      sum += parseInt(item.totalItemPrice);
    });
    this.itemTotal = sum;
    return sum;
  };
  calcTotal(data: any) {
    let { coupon, deliveryfee } = this.props;
    let sum = 0;
    data.forEach((item: any) => {
      sum += parseInt(item.totalItemPrice);
    });
    let discountValue: any = 0;
    if (!this.isEmpty(coupon)) {
      if (coupon.type == 'menu') {
        if (coupon.coupon_value !== 0) {
          this.discount = coupon.coupon_value;
          discountValue = coupon.coupon_value;
          this.discountPrice = discountValue;
        } else {
          this.discount = coupon.percent;
          discountValue = (sum * coupon.percent) / 100;
          this.discountPrice = discountValue;
        }
      } else if (coupon.type == 'group') {
        data.forEach((item: any, index: any) => {
          if (item.combo) {
            if (item.combo.group_id == coupon.type_id) {
              if (coupon.coupon_value !== 0) {
                this.discount = coupon.coupon_value;
                discountValue = coupon.coupon_value;
                this.discountPrice = discountValue;
                item.totalItemPrice = item.totalItemPrice - coupon.coupon_value;
                console.log(item)
              }
              else {
                this.discount = coupon.percent;
                discountValue = (item.totalItemPrice * coupon.percent) / 100;
                this.discountPrice = discountValue;
                item.totalItemPrice = item.totalItemPrice - discountValue;
              }
            }
          }
          if (item.item) {
            if (item.item.item_group_id == coupon.type_id) {
              if (coupon.coupon_value !== 0) {
                this.discount = coupon.coupon_value;
                discountValue = coupon.coupon_value;
                this.discountPrice = discountValue;
                item.totalItemPrice = item.totalItemPrice - coupon.coupon_value;
                console.log(item)
              }
              else {
                this.discount = coupon.percent;
                discountValue = (item.totalItemPrice * coupon.percent) / 100;
                this.discountPrice = discountValue;
                item.totalItemPrice = item.totalItemPrice - discountValue;
              }
            }
          }
        });
      }
    } else {
      this.discount = 0;
      this.cartTotal = 0;
    }
    if (this.props.orderType == 'Delivery' || this.props.orderType == "") {
      sum = sum - discountValue + deliveryfee;
    } else if (this.props.orderType == 'Pickup' || this.props.orderType == "") {
      sum = sum - discountValue;
    }
    this.cartTotal = Math.round(sum);
    return Math.round(sum);
  };
  changeQuantity(value: any, currentIndex: any) {
    let cart: any = this.props.cart;
    let dataCart = JSON.parse(cart);
    let price = dataCart[currentIndex].price;
    let actualPrice = dataCart[currentIndex].actualPrice;
    dataCart.map((obj: any, index: any) => {
      if (currentIndex == index) {
        if (value) {
          if (value > obj.quantity) {
            obj.totalItemPrice += Math.round(price);
            obj.subTotal = Math.round(value * actualPrice);
            obj.quantity = value;
          } else {
            obj.totalItemPrice -= Math.round(price);
            obj.subTotal = Math.round(value * actualPrice);
            obj.quantity = value;
          }
        } else {
          dataCart.splice(currentIndex, 1);
          this.cartTotal = 0;
        }
      }
    });
    this.props.saveCart(dataCart);
  };
  handleCoupons() {
    const { couponCode, discountFlag } = this.state;
    let { cart } = this.props;
    let data = {
      coupon_code: couponCode,
      order_mode: "mobile",
      order_channel: this.props.orderType == "Delivery" ? "delivery" : "pickup"
    };
    let Cart: any = cart;
    let dataCart = JSON.parse(Cart);
    dataCart.forEach((item: any) => {
      if (item.combo) {
        if (item.combo.discount_price) {
          this.setState({ discountFlag: true })
        }
      } else {
        if (item.selectedsize.discount_price) {
          this.setState({ discountFlag: true })
        }
      }
    })
    if (couponCode !== "" && discountFlag == false) {
      this.props.applyCoupon(data);
    } else {
      Toast.show({
        text: "Coupon cannot be applied on discounted items",
        duration: 3000,
        type: 'warning',
      });
    }
  };
  renderTax(tax: any, data: any) {
    let sum = 0;
    let taxVal = 0;
    data.forEach((item: any) => {
      sum += parseInt(item.totalItemPrice);
    });
    taxVal = Math.round((tax / 100) * sum);
    this.tax = taxVal;
    return `${taxVal} PKR`;
  }
  confirmOrder = async () => {
    let data: any = this.props.cart;
    let tax: any = this.props.taxdata ? this.props.taxdata.tax_percent : 0;
    let cart: any = JSON.parse(data);
    let discount = 0;
    const token = await AsyncStorage.getItem('token');
    cart.forEach((item: any) => {
      if (item.combo) {
        if (item.combo.discount_price) {
          discount += item.quantity * Math.round(item.combo.combo_mrp_price - item.combo.discount_price);
        }
      } else {
        if (item.selectedsize.discount_price) {
          discount += item.quantity * Math.round(item.selectedsize.mrp - item.selectedsize.discount_price);
        }
      }
    })
    this.discountPrice = discount;
    if (this.props.orderType !== "" && !this.isEmpty(this.props.selectedStore)) {
      if (this.props.orderType == "Delivery" && this.props.deliveryaddress !== "") {
        if (token) {
          if (jwtDecode(token).exp >= Date.now() / 1000) {
            let obj: any = {
              order_status_code: 1,
              order_grossprice: this.cartTotal,
              discount: this.discountPrice,
              store_id:
                this.props.selectedStore && this.props.selectedStore.store_id,
              delivery_status: this.props.orderType,
              special_inst: this.props.instructions,
              order_channel: "mobile",
              // "house/street": this.props.house + " " + this.props.street,
              cartItems: cart,
            };
            if (this.props.orderType == "Delivery") {
              obj.order_netprice = this.cartTotal - this.props.deliveryfee,
                obj.delivery_fee = this.props.deliveryfee;
            } else if (this.props.orderType == "Pickup") {
              obj.order_netprice = this.cartTotal
            }
            if (!this.isEmpty(this.props.coupon)) {
              obj.coupon = this.props.coupon;
              obj.coupon_redeem = 1;
            }
            if (this.props.deliveryaddress !== "") {
              obj.delivery_address = this.props.profileaddress !== "" ? this.props.profileaddress + " / " + this.props.deliveryaddress : this.props.deliveryaddress;
            }
            let success = await this.checkPhone();
            if (success) {
              if (this.props.orderperiod == 'later') {
                let time = this.props.datetime.time.split(':');
                let date = this.props.datetime.date.split('-');
                obj.future_status = 1;
                let future_date: any = new Date(
                  date[0],
                  date[1] - 1,
                  date[2],
                  time[0],
                  time[1],
                );
                let utc_future_date = moment(future_date).utc(false).format('YYYY-MM-DD HH:mm')
                obj.future_date = utc_future_date;
                this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
              } else if (this.props.orderperiod == 'now') {
                this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
              }
            } else {
              this.showverifyPhoneNumber()
            }
          } else {
            AsyncStorage.removeItem('token');
            this.renderScreen("guestDetail")
          }
        } else {
          if (!this.isEmpty(this.props.guestInfo)) {
            let obj: any = {
              order_status_code: 1,
              order_grossprice: this.cartTotal,
              discount: this.discountPrice,
              store_id:
                this.props.selectedStore && this.props.selectedStore.store_id,
              delivery_status: this.props.orderType,
              cartItems: cart,
              order_channel: "mobile",
              special_inst: this.props.instructions,
              // "house/street": this.props.house + " " + this.props.street,
              customer_id: this.props.guestInfo.customer_id
            };
            if (!this.isEmpty(this.props.coupon)) {
              obj.coupon = this.props.coupon;
              obj.coupon_redeem = 1;
            }
            if (this.props.orderType == "Delivery") {
              obj.order_netprice = this.cartTotal - this.props.deliveryfee,
                obj.delivery_fee = this.props.deliveryfee;
            } else if (this.props.orderType == "Pickup") {
              obj.order_netprice = this.cartTotal
            }
            if (this.props.deliveryaddress !== "") {
              obj.delivery_address = this.props.profileaddress !== "" ? this.props.profileaddress + " / " + this.props.deliveryaddress : this.props.deliveryaddress;
            }
            if (this.props.orderperiod == 'later') {
              let time = this.props.datetime.time.split(':');
              let date = this.props.datetime.date.split('-');
              obj.future_status = 1;
              let future_date: any = new Date(
                date[0],
                date[1] - 1,
                date[2],
                time[0],
                time[1],
              );
              let utc_future_date = moment(future_date).utc(false).format('YYYY-MM-DD HH:mm')
              obj.future_date = utc_future_date;
              this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
              // this.props.saveLaterOrder(obj, this.props.selectedStore, tax, this.props.navigation);
            } else if (this.props.orderperiod == 'now') {
              this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
            }
          } else {
            this.renderScreen("guestDetail")
          }
        }
      } else if (this.props.orderType == "Pickup") {
        if (token) {
          if (jwtDecode(token).exp >= Date.now() / 1000) {
            let obj: any = {
              order_status_code: 1,
              order_grossprice: this.cartTotal,
              discount: this.discountPrice,
              store_id:
                this.props.selectedStore && this.props.selectedStore.store_id,
              delivery_status: this.props.orderType,
              special_inst: this.props.instructions,
              order_channel: "mobile",
              // "house/street": this.props.house + " " + this.props.street,
              cartItems: cart,
            };
            if (!this.isEmpty(this.props.coupon)) {
              obj.coupon = this.props.coupon;
              obj.coupon_redeem = 1;
            }
            if (this.props.orderType == "Delivery") {
              obj.order_netprice = this.cartTotal - this.props.deliveryfee,
                obj.delivery_fee = this.props.deliveryfee;
            } else if (this.props.orderType == "Pickup") {
              obj.order_netprice = this.cartTotal
            }
            if (this.props.deliveryaddress !== "") {
              obj.delivery_address = this.props.profileaddress !== "" ? this.props.profileaddress + " / " + this.props.deliveryaddress : this.props.deliveryaddress;
            }
            let success = await this.checkPhone();
            if (success) {
              if (this.props.orderperiod == 'later') {
                let time = this.props.datetime.time.split(':');
                let date = this.props.datetime.date.split('-');
                obj.future_status = 1;
                let future_date: any = new Date(
                  date[0],
                  date[1] - 1,
                  date[2],
                  time[0],
                  time[1],
                );
                let utc_future_date = moment(future_date).utc(false).format('YYYY-MM-DD HH:mm')
                obj.future_date = utc_future_date;
                this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
              } else if (this.props.orderperiod == 'now') {
                this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
              }
            } else {
              this.showverifyPhoneNumber()
            }

          } else {
            AsyncStorage.removeItem('token');
            this.renderScreen("guestDetail")
          }
        } else {
          if (!this.isEmpty(this.props.guestInfo)) {
            let obj: any = {
              order_status_code: 1,
              order_grossprice: this.cartTotal,
              discount: this.discountPrice,
              store_id:
                this.props.selectedStore && this.props.selectedStore.store_id,
              delivery_status: this.props.orderType,
              cartItems: cart,
              order_channel: "mobile",
              special_inst: this.props.instructions,
              // "house/street": this.props.house + " " + this.props.street,
              customer_id: this.props.guestInfo.customer_id
            };
            if (!this.isEmpty(this.props.coupon)) {
              obj.coupon = this.props.coupon;
              obj.coupon_redeem = 1;
            }
            if (this.props.orderType == "Delivery") {
              obj.order_netprice = this.cartTotal - this.props.deliveryfee,
                obj.delivery_fee = this.props.deliveryfee;
            } else if (this.props.orderType == "Pickup") {
              obj.order_netprice = this.cartTotal
            }
            if (this.props.deliveryaddress !== "") {
              obj.delivery_address = this.props.profileaddress !== "" ? this.props.profileaddress + " / " + this.props.deliveryaddress : this.props.deliveryaddress;
            }
            if (this.props.orderperiod == 'later') {
              let time = this.props.datetime.time.split(':');
              let date = this.props.datetime.date.split('-');
              obj.future_status = 1;
              let future_date: any = new Date(
                date[0],
                date[1] - 1,
                date[2],
                time[0],
                time[1],
              );
              let utc_future_date = moment(future_date).utc(false).format('YYYY-MM-DD HH:mm')
              obj.future_date = utc_future_date;
              this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
            } else if (this.props.orderperiod == 'now') {
              this.props.saveOrder(obj, this.props.selectedStore, tax, this.props.navigation);
            }
          } else {
            this.renderScreen("guestDetail")
          }
        }
      } else {
        // this.confirmLocation();
      }
    } else {
    //   this.confirmLocation()
    }

  };
//   changeLocation() {
//     let { deliveryaddress } = this.props;
//     Geocoder.from(deliveryaddress)
//       .then((json: any) => {
//         var location = json.results[0].geometry.location;
//         this.props.navigation.navigate("confirmlocation", {
//           currentlat: location.lat,
//           currentlng: location.lng
//         })
//       })
//       .catch((error: any) => {
//         // console.warn(error));
//       })
//   };
//   confirmLocation = async () => {
//     if (Platform.OS === 'android') {
//       await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//       );
//     }
//     Geolocation.getCurrentPosition(
//       (position: any) => {
//         this.props.navigation.navigate("confirmlocation", {
//           currentlat: position.coords.latitude,
//           currentlng: position.coords.longitude
//         })
//       },
//       (error: any) => {
//         this.props.navigation.navigate("confirmlocation", {
//           currentlat: "",
//           currentlng: ""
//         })
//       },
//       {
//         enableHighAccuracy: true,
//         timeout: 5000,
//         maximumAge: 1000,
//       },
//     );
//   };
  showverifyPhoneNumber() {
    this.setState({ showAlert: true })
  }
  validatePhone() {
    this.setState({ showAlert: false });
    this.renderScreen("editprofile");
  }
  checkPhone = async () => {
    const phone = await AsyncStorage.getItem("phone");
    if (phone) {
      var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
      var mobileno = /^((\\+91-?)|0)?[0-9]{10}$/;
      if (phone.match(phoneno) || phone.match(mobileno)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };
  renderCart(data: any) {
    let { deliveryfee, taxdata, coupon } = this.props;
    if (!this.isEmpty(data)) {
      let cart = JSON.parse(data);
      if (cart.length > 0) {
        return (
          <Row>
            <Col>
              <List>
                {cart.map((obj: any, index: any) => {
                  return (
                    <ListItem thumbnail key={index}>
                      <Left>
                        <FastImage
                          style={styles.productThumbnail}
                          {...(!this.isEmpty(obj.item)
                            ? {
                              source: {
                                uri: `${BASE_URL}${obj.image}`,
                                priority: FastImage.priority.high
                              }
                            }
                            : {
                              source: {
                                uri: `${BASE_URL}${obj.image}`,
                                priority: FastImage.priority.high
                              }
                            })}
                          resizeMode={FastImage.resizeMode.contain}
                        />
                      </Left>
                      <Body style={{ marginBottom: 15 }}>
                        <Text style={styles.productTitle}>
                          {(!this.isEmpty(obj.item) && `${obj.item.item_name} x ${obj.quantity}`) ||
                            (!this.isEmpty(obj.combo) && `${obj.combo.combo_name} x ${obj.quantity}`)} <Text note>{obj.selectedsize && `(${obj.selectedsize.size})`}</Text>
                        </Text>
                        <Text note>
                          {
                            (!this.isEmpty(obj.item) && typeof obj.item.modifiers == "string") ?
                              JSON.parse(obj.item.modifiers).map((item: any) => {
                                return (
                                  `${item.modifier_name} * ${item.quantity} (${item.modifier_sale_price} PKR), `
                                )
                              }) :
                              (!this.isEmpty(obj.item) && !this.isEmpty(obj.item.modifiers)) &&
                              Object.keys(obj.item.modifiers).map(
                                (key: any, index) => {
                                  return (
                                    obj.item.modifiers[key].map(
                                      (item: any, index: any) => {
                                        return (
                                          item.selected === true &&
                                          `${item.modifier_name} * ${item.quantity} (${item.modifier_sale_price} PKR), `
                                        )
                                        // + (obj.item.modifiers[key][index + 1] ? " - " : ""
                                      }
                                    )
                                  )
                                })
                          }
                          {!this.isEmpty(obj.combo) && typeof obj.combo.modifiers == "string" ?
                            JSON.parse(obj.combo.modifiers).map((item: any) => {
                              return (
                                `${item.modifier_name} * ${item.quantity} (${item.modifier_sale_price} PKR). `
                              )
                            }) :
                            (!this.isEmpty(obj.combo) && !this.isEmpty(obj.combo.modifiers)) &&
                            Object.keys(obj.combo.modifiers).map(
                              (key: any, index) => {
                                return (
                                  obj.combo.modifiers[key].map(
                                    (item: any, index: any) => {
                                      return (
                                        item.selected === true &&
                                        `${item.modifier_name} * ${item.quantity} (${item.modifier_sale_price} PKR), `
                                        // item.modifier_name + (obj.combo.modifiers[key][index + 1] ? " - " : "")
                                      )
                                    }
                                  )
                                )
                              })}
                        </Text>
                        <NumericInput
                          value={obj.quantity}
                          totalWidth={100}
                          totalHeight={25}
                          valueType="real"
                          initValue={obj.quantity}
                          rounded
                          textColor="#bc2c3d"
                          containerStyle={{ marginTop: 10 }}
                          onChange={(value) =>
                            this.changeQuantity(value, index)
                          }
                        />
                      </Body>
                      <Right style={{ marginBottom: 15 }}>
                        <Icon
                          name="trash"
                          type="FontAwesome"
                          style={{ color: 'red', marginBottom: 5 }}
                          onPress={() => this.handleDelete(obj, index)}
                        />
                        <Text style={styles.productPrice}>
                           {obj.totalItemPrice} PKR
                        </Text>
                        <Text style={{ fontSize: 10 }}>
                          (Include Tax)
                        </Text>
                      </Right>
                    </ListItem>
                  );
                })}
                <View padder>
                  <Textarea value={this.props.instructions} style={{ borderRadius: 10 }} underline rowSpan={3} bordered placeholder="Add special cooking instructions (Optional)" onChangeText={(instructions: any) => this.props.getInst(instructions)} />
                </View>
                {this.isEmpty(coupon) && (
                  <ListItem icon>
                    <Left>
                      <Button
                        rounded
                        transparent
                        style={styles.percentIconButton}>
                        <Icon
                          name="percent"
                          type="FontAwesome"
                          style={styles.percentIcon}
                        />
                      </Button>
                    </Left>
                    <Body>
                      <Input
                        autoCapitalize="none"
                        placeholder="APPLY COUPON"
                        onChangeText={(couponCode) => this.setState({ couponCode })}
                      />
                    </Body>
                    <Right>
                      <Button transparent onPress={this.handleCoupons}>
                        <Icon name="arrow-forward" style={{ color: '#747474' }} />
                      </Button>
                    </Right>
                  </ListItem>
                )}
                <ListItem noBorder>
                  <Left>
                    <Text style={styles.itemsTotaldiscount}>Item Total</Text>
                  </Left>
                  <Text style={styles.totalvaldisval}>
                    {' '}
                    {this.calcItemTotal(cart)} PKR
                  </Text>
                </ListItem>
                {taxdata &&
                  <ListItem noBorder style={styles.itemCartTax}>
                    <Left>
                      <Text style={styles.itemsTotaldiscount}>Tax</Text>
                    </Left>

                    <Text style={{ fontSize: 10, paddingRight: 15 }}>
                      (Inclusive of Tax {taxdata ? taxdata.tax_percent : 0}% )
                      {/* i.e {this.renderTax(taxdata.tax_percent, cart)} */}
                    </Text>
                  </ListItem>
                }
                {!this.isEmpty(coupon) && (
                  <ListItem noBorder>
                    <Left>
                      <Text style={styles.totaldiscount}>Discount</Text>
                    </Left>
                    <Right>
                      <Text style={styles.totaldisval}>
                        {coupon.coupon_value !== 0 ? `${coupon.coupon_value} PKR` : `${coupon.percent} %`}
                        {/* {this.discount} % */}
                      </Text>
                    </Right>
                  </ListItem>
                )}
                {(this.props.orderType == 'Delivery' && !this.isEmpty(this.props.selectedStore)) && <ListItem>
                  <Left>
                    <Text style={styles.deliveryfee}>Delivery Fee</Text>
                  </Left>
                  <Right>
                    <Text style={styles.deliveryfee}>{deliveryfee} PKR</Text>
                  </Right>
                </ListItem>}
                <ListItem>
                  <Left>
                    <Text style={styles.total}>Total</Text>
                  </Left>
                  <Text style={{ ...styles.total, textAlign: 'right' }}>
                     {this.calcTotal(cart)} PKR
                  </Text>
                </ListItem>
                {(this.props.deliveryaddress !== "" && this.props.orderType == "Delivery") && (
                  <ListItem>
                    <Left>
                      <Icon name="map-marker"
                        type="FontAwesome"
                        style={{ color: '#999999' }} />
                      <Body>
                        <Text style={styles.deliverat}>Deliver at : </Text>
                        <Text style={styles.deliverat}>{this.props.profileaddress !== "" ? this.props.profileaddress + " / " + this.props.deliveryaddress : this.props.deliveryaddress}</Text>
                      </Body>
                    </Left>
                    {/* <Text onPress={this.changeLocation} style={{ ...styles.change, textAlign: 'right' }}>
                      Change
                  </Text> */}
                  </ListItem>)}
                {(!this.isEmpty(this.props.selectedStore) && this.props.orderType == "Pickup") && (
                  <ListItem>
                    <Left>
                      <Icon name="map-marker"
                        type="FontAwesome"
                        style={{ color: '#999999' }} />
                      <Body>
                        <Text style={styles.deliverat}>Pick from : </Text>
                        <Text style={styles.deliverat}>{this.props.selectedStore && this.props.selectedStore.address}</Text>
                      </Body>
                    </Left>
                    {/* <Text onPress={this.confirmLocation} style={{ ...styles.change, textAlign: 'right' }}>
                      Change
                 </Text> */}
                  </ListItem>
                )}
                <Button
                  block
                  large
                  style={styles.confirmButton}
                  onPress={this.confirmOrder}
                >
                  <Left>
                    <Text style={styles.confirmText}>Confirm Order</Text>
                  </Left>
                  <Right>
                    <Icon
                      name="shopping-cart"
                      type="FontAwesome"
                      style={{ color: 'white' }}
                    />
                  </Right>
                </Button>
              </List>
            </Col>
          </Row >
        );
      } else {
        return (
          <Row>
            <Col style={styles.group1}>
              <FastImage
                style={styles.cartImage}
                source={require('../assets/images/cart.png')}
                resizeMode={FastImage.resizeMode.contain}
              />
              <Text style={styles.cartEmpty}>Cart Empty</Text>
              <Text style={styles.goodfoodis}>
                Good food is always cooking! Go ahead, order some yummy food
                from the menu.
              </Text>
            </Col>
          </Row>
        );
      }
    } else {
      return (
        <Row>
          <Col style={styles.group1}>
            <FastImage
              style={styles.cartImage}
              source={require('../assets/images/cart.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.cartEmpty}>Cart Empty</Text>
            <Text style={styles.goodfoodis}>
              Good food is always cooking! Go ahead, order some yummy food from
              the menu.
            </Text>
          </Col>
        </Row>
      );
    }
  };
  render() {
    // let { loading } = this.state;
    let { cart, loader, navigation } = this.props;
    return (
      <Container>
        {loader &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <Dialog
          visible={this.state.showAlert}
          onTouchOutside={() => {
            this.setState({ showAlert: false });
          }}
          dialogTitle={<DialogTitle title="Validate Phone" />}
          footer={
            <DialogFooter>
              <DialogButton
                text="CANCEL"
                onPress={() => this.setState({ showAlert: false })}
              />
              <DialogButton
                text="OK"
                onPress={() => this.validatePhone()}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text>Your phone number is not valid , please update your phone number</Text>
          </DialogContent>
        </Dialog>

        <Topbar navigation={navigation} title="My Cart" tabs="" />

        <Content padder>
          <Grid>{this.renderCart(cart)}</Grid>
        </Content>
        <Footer navigation={navigation} active="cart" />
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    cart: state.cart.cartData,
    orderType: state.customer.orderType,
    coupon: state.cart.coupon,
    taxdata: state.cart.taxData,
    datetime: state.customer.datetime,
    deliveryfee: state.customer.deliveryfee,
    selectedStore: state.customer.selectedStore,
    orderperiod: state.customer.orderperiod,
    addresses: state.customer.addressData,
    deliveryaddress: state.customer.deliveryaddress,
    profileaddress: state.customer.profileaddress,
    guestInfo: state.customer.guestInfo,
    instructions: state.cart.instructions,
    loader: state.cart.loader,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    getCart: function () {
      dispatch(getCart());
    },
    getInst: (text: any) => {
      dispatch(getInst(text));
    },
    saveCart: function (cart: any) {
      dispatch(saveCart(cart));
    },
    saveOrder: function (data: any, store: any, tax: any, navigation: any) {
      dispatch(saveOrder(data, store, tax, navigation));
    },
    saveLaterOrder: function (data: any, store: any, tax: any, navigation: any) {
      dispatch(saveLaterOrder(data, store, tax, navigation));
    },
    applyCoupon: function (coupon: any) {
      dispatch(applyCoupon(coupon));
    },
    getTax: function (state_id: any) {
      dispatch(getTaxValue(state_id));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
