import React from 'react';
import styles from '../assets/css/orderdetail';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import { Rating } from 'react-native-ratings';
var jwtDecode = require('jwt-decode');
import {
    Container,
    Text,
    Content,
    Button,
    Textarea,
    ListItem,
    Body,
    Left,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { saveReview } from '../redux';
import AsyncStorage from '@react-native-community/async-storage';
import { FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import { BASE_URL } from '../client-config';

class Review extends React.PureComponent<
    { orderData: any, navigation: any, saveReview: (orderId: any, data: any, navigation: any) => {} }, { ratingArr: any[], feedback: any }
    > {
    constructor(props: any) {
        super(props);
        this.state = {
            ratingArr: [],
            feedback: ""
        }
        this.isEmpty = this.isEmpty.bind(this);
        this.ratingCompleted = this.ratingCompleted.bind(this);
        this.reviewsection = this.reviewsection.bind(this);
        this.renderReviews = this.renderReviews.bind(this);
    }
    ratingCompleted(rating: any, index: any) {
        let { ratingArr } = this.state;
        ratingArr[index] = rating;
        this.setState({ ratingArr: ratingArr })
    }
    isEmpty(obj: any) {
        if (obj === null) {
            return true;
        }
        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
            return false;
        }
        if (obj && Object.keys(obj).length === 0) {
            return true;
        }
        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and toValue enumeration bugs in IE < 9
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    submitReview = async () => {
        let { orderData } = this.props;
        let { ratingArr, feedback } = this.state;
        let items_feedback: any = [];
        let token: any = await AsyncStorage.getItem('token');
        var decoded: any = jwtDecode(token);
        if (ratingArr.length > 0) {
            let sum = 0;
            ratingArr.forEach((rating: any) => {
                sum += rating ? rating : 0;
            });
            let AvgRating = sum / ratingArr.length;
            orderData.orderItems.map((item: any, index: any) => {
                items_feedback.push({
                    ratings: ratingArr[index] ? ratingArr[index] : 0,
                    customer_id: decoded.customer_id,
                    menu_item_id: item.menu_item_id,
                    combo_id: item.combo_id
                })
            });
            let data = {
                ratings: AvgRating,
                feedback: feedback,
                items: items_feedback
            }
            this.props.saveReview(orderData.order.order_id, data, this.props.navigation)
        }
    }
    renderReviews({ item, index }: any) {
        return (
            <ListItem
                noBorder
                thumbnail
                key={index}
                style={{ marginVertical: 10 }}>
                <Left>
                <FastImage
                    style={styles.productThumbnail}
                    source={{
                      uri: `${BASE_URL}${item.image_url}`,
                      priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </Left>
                <Body>
                    <Text style={styles.productTitle}>
                        {item.item_name == null
                            ? item.combo_name
                            : item.item_name}
                    </Text>
                    {item.item_size && <Text note>{JSON.parse(item.item_size).size}</Text>}
                    <Text note>{item.order_item_quantity} x</Text>
                </Body>
                <Rating
                    key={index}
                    startingValue={0}
                    imageSize={30}
                    onFinishRating={(rating) => this.ratingCompleted(rating, index)}
                />
            </ListItem>
        )
    }
    reviewsection() {
        let { orderData } = this.props;
        return (
            <Grid>
                <Row>
                    <Col>
                        <Row>
                            <Col style={{ paddingHorizontal: 10 }}>
                                {!this.isEmpty(orderData) &&
                                    <FlatList
                                        data={orderData.orderItems}
                                        renderItem={this.renderReviews}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                }
                                <Textarea style={{ borderRadius: 10 }} underline rowSpan={3} bordered placeholder="Describe your experience (optional)" onChangeText={(feedback) => { this.setState({ feedback }) }} />
                                <Button
                                    block
                                    large
                                    style={styles.repeatButton}
                                    onPress={() => this.submitReview()}>
                                    <Text style={styles.repeatText}>Submit</Text>
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Grid>
        );
    };
    render() {
        let { navigation } = this.props;
        return (
            <Container>
                <Topbar navigation={navigation} title="Review" tabs="" />
                <Content padder>{this.reviewsection()}</Content>
                <Footer navigation={navigation} active="" />
            </Container>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        orderData: state.customer.orderDetail
    };
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        saveReview: function (orderId: any, review: any, navigation: any) {
            dispatch(saveReview(orderId, review, navigation));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Review);
