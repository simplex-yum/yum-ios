import React from 'react';
import styles from '../assets/css/orderdetail';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import {
  Container,
  Text,
  Content,
  Body,
  ListItem,
  Left,
  Right,
  Card,
  CardItem,
  List
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { OrderDetailProps } from '../interfaces/customer';
import {
  getCart,
  getTaxValue,
  saveCart
} from '../redux';
import { connect } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import { ActivityIndicator, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import { BASE_URL } from '../client-config';

class OrderDetail extends React.Component<OrderDetailProps, {}> {
  itemTotal: any;
  constructor(props: any) {
    super(props);
    this.renderTax = this.renderTax.bind(this);
    this.isEmpty = this.isEmpty.bind(this);
    this.calcItemTotal = this.calcItemTotal.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.repeatOrderDetail = this.repeatOrderDetail.bind(this);
    this.renderorderItems = this.renderorderItems.bind(this);
  }
  componentDidMount = () => {
    this.props.getCart();
    // this.props.getTax(this.props.selectedStore.state_id);
  };
  isEmpty(obj: any) {
    if (obj === null) {
      return true;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
      return false;
    }
    if (obj && Object.keys(obj).length === 0) {
      return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  calcItemTotal(data: any) {
    let sum = 0;
    data.forEach((item: any) => {
      sum += parseInt(item.order_item_grossprice);
    });
    this.itemTotal = sum;
    return sum;
  };
  renderTax(tax: any) {
    let taxVal = 0;
    taxVal = Math.round((tax / 100) * this.itemTotal);
    return taxVal;
  }
  handleAdd(orderData: any) {
    let cart: any = [];
    let data: any = this.props.cart;
    if ((data && data !== undefined) && (data.length !== 0)) {
      cart = JSON.parse(data);
    }
    if (!this.isEmpty(orderData)) {
      orderData.orderItems.forEach((orderItem: any) => {
        if (orderItem.menu_item_id !== null) {
          //if Item is menu_item
          if (cart.length > 0) {
            let found: any = '';
            cart.forEach((element: any) => {
              if (element['item']) {
                if (element['item'].menu_item_id == orderItem.menu_item_id) {
                  if (orderItem.item_size.size == element.selectedsize.size) {
                    element.quantity = element.quantity + 1;
                    element.totalItemPrice = element.price * element.quantity;
                    element.subTotal = element.actualPrice * element.quantity;
                    return (found = 1);
                  }
                }
              }
            });
            if (found == '') {
              let size: any = orderItem.item_size && JSON.parse(orderItem.item_size);
              let modifiers: any = orderItem.modifiers ? JSON.parse(orderItem.modifiers) : [];
              let totalItemPrice = 0;
              let subTotal = 0;
              if (modifiers.length > 0) {
                modifiers.forEach((element: any) => {
                  totalItemPrice += element.modifier_sale_price;
                  subTotal += element.modifier_sale_price;
                });
              }
              const itemcart = {
                item: orderItem,
                quantity: orderItem.order_item_quantity,
                selectedsize: size,
                price: Math.round(size.mrp) + totalItemPrice,
                totalItemPrice: (Math.round(size.mrp) + totalItemPrice) * orderItem.order_item_quantity,
                actualPrice: Math.round(size.price) + subTotal,
                subTotal: (Math.round(size.price) + subTotal) * orderItem.order_item_quantity,
                image: orderItem.image_url
              };
              // console.log(itemcart)
              cart.push(itemcart);
            }
          } else {
            let size: any =
              orderItem.item_size && JSON.parse(orderItem.item_size);
            let modifiers: any = orderItem.modifiers
              ? JSON.parse(orderItem.modifiers)
              : [];
            let totalItemPrice = 0;
            let subTotal = 0;
            if (modifiers.length > 0) {
              modifiers.forEach((element: any) => {
                totalItemPrice += element.modifier_sale_price;
                subTotal += element.modifier_sale_price;
              });
            }
            const itemcart = {
              item: orderItem,
              quantity: orderItem.order_item_quantity,
              selectedsize: size,
              price: Math.round(size.mrp) + totalItemPrice,
              totalItemPrice: (Math.round(size.mrp) + totalItemPrice) * orderItem.order_item_quantity,
              actualPrice: Math.round(size.price) + subTotal,
              subTotal: (Math.round(size.price) + subTotal) * orderItem.order_item_quantity,
              image: orderItem.image_url,
            };
            // console.log(itemcart)
            cart.push(itemcart);
          }
        } else if (orderItem.combo_id) {
          //if Item is combo item
          if (cart.length > 0) {
            let found: any = '';
            let modifier_comparison: any = true;
            cart.forEach((element: any) => {
              // console.log(element['combo'].combo_id)
              if (element['combo']) {
                if (element['combo'].combo_id == orderItem.combo_id) {
                  let cartmodifiers = element['combo'].modifiers;
                  let modifiers = cartmodifiers && JSON.parse(cartmodifiers);
                  modifiers.map((item: any) => {
                    orderItem.modifiers && JSON.parse(orderItem.modifiers).map((mod: any) => {
                      if (item.modifier_name == mod.modifier_name) {
                        item.quantity += mod.quantity;
                      } else {
                        return modifier_comparison = false;
                      }
                    });
                  })
                  element['combo'].modifiers = JSON.stringify(modifiers);
                  element.quantity = element.quantity + 1;
                  element.totalItemPrice = orderItem.order_item_grossprice + element.totalItemPrice;
                  element.subTotal = element.actualPrice * element.quantity;
                  return (found = 1);
                }
              }
            });
            if (found == '') {
              if (orderItem.is_active == 1 && orderItem.is_publish == 1) {
                let modifiers: any = orderItem.modifiers ? JSON.parse(orderItem.modifiers) : [];
                let totalItemPrice = 0;
                let subTotal = 0;
                if (modifiers.length > 0) {
                  modifiers.forEach((element: any) => {
                    totalItemPrice += (element.modifier_sale_price * element.quantity);
                    subTotal += element.modifier_sale_price;
                  });
                }
                const itemcart = {
                  combo: orderItem,
                  quantity: orderItem.order_item_quantity,
                  price: Math.round(orderItem.combo_mrp_price),
                  totalItemPrice: (Math.round(orderItem.combo_mrp_price + totalItemPrice)) * orderItem.order_item_quantity,
                  actualPrice: Math.round(orderItem.combo_sales_price) + subTotal,
                  subTotal: (Math.round(orderItem.combo_sales_price) + subTotal) * orderItem.order_item_quantity,
                  image: orderItem.image_url,
                };
                cart.push(itemcart);
              }
            }
          } else {
            if (orderItem.is_active == 1 && orderItem.is_publish == 1) {
              let modifiers: any = orderItem.modifiers ? JSON.parse(orderItem.modifiers) : [];
              let totalItemPrice = 0;
              let subTotal = 0;
              if (modifiers.length > 0) {
                modifiers.forEach((element: any) => {
                  totalItemPrice += (element.modifier_sale_price * element.quantity);
                  subTotal += element.modifier_sale_price;
                });
              }
              const itemcart = {
                combo: orderItem,
                quantity: orderItem.order_item_quantity,
                price: Math.round(orderItem.combo_mrp_price),
                totalItemPrice: (Math.round(orderItem.combo_mrp_price + totalItemPrice)) * orderItem.order_item_quantity,
                actualPrice: Math.round(orderItem.combo_sales_price) + subTotal,
                subTotal: (Math.round(orderItem.combo_sales_price) + subTotal) * orderItem.order_item_quantity,
                image: orderItem.image_url,
              };
              cart.push(itemcart);
            }
          }
        } else {
          // console.log('error');
        }
      });
    }
    // console.log(cart)
    this.props.saveCart(cart);
    this.props.navigation.navigate("cart");
  };
  renderorderItems({ item, index }: any) {
    return (
      <ListItem
        noBorder
        thumbnail
        key={index}
        style={{ marginVertical: 10 }}>
        <Left>
          <FastImage
            style={styles.productThumbnail}
            source={{
              uri: `${BASE_URL}${item.image_url}`,
              priority: FastImage.priority.high,
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </Left>
        <Body>
          <Text style={styles.productTitle}>
            {item.item_name == null
              ? item.combo_name + ` x ${item.order_item_quantity}`
              : item.item_name + ` x ${item.order_item_quantity}`}
          </Text>
          {item.item_size && <Text note>{JSON.parse(item.item_size).size}</Text>}
          <Text note>
            {(!this.isEmpty(item) && typeof item.modifiers == "string") ?
              JSON.parse(item.modifiers).map((item: any) => {
                return (
                  `${item.modifier_name} * ${item.quantity}, `
                )
              }) : ""
            }
          </Text>
        </Body>
        <Right>
          <Text>{item.order_item_grossprice} PKR</Text>
          <Text style={{ fontSize: 10 }}>
            (Include Tax)
        </Text>
        </Right>
      </ListItem>
    )
  }
  repeatOrderDetail() {
    let { orderData, taxdata } = this.props;
    return (
      <Grid>
        <Row>
          <Col>
            <Row>
              <Col style={{ paddingHorizontal: 10 }}>
                <Card transparent>
                  <CardItem header>
                    <Body>
                      <Text style={styles.orderId}>
                        #
                        {!this.isEmpty(orderData) &&
                          orderData.order.order_id}
                      </Text>
                      <Text note>
                        {!this.isEmpty(orderData) &&
                          moment(orderData.order.date_created).local().format('YYYY-MM-DD HH:mm')
                        }
                      </Text>
                    </Body>
                    <Right>
                      {(() => {
                        if (
                          !this.isEmpty(orderData) &&
                          orderData.order.order_status_description ==
                          'delivered'
                        ) {
                          return (
                            <Text style={styles.deliveredOrder}>Delivered</Text>
                          );
                        } else if (
                          !this.isEmpty(orderData) &&
                          orderData.order.order_status_description ==
                          'cancelled'
                        ) {
                          return (
                            <Text style={styles.cancelledOrder}>Cancelled</Text>
                          );
                        } else if (
                          !this.isEmpty(orderData) &&
                          orderData.order.order_status_description ==
                          'in kitchen'
                        ) {
                          return (
                            <Text style={styles.cancelledOrder}>In Kitchen</Text>
                          );
                        } else if (
                          !this.isEmpty(orderData) &&
                          orderData.order.order_status_description ==
                          'ready for delivery'
                        ) {
                          return (
                            <Text style={styles.cancelledOrder}>Ready for delivery</Text>
                          );
                        } else if (
                          !this.isEmpty(orderData) &&
                          orderData.order.order_status_description ==
                          'picked'
                        ) {
                          return (
                            <Text style={styles.cancelledOrder}>Picked</Text>
                          );
                        }
                      })()}
                    </Right>
                  </CardItem>
                  <List>
                    {!this.isEmpty(orderData) &&
                      <FlatList
                        data={orderData.orderItems}
                        renderItem={this.renderorderItems}
                        keyExtractor={(item, index) => index.toString()}
                      />
                    }
                    <ListItem noBorder>
                      <Left>
                        <Text style={styles.itemsTotaldiscount}>
                          Item Total
                        </Text>
                      </Left>
                      {/* <Text style={{ fontSize: 10 }}>(Include Tax {taxdata ? taxdata.tax_percent : 0}%) </Text> */}
                      <Text style={styles.totalvaldisval}>
                        {' '}
                        {!this.isEmpty(orderData) &&
                          orderData.orderItems &&
                          this.calcItemTotal(
                            !this.isEmpty(orderData) && orderData.orderItems,
                          )} PKR
                      </Text>
                    </ListItem>
                    {!this.isEmpty(orderData) && orderData.order.discount > 0 && (
                      <ListItem noBorder>
                        <Left>
                          <Text style={styles.itemsTotaldiscount}>
                            Discount
                          </Text>
                        </Left>
                        <Right>
                          <Text style={styles.totalvaldisval}>
                             {!this.isEmpty(orderData) && orderData.order.discount}{' '} PKR
                          </Text>
                        </Right>
                      </ListItem>
                    )}
                    {/* <ListItem noBorder style={styles.itemCartTax}>
                      <Left>
                        <Text style={styles.itemsTotaldiscount}>Tax</Text>
                      </Left>

                      <Text style={{ fontSize: 10, paddingRight: 15 }}>
                        (Include Tax {taxdata ? taxdata.tax_percent : 0}%)
                      </Text>
                    </ListItem> */}
                    {(!this.isEmpty(orderData) && orderData.order.delivery_status == 'Delivery') && <ListItem>
                      <Left>
                        <Text style={styles.deliveryfee}>Delivery Fee</Text>
                      </Left>
                      <Right>
                        <Text style={styles.deliveryfee}>
                          {!this.isEmpty(orderData) &&
                            orderData.order.delivery_fee} PKR
                        </Text>
                      </Right>
                    </ListItem>}
                    <ListItem>
                      <Left>
                        <Text style={styles.total}>Total</Text>
                      </Left>
                      <Text style={{ ...styles.total, textAlign: 'right' }}>
                        {!this.isEmpty(orderData) &&
                          orderData.order.order_grossprice} PKR
                      </Text>
                    </ListItem>
                    {!this.isEmpty(orderData) &&
                      orderData.order.ratings == null &&
                      < Text style={styles.review} onPress={() => this.props.navigation.navigate("review")}>Give your review</Text>}
                    {/* {(!this.isEmpty(orderData) && orderData.order.discount == 0) && (
                      <Button
                        block
                        large
                        style={styles.repeatButton}
                        onPress={() => this.handleAdd(orderData)}>
                        <Text style={styles.repeatText}>Repeat Order</Text>
                      </Button>
                    )} */}
                  </List>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid >
    );
  };
  render() {
    let { loading, navigation } = this.props;
    return (
      <Container>
        {loading &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <Topbar navigation={navigation} title="Order Detail" tabs="" />
        <Content padder>{this.repeatOrderDetail()}</Content>
        <Footer navigation={navigation} active="" />
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    loading: state.customer.orderDetailloading,
    orderData: state.customer.orderDetail,
    cart: state.cart.cartData,
    taxdata: state.cart.taxData,
    selectedStore: state.customer.selectedStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    // saveDeliveryFee: function (deliveryfee: any) {
    //   dispatch(saveDeliveryFee(deliveryfee));
    // },
    // saveOrderType: function (type: any) {
    //   dispatch(saveOrderType(type));
    // },
    getCart: function () {
      dispatch(getCart());
    },
    saveCart: function (cart: any[]) {
      dispatch(saveCart(cart));
    },
    getTax: function (state_id: any) {
      dispatch(getTaxValue(state_id));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail);
