import React, { PureComponent } from 'react';
import { Container, Tab, Tabs, ScrollableTab, StyleProvider, Content, Grid, Row, Col, Text, Card, CardItem, Left, Right, Button, Icon, Body, ListItem, Toast, List } from 'native-base';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import { connect } from 'react-redux';
import { favouriteAdd, saveCart } from '../redux';
import { GroupsProps } from '../interfaces/menu';
import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';
import styles from '../assets/css/groups'
import { Alert, FlatList, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image';
import {
    widthPercentageToDP as dw,
    heightPercentageToDP as dh
}
    from '../components/responsiveness'
import { BASE_URL } from '../client-config';
var jwtDecode = require('jwt-decode');

class Groups extends PureComponent<GroupsProps, { items: any[] }> {
    constructor(props: any) {
        super(props);
        this.state = {
            items: []
        }
        this.renderIngredients = this.renderIngredients.bind(this)
        this.isEmpty = this.isEmpty.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.itemsListing = this.itemsListing.bind(this);
    }
    isEmpty(obj: any) {
        if (obj === null) {
            return true;
        }
        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
            return false;
        }
        if (obj && Object.keys(obj).length === 0) {
            return true;
        }
        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and toValue enumeration bugs in IE < 9
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    renderIngredients(combo_ingredients: any) {
        let ingredientList: any = [];
        if ((combo_ingredients && combo_ingredients.length > 0)) {
            let data = JSON.parse(combo_ingredients);
            data.map((comboItem: any, index: any) => {
                ingredientList.push(comboItem.quantity + '  ' + comboItem.itemName);
            });
        }
        return (
            <Text style={styles.ingredients}>
                { ingredientList.join(' + ')}
            </Text>
        );
    };
    addFav = async (combo_id: any, menu_item_id: any, group_id: any) => {
        const token = await AsyncStorage.getItem('token');
        if (token) {
            if (jwtDecode(token).exp >= Date.now() / 1000) {
                let param1: any = {};
                if (combo_id !== "") {
                    param1 = {
                        combo_id: combo_id,
                        group_id: group_id
                    };
                } else if (menu_item_id !== "") {
                    param1 = {
                        menu_item_id: menu_item_id,
                        group_id: group_id
                    };
                }
                this.props.favouriteAdd(param1);
            } else {
                AsyncStorage.removeItem('token');
            }
        } else {
            Alert.alert(
                'Please Sign in',
                'Just sign in or register with your personal account',
                [
                    { text: 'Cancel' },
                    {
                        text: 'OK',
                        onPress: () => {
                            this.props.navigation.navigate("signin")
                        },
                    },
                ],
            );
        }
    };
    handleAdd(item: any) {
        let cart: any = [];
        let data: any = this.props.cart;
        if ((data && data !== undefined) && (data.length !== 0)) {
            let testData: any[] = JSON.parse(data);
            if (testData.length !== 0) {
                cart = JSON.parse(data);
            }
        }
        if (!this.isEmpty(item)) {
            if (item.menu_item_id) {
                // console.log(item.combo_id, item.menu_item_id)
                let sizearray: any = JSON.parse(item.item_size);
                //     //if Item is menu_item
                if (!(cart && cart.length == 0)) {
                    let found: any = '';
                    cart.forEach((element: any) => {
                        if (element['item']) {
                            if (element['item'].menu_item_id == item.menu_item_id) {
                                if (sizearray[0].size == element.selectedsize.size) {
                                    // console.log("match",sizearray[0].size)
                                    element.quantity = element.quantity + 1;
                                    element.totalItemPrice = Math.round(element.price * element.quantity);
                                    element.subTotal = Math.round(element.actualPrice * element.quantity);
                                    return (found = 1);
                                }
                            }
                        }
                    });
                    if (found == '') {
                        const itemcart = {
                            item: item,
                            quantity: 1,
                            selectedsize: sizearray[0],
                            price: (sizearray[0].discount_price && (sizearray[0].order_channel == "all" || sizearray[0].order_channel == "mobile")) ? Math.round(sizearray[0].discount_price) : Math.round(sizearray[0].mrp),
                            totalItemPrice: (sizearray[0].discount_price && (sizearray[0].order_channel == "all" || sizearray[0].order_channel == "mobile")) ? Math.round(sizearray[0].discount_price) : Math.round(sizearray[0].mrp),
                            actualPrice: Math.round(sizearray[0].price),
                            subTotal: Math.round(sizearray[0].price),
                            image: item.image_url !== "" ? item.image_url : ''
                        };
                        cart.push(itemcart);
                    }
                } else {
                    const itemcart = {
                        item: item,
                        quantity: 1,
                        selectedsize: sizearray[0],
                        price: (sizearray[0].discount_price && (sizearray[0].order_channel == "all" || sizearray[0].order_channel == "mobile")) ? Math.round(sizearray[0].discount_price) : Math.round(sizearray[0].mrp),
                        totalItemPrice: (sizearray[0].discount_price && (sizearray[0].order_channel == "all" || sizearray[0].order_channel == "mobile")) ? Math.round(sizearray[0].discount_price) : Math.round(sizearray[0].mrp),
                        actualPrice: Math.round(sizearray[0].price),
                        subTotal: Math.round(sizearray[0].price),
                        image: item.image_url !== "" ? item.image_url : ''
                    };
                    cart.push(itemcart);
                }
            } else if (item.combo_id > 0) {
                // console.log(item.combo_id)
                //     //if Item is combo item
                if (!this.isEmpty(cart)) {
                    // console.log(cart)
                    // console.log(item.combo_id, item.menu_item_id)
                    let found: any = '';
                    cart.forEach((element: any) => {
                        // console.log(element['combo'].combo_id)
                        if (element['combo']) {
                            if (element['combo'].combo_id == item.combo_id) {
                                element.quantity = element.quantity + 1;
                                element.totalItemPrice = Math.round(element.price * element.quantity);
                                element.subTotal = Math.round(element.actualPrice * element.quantity);
                                return (found = 1);
                            }
                        }
                    });
                    // console.log(found)
                    if (found == '') {
                        const itemcart = {
                            combo: item,
                            quantity: 1,
                            price: (item.discount_price && (item.order_channel == "all" || item.order_channel == "mobile")) ? Math.round(item.discount_price) : Math.round(item.combo_mrp_price),
                            totalItemPrice: (item.discount_price && (item.order_channel == "all" || item.order_channel == "mobile")) ? Math.round(item.discount_price) : Math.round(item.combo_mrp_price),
                            actualPrice: Math.round(item.combo_sales_price),
                            subTotal: Math.round(item.combo_sales_price),
                            image: item.image_url !== "" ? item.image_url : ''
                        };
                        cart.push(itemcart);
                    }
                } else {
                    const itemcart = {
                        combo: item,
                        quantity: 1,
                        price: (item.discount_price && (item.order_channel == "all" || item.order_channel == "mobile")) ? Math.round(item.discount_price) : Math.round(item.combo_mrp_price),
                        totalItemPrice: (item.discount_price && (item.order_channel == "all" || item.order_channel == "mobile")) ? Math.round(item.discount_price) : Math.round(item.combo_mrp_price),
                        actualPrice: Math.round(item.combo_sales_price),
                        subTotal: Math.round(item.combo_sales_price),
                        image: item.image_url !== "" ? item.image_url : ''
                    };
                    cart.push(itemcart);
                }
            } else {
                // console.log('error');
            }
        }
        this.props.saveCart(cart);
        Toast.show({
            text: `${item.item_name ? item.item_name : item.combo_name} Added to Cart`,
            duration: 2000,
            position: "bottom"
        });
    };
    itemsListing({ item }:any) {
        let sizearray: any[] = [];
        if (item.menu_item_id && item.menu_item_id !== "") {
            sizearray = JSON.parse(item.item_size);
            return (
                <ListItem key={item.menu_item_id} thumbnail onPress={() =>
                    this.props.navigation.navigate("itemdetail", {
                        itemId: item.menu_item_id
                    })}>
                    <Left>
                        <FastImage
                            style={styles.itemImage}
                            source={{
                                uri: `${BASE_URL}${item.image_url}`,
                                priority: FastImage.priority.high,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                    </Left>
                    <Body style={{ borderBottomWidth: 0 }}>
                        <Text style={{ fontWeight: 'bold' }}>{item.item_name}</Text>
                        <Text note>{item.item_description}</Text>
                        <Text style={styles.itemPrice}>
                            <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid', fontSize: 10 }}>
                                {(((sizearray[0].order_channel == 'all' || sizearray[0].order_channel == 'mobile') && sizearray[0].discount_price) &&
                                    sizearray[0].mrp)
                                }
                            </Text>
                            {((sizearray[0].order_channel == 'all' || sizearray[0].order_channel == 'mobile') && sizearray[0].discount_price) ? ` ${Math.round(sizearray[0].discount_price)} PKR` : `${Math.round(sizearray[0].mrp)} PKR`}
                        </Text>
                    </Body>
                    <Right style={{ borderBottomWidth: 0 }}>
                        <Icon
                            style={{ color: '#bc2c3d', fontSize: 25 }}
                            name={item.isFav == true ? 'heart' : 'heart-o'}
                            type="FontAwesome"
                            onPress={() => {
                                item.isFav !== true && this.addFav("", item.menu_item_id, item.item_group_id);
                            }}
                        />
                        <Button style={styles.addCart} onPress={() => this.handleAdd(item)}>
                            <Text>+ADD</Text>
                        </Button>
                    </Right>
                </ListItem>
            )
        } else if (item.combo_id && item.combo_id !== "") {
            return (
                <ListItem key={item.combo_id} thumbnail onPress={() =>
                    this.props.navigation.navigate("itemdetail", {
                        comboId: item.combo_id
                    })}>
                    <Left>
                        <FastImage
                            style={styles.itemImage}
                            source={{
                                uri: `${BASE_URL}${item.image_url}`,
                                priority: FastImage.priority.high,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                    </Left>
                    <Body style={{ borderBottomWidth: 0 }}>
                        <Text style={{ fontWeight: 'bold' }}>{item.combo_name}</Text>
                        <Text note>{this.renderIngredients(item.combo_ingredients)}</Text>
                        <Text style={styles.itemPrice}>
                            <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid', fontSize: 10 }}>
                                {((item.order_channel == 'all' || item.order_channel == 'mobile') && item.discount_price) && item.combo_mrp_price}
                            </Text>
                            {((item.order_channel == 'all' || item.order_channel == 'mobile') && item.discount_price) ? ` ${Math.round(item.discount_price)} PKR` : `${Math.round(item.combo_mrp_price)} PKR`}
                        </Text>
                    </Body>
                    <Right style={{ borderBottomWidth: 0 }}>
                        <Icon
                            style={{ color: '#bc2c3d', fontSize: 25 }}
                            name={item.isFav == true ? 'heart' : 'heart-o'}
                            type="FontAwesome"
                            onPress={() => {
                                item.isFav !== true && this.addFav(item.combo_id, "", item.group_id);
                            }}
                        />
                        <Button style={styles.addCart} onPress={() => this.handleAdd(item)}>
                            <Text>+ADD</Text>
                        </Button>
                    </Right>
                </ListItem>
            )
        }
    }
    render() {
        let { groups, groupsItems, route, navigation } = this.props;
        const { page } = route.params;
        return (
            <StyleProvider style={getTheme(platform)}>
                <Container>
                    <Topbar navigation={navigation} title="Explore Menu" tabs="hasTabs" />
                    <Tabs tabBarBackgroundColor='#fff' locked initialPage={page} renderTabBar={() => <ScrollableTab />}>
                        {
                            groups.map((group, index) => {
                                let Items: any = groupsItems.find((obj) => {
                                    return obj.group_id == group.group_id
                                })
                                return (
                                    <Tab key={index} heading={group.group_name} tabStyle={{ marginHorizontal: -5 }}>
                                        <Content>
                                            <Grid>
                                                <Row>
                                                    <Col>
                                                        <List>
                                                            <FlatList
                                                                data={Items.data}
                                                                renderItem={this.itemsListing}
                                                                showsVerticalScrollIndicator={false}
                                                                keyExtractor={(item, index) => index.toString()}
                                                                ListEmptyComponent={
                                                                    <Row><Col style={styles.group1}>
                                                                        <FastImage
                                                                            style={styles.foodImage}
                                                                            source={require('../assets/images/food.png')}
                                                                            resizeMode={FastImage.resizeMode.contain}
                                                                        />
                                                                        <Text style={styles.noOrderAva}>No Food</Text>
                                                                        <Text style={styles.goodfoodis}>
                                                                            Good food is always cooking! Go ahead, order some yummy food
                                  from the menu.</Text>
                                                                    </Col>
                                                                    </Row>
                                                                }
                                                            />
                                                        </List>
                                                    </Col>
                                                </Row>
                                            </Grid>
                                        </Content>
                                    </Tab>
                                )
                            })}
                    </Tabs>
                    <Footer navigation={navigation} active="" />
                </Container>
            </StyleProvider>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        groups: state.menu.allGroups,
        groupsItems: state.menu.allGroupsData,
        cart: state.cart.cartData
    };
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        saveCart: function (cart: any[]) {
            dispatch(saveCart(cart));
        },
        favouriteAdd: function (param1: any) {
            dispatch(favouriteAdd(param1));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Groups);