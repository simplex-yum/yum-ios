import React from 'react';
import styles from '../assets/css/profile';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import {
  Container,
  Button,
  Text,
  Content,
  ListItem,
  Body,
  Icon,
  Left,
  Right,
  Switch,
  Badge,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ActivityIndicator, Keyboard, View } from 'react-native';
import { connect } from 'react-redux';
import { logoutCustomer, ordersList, setloginCustomer, UpdateFCMDeviceStatus } from '../redux';
import { ProfileProps } from '../interfaces/customer';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image';
import { BASE_URL } from '../client-config';

function Guest(props: any) {
  return (
    <Grid>
      <Row>
        <Col style={styles.group1}>
          <Button transparent style={styles.editProfile}>
            <FastImage
              style={styles.avatar}
              source={require('../assets/images/userAvatar.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          </Button>
          <Text>
            <Text
              style={styles.menuIcon}
              onPress={() =>
                props.navigation.navigate("signin")
              }>
              Log in
            </Text>{' '}
            /
            <Text
              style={styles.menuIcon}
              onPress={() => {
                props.navigation.navigate("signup")
              }}>
              {' '}
              Create account
            </Text>{' '}
          </Text>
        </Col>
      </Row>
      <Row>
        <Col style={styles.group2}>
          {/* <ListItem itemDivider>
            <Text style={styles.dividerTitle}>Notifications</Text>
          </ListItem>
          <ListItem icon noBorder>
            <Left>
              <Button transparent>
                <Icon
                  name="bell-o"
                  type="FontAwesome"
                  style={styles.menuIcon}
                />
              </Button>
            </Left>
            <Body>
              <Text style={styles.menuTitle}>Push Notifications</Text>
            </Body>
            <Right>
              <Switch value={props.status == true ? true : false} onValueChange={() => props.handleClick()} />
            </Right>
          </ListItem>
           */}
          <ListItem itemDivider>
            <Text style={styles.dividerTitle}>More</Text>
          </ListItem>
          <ListItem icon noBorder onPress={() => {
            props.navigation.navigate("help")
          }}>
            <Left>
              <Button transparent>
                <Icon
                  name="question-circle-o"
                  type="FontAwesome"
                  style={styles.menuIcon}
                />
              </Button>
            </Left>
            <Body>
              <Text style={styles.menuTitle}>Help</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        </Col>
      </Row>
    </Grid>
  );
}

class Profile extends React.Component<ProfileProps, {}> {
  constructor(props: any) {
    super(props);
    this.profilePic = this.profilePic.bind(this);
    this.manageAddress = this.manageAddress.bind(this);
    this.addressData = this.addressData.bind(this);
    this.paymentMethodFunction = this.paymentMethodFunction.bind(this);
    this.orderData = this.orderData.bind(this);
    this.ordersFunction = this.ordersFunction.bind(this);
    this.orderType = this.orderType.bind(this);
    this.favouriteListFunction = this.favouriteListFunction.bind(this);
    this.offersFunction = this.offersFunction.bind(this);
    this.notification = this.notification.bind(this);
    this.updateFCMStatus = this.updateFCMStatus.bind(this);
    this.pushNotification = this.pushNotification.bind(this);
    this.moreFunction = this.moreFunction.bind(this);
    this.help = this.help.bind(this);
    this.renderScreen = this.renderScreen.bind(this);
    this.logoutFunction = this.logoutFunction.bind(this);
  }
  componentDidMount() {
    Keyboard.dismiss();
    this.props.setloginCustomer();
  };
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return;
    };
  }
  renderScreen(screen: any) {
    this.props.navigation.navigate(screen);
  }
  async updateFCMStatus() {
    let fcmToken: any = await AsyncStorage.getItem('fcmToken');
    let { fcmStatus } = this.props;
    let FCM: any = {
      token: fcmToken,
      is_active: fcmStatus == true ? 0 : 1
    }
    this.props.UpdateFCMDeviceStatus(FCM);
  }
  profilePic() {
    let { profile_pic, fileUri, name, email_address, phone_number } = this.props;
    return (
      <Col style={styles.group1}>
        <Button transparent style={styles.editProfile}>
          {(() => {
            if (profile_pic) {
              return (
                <FastImage
                  style={styles.avatar}
                  source={{
                    uri: `${BASE_URL}${profile_pic}`,
                    priority: FastImage.priority.high,
                  }}
                  resizeMode={FastImage.resizeMode.contain}
                />
              );
            } else if (fileUri) {
              return <FastImage
                style={styles.avatar}
                source={{
                  uri: fileUri,
                  priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.contain}
              />
            } else {
              return <FastImage
                style={styles.avatar}
                source={require('../assets/images/userAvatar.png')}
                resizeMode={FastImage.resizeMode.contain}
              />
            }
          })()}
          <Badge
            style={styles.editButton}
            onTouchStart={() => this.renderScreen("editprofile")}>
            <Text style={styles.editText}>
              Edit
            </Text>
          </Badge>
        </Button>
        <Text style={styles.name}>{name && name}</Text>
        <Text style={styles.email}>
          {phone_number && `${phone_number && phone_number} -`} {email_address}
        </Text>
        {/* {userInfo.town_city !== "" &&
                                            <Text style={styles.location}> <Icon name="map-marker" type="FontAwesome" style={styles.menuIcon} /> {userInfo.town_city}, {userInfo.country} </Text>
                                        } */}
      </Col>
    );
  };
  addressData() {
    this.renderScreen("address");
  }
  manageAddress() {
    return (
      <ListItem
        icon
        noBorder
        onPress={this.addressData}>
        <Left>
          <Button transparent>
            <Icon
              name="map-marker"
              type="FontAwesome"
              style={styles.menuIcon}
            />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Manage Address</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };

  paymentMethodFunction() {
    return (
      <ListItem
        icon
        noBorder
        onPress={() => {
          this.renderScreen("paymentmethods")
        }}>
        <Left>
          <Button transparent>
            <Icon
              name="credit-card"
              type="FontAwesome"
              style={styles.menuIcon}
            />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Payment Methods</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };
  orderData() {
    this.props.ordersList(this.props.navigation);
  }
  ordersFunction() {
    return (
      <ListItem
        icon
        noBorder
        onPress={this.orderData}>
        <Left>
          <Button transparent>
            <Icon
              name="shopping-bag"
              type="FontAwesome"
              style={styles.menuIcon}
            />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Orders</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };

  orderType() {
    return (
      <ListItem
        icon
        noBorder
        onPress={() => {
          this.renderScreen("ordertype")
        }}>
        <Left>
          <Button transparent>
            <Icon
              name="credit-card"
              type="FontAwesome"
              style={styles.menuIcon}
            />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Choose Order Type</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };

  favouriteListFunction() {
    return (
      <ListItem
        icon
        noBorder
        onPress={() => {
          this.renderScreen("favourites")
        }}>
        <Left>
          <Button transparent>
            <Icon name="heart-o" type="FontAwesome" style={styles.menuIcon} />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Favourite</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };

  offersFunction() {
    return (
      <ListItem
        icon
        noBorder
        onPress={() => {
          this.renderScreen("offers")
        }}>
        <Left>
          <Button rounded transparent style={styles.percentIconButton}>
            <Icon
              name="percent"
              type="FontAwesome"
              style={styles.percentIcon}
            />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Offers</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };

  notification() {
    return (
      <ListItem itemDivider>
        <Text style={styles.dividerTitle}>Notifications</Text>
      </ListItem>
    );
  };

  pushNotification() {
    let { fcmStatus } = this.props;
    return (
      <ListItem icon noBorder>
        <Left>
          <Button transparent>
            <Icon name="bell-o" type="FontAwesome" style={styles.menuIcon} />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Push Notifications</Text>
        </Body>
        <Right>
          <Switch value={fcmStatus == true ? true : false} onValueChange={() => this.updateFCMStatus()} />
        </Right>
      </ListItem>
    );
  };

  moreFunction() {
    return (
      <ListItem itemDivider>
        <Text style={styles.dividerTitle}>More</Text>
      </ListItem>
    );
  };

  help() {
    return (
      <ListItem icon noBorder onPress={() => {
        this.renderScreen("help")
      }}>
        <Left>
          <Button transparent>
            <Icon
              name="question-circle-o"
              type="FontAwesome"
              style={styles.menuIcon}
            />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Help</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  };

  logoutFunction() {
    let { navigation } = this.props;
    return (
      <ListItem icon noBorder onPress={() => this.props.logoutCustomer(navigation)}>
        <Left>
          <Button transparent>
            <Icon name="sign-out" type="FontAwesome" style={styles.menuIcon} />
          </Button>
        </Left>
        <Body>
          <Text style={styles.menuTitle}>Logout</Text>
        </Body>
      </ListItem>
    );
  };
  render() {
    let { fcmStatus, isLoggedIn, loading, navigation } = this.props;
    return (
      <Container>
        {loading &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <Topbar navigation={navigation} title="Profile" tabs="" />
        <Content padder>
          {isLoggedIn == false ? (
            <Guest handleClick={this.updateFCMStatus} navigation={navigation} status={fcmStatus} />
          ) : (
              <Grid>
                <Row>{this.profilePic()}</Row>
                <Row>
                  <Col style={styles.group2}>
                    <ListItem itemDivider>
                      <Text style={styles.dividerTitle}>My Account</Text>
                    </ListItem>
                    {this.manageAddress()}
                    {/* {this.paymentMethodFunction()} */}
                    {/* {this.ordersFunction()} */}
                    {/* {this.orderType()} */}
                    {this.favouriteListFunction()}
                    {this.offersFunction()}
                    {/* {this.notification()}
                    {this.pushNotification()} */}
                    {this.moreFunction()}
                    {this.help()}
                    {this.logoutFunction()}
                  </Col>
                </Row>
              </Grid>
            )}
        </Content>
        <Footer navigation={navigation} active="profile" />
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    userInfo: state.customer.userInfo,
    name: state.customer.name,
    email_address: state.customer.email_address,
    phone_number: state.customer.phone_number,
    profile_pic: state.customer.profile_pic,
    fileUri: state.customer.fileUri,
    loading: state.customer.profileLoad,
    fcmStatus: state.customer.fcmStatus,
    isLoggedIn: state.customer.isLoggedIn
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    logoutCustomer: (navigation: any) => {
      dispatch(logoutCustomer(navigation));
    },
    setloginCustomer: () => {
      dispatch(setloginCustomer());
    },
    UpdateFCMDeviceStatus: (FCM: any) => {
      dispatch(UpdateFCMDeviceStatus(FCM))
    },
    ordersList: function (navigation: any) {
      dispatch(ordersList(navigation));
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
