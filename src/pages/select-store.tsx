import React from 'react';
import styles from '../assets/css/ordertype';
import Topbar from '../components/Topbar/segmentTopbar';
import {
  Container,
  Button,
  Text,
  Content,
  Icon,
  List,
  ListItem,
  Body,
  Left,
  Radio,
  Right,
  Segment,
  StyleProvider,
  Form,
  Item,
  Input,
  Label,
  Toast,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import {
  addContact,
  saveDateTime,
  saveDeliveryFee,
  saveOrderPeriod,
  saveSelectStoreId,
  saveStore,
} from '../redux';
import { ActivityIndicator, Alert, Keyboard, View } from 'react-native';
import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';
import moment from 'moment';

class SelectStore extends React.PureComponent<
  {
    stores: any[];
    deliveryfee: any;
    storeStatus: any;
    orderType: any;
    loading: any;
    navigation: any;
    selectStoreId: any;
    addContact: (obj: any) => {},
    saveStore: (data: any, navigation: any) => {};
    saveSelectStoreId: (storeID: any) => {},
    saveDateTime: (datetime: any) => {};
    saveDeliveryFee: (delivery_fee: any) => {};
    saveOrderPeriod: (orderperiod: any) => {};
  },
  {
    validTimeFlag: any;
    contactnumber: any;
    activeTab: any;
    showmode: any;
    showpicker: any;
    date: any;
    time: any;
  }
  > {
  constructor(props: any) {
    super(props);
    this.state = {
      contactnumber: '',
      activeTab: 0,
      showpicker: false,
      validTimeFlag: false,
      showmode: '',
      date: '',
      time: '',
    };
    this.selectOrderPeriod = this.selectOrderPeriod.bind(this);
    this.showDateTimePicker = this.showDateTimePicker.bind(this);
    this.openingclosingtime = this.openingclosingtime.bind(this);
    this.HoursArray = this.HoursArray.bind(this);
    this.saveStoreData = this.saveStoreData.bind(this);
    this.isContactReady = this.isContactReady.bind(this);
    this.submitContact = this.submitContact.bind(this);
    this.setTime = this.setTime.bind(this);
    this.setDate = this.setDate.bind(this);
    this.validatePhone = this.validatePhone.bind(this);
    this.topButton = this.topButton.bind(this);
    this.selectStore = this.selectStore.bind(this);
  }
  selectOrderPeriod(tabIndex: any, orderperiod: any) {
    this.setState({ activeTab: tabIndex });
    this.props.saveOrderPeriod(orderperiod);
  };
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return;
    };
  }
  showDateTimePicker(currentMode: any) {
    this.setState({ showpicker: true, showmode: currentMode });
  };
  openingclosingtime(store_id: any) {
    let { stores } = this.props;
    let result = stores.find((x) => x.store_id === store_id); //find the store from the stores list
    //convert the opening time 24 hour to 12 hour
    let openingTime = result.store_open_time.split(':');
    let OpeningHours = openingTime[0];
    let OpeningAmOrPm = OpeningHours >= 12 ? 'PM' : 'AM';
    OpeningHours = OpeningHours % 12 || 12;
    let Opening = OpeningHours + ':' + openingTime[1] + ' ' + OpeningAmOrPm;
    //convert the closing time 24 hour to 12 hour
    let closingTime = result.store_close_time.split(':');
    let ClosingHours = closingTime[0];
    let ClosingAmOrPm = ClosingHours >= 12 ? 'PM' : 'AM';
    ClosingHours = ClosingHours % 12 || 12;
    let Closing = ClosingHours + ':' + closingTime[1] + ' ' + ClosingAmOrPm;
    return `${Opening} - ${Closing}`;
  };
  //Compare time make array of hours
  HoursArray(hourDiff: any, starttime: any) {
    if (String(hourDiff).indexOf('-') !== -1) {
      hourDiff = hourDiff + 24;
    }
    var hours: any = [];
    let hour = starttime;
    for (let i = 0; i <= hourDiff; i++) {
      hour = starttime + i;
      let AmOrPm = '';
      hour >= 12 && hour < 24 ? (AmOrPm = 'pm') : (AmOrPm = 'am');
      hour = hour % 12 || 12;
      hours.push(hour + AmOrPm);
    }
    var string = hours.join(':');
    return string;
  };
  saveStoreData() {
    let { stores, selectStoreId } = this.props;
    let { activeTab, date, time, validTimeFlag } = this.state;
    let result = stores.find((x: any) => x.store_id === selectStoreId); //find the store from the stores list
    let today = new Date();
    //convert the current time 24 hour to 12 hour
    let CurrentHours: any = today.getHours();
    let CurrentAmOrPm = CurrentHours >= 12 ? 'pm' : 'am';
    CurrentHours = CurrentHours % 12 || 12;
    let Currenttime: any = CurrentHours + '' + CurrentAmOrPm;
    //define hours or minutes
    var timeStartMin = new Date(
      '01/01/2007 ' + result.store_open_time,
    ).getMinutes();
    var timeEndMin = new Date(
      '01/01/2007 ' + result.store_close_time,
    ).getMinutes();
    var timeStartHours = new Date(
      '01/01/2007 ' + result.store_open_time,
    ).getHours();
    var timeEndHours = new Date(
      '01/01/2007 ' + result.store_close_time,
    ).getHours();
    var hourDiff: any = timeEndHours - timeStartHours;
    let HourString = this.HoursArray(hourDiff, timeStartHours);
    let splitHourArray = HourString.split(":");
    if (activeTab == 0) {  //if order period is now
      if (HourString.indexOf(Currenttime) !== -1) { //if current Hour with am/pm is exist in HourString
        if (timeStartMin == 0 && timeEndMin == 0) {  //if store opening minutes and closing minutes are 00
          if (splitHourArray.length - 1 == splitHourArray.indexOf(Currenttime)) {
            //if current Hour with am/pm is exist in HourString at last index
            Alert.alert('Close', `sorry! Store is closed`);
          } else {
            this.props.saveStore(result, this.props.navigation);
            this.props.saveDeliveryFee(result.delivery_fee);
            this.props.saveDateTime({ date: date, time: time });
          }
        } else {
          if (timeStartMin == 0 || timeEndMin == 0) {  //if store opening or closing minute is 00
            if (splitHourArray.length - 1 == splitHourArray.indexOf(Currenttime)) {
              if (today.getMinutes() < timeEndMin) {
                this.props.saveStore(result, this.props.navigation);
                this.props.saveDeliveryFee(result.delivery_fee);
                this.props.saveDateTime({ date: date, time: time });
                // if (deliveryfee !== 0) {
                //     this.props.saveDeliveryFee(result.delivery_fee)
                // }else{
                //     console.log('hello')
                // }
              } else {
                Alert.alert('Close', `sorry! Store is closed`);
              }
            } else {
              this.props.saveStore(result, this.props.navigation);
              this.props.saveDeliveryFee(result.delivery_fee);
              this.props.saveDateTime({ date: date, time: time });
            }
          } else { //if store opening or closing minute is not 00
            if (
              today.getMinutes() > timeStartMin ||
              today.getMinutes() < timeEndMin
            ) {
              this.props.saveStore(result, this.props.navigation);
              this.props.saveDeliveryFee(result.delivery_fee);
              this.props.saveDateTime({ date: date, time: time });
              // if (deliveryfee !== 0) {
              //     this.props.saveDeliveryFee(result.delivery_fee)
              // }else{
              //     console.log('hello')
              // }
            } else {
              Alert.alert('Close', `sorry! Store is closed`);
            }
          }
        }
      } else {
        Alert.alert('Close', `sorry! Store is closed`);
      }
    }
    //for later funtion
    else if (activeTab == 1) {  //if order period is later
      if (date !== '' && time !== '' && validTimeFlag == true) {
        let PickHours = 0;
        let PickM = 0;
        let PickTime = time.split(":");
        let PickTimeAmOrPm = PickTime[0] >= 12 ? 'pm' : 'am';
        PickHours = PickTime[0] % 12 || 12;
        PickM = PickTime[1];
        let PickHourswithampm: any = PickHours + '' + PickTimeAmOrPm;
        if (HourString.indexOf(PickHourswithampm) !== -1) { //if Pick Hour with am/pm is exist in HourString
          if (timeStartMin == 0 && timeEndMin == 0) {  //if store opening minutes and closing minutes are 00
            if (splitHourArray.length - 1 == splitHourArray.indexOf(PickHourswithampm)) {
              //if Pick Hour with am/pm is exist in HourString at last index
              Alert.alert('Close', `Please select accurate date time`);
            } else {
              this.props.saveStore(result, this.props.navigation);
              this.props.saveDeliveryFee(result.delivery_fee);
              this.props.saveDateTime({
                date: this.state.date,
                time: this.state.time,
              });
            }
          } else {
            if (timeStartMin == 0 || timeEndMin == 0) {  //if store opening or closing minute is 00
              if (splitHourArray.length - 1 == splitHourArray.indexOf(PickHourswithampm)) {
                if (PickM < timeEndMin) {
                  this.props.saveStore(result, this.props.navigation);
                  this.props.saveDeliveryFee(result.delivery_fee);
                  this.props.saveDateTime({
                    date: this.state.date,
                    time: this.state.time,
                  });
                } else {
                  Alert.alert('Close', `Please select accurate date time`);
                }
              } else {
                this.props.saveStore(result, this.props.navigation);
                this.props.saveDeliveryFee(result.delivery_fee);
                this.props.saveDateTime({
                  date: this.state.date,
                  time: this.state.time,
                });
              }
            } else { //if store opening or closing minute is not 00
              if (
                PickM > timeStartMin ||
                PickM < timeEndMin
              ) {
                this.props.saveStore(result, this.props.navigation);
                this.props.saveDeliveryFee(result.delivery_fee);
                this.props.saveDateTime({
                  date: this.state.date,
                  time: this.state.time,
                });
              } else {
                Alert.alert('Close', `Please select accurate date time`);
              }
            }
          }
        } else {
          Alert.alert('Close', `Please select accurate date time`);
        }
      } else {
        Alert.alert(`Please select date & time`);
      }
    }
  }
  isContactReady() {
    let { contactnumber } = this.state;
    let { stores } = this.props;
    return ((contactnumber !== "") || stores.length > 0);
  }
  submitContact() {
    let obj = {
      contact_number: this.state.contactnumber
    }
    this.props.addContact(obj);
  }
  setTime(event: any, date: any) {
    if (date == undefined) {
      this.setState({ showpicker: false, showmode: '' });
    } else {
      let validMinDateTime = new Date();
      validMinDateTime.setHours(validMinDateTime.getHours() + 1);
      let futureDate = moment(this.state.date).startOf('day');
      let currentDate = moment(new Date()).startOf('day');

      if (futureDate.isSame(currentDate)) {
        let futureDatetime = new Date(date);
        if (futureDatetime >= validMinDateTime) {
          this.setState({
            showpicker: false,
            showmode: '',
            time: date.getHours() + ':' + date.getMinutes(),
            validTimeFlag: true
          });
        } else {
          Toast.show({
            text: "Future order can be placed minimum 1 hour ahead of current time",
            buttonText: 'OK',
            duration: 5000,
            type: 'danger',
          });
          this.setState({ showpicker: false, showmode: '', validTimeFlag: false });
        }
      } else {
        this.setState({
          showpicker: false,
          showmode: '',
          time: date.getHours() + ':' + date.getMinutes(),
          validTimeFlag: true
        });
      }
    }
  };
  setDate(event: any, selectedDate: any) {
    if (selectedDate == undefined) {
      this.setState({ showpicker: false, showmode: '' });
    } else {
      this.setState({
        showpicker: false,
        showmode: '',
        date: selectedDate.toISOString().split('T')[0],
      });
    }
  };
  validatePhone(phone: any) {
    let maxNum = 11;
    if (phone.length > maxNum) {
      phone = phone.slice(0, maxNum);
    }
    this.setState({ contactnumber: phone });
  };

  topButton() {
    return (
      <Segment>
        <Button
          first
          {...(this.state.activeTab == 0 && { active: true })}
          onPress={() => this.selectOrderPeriod(0, 'now')}>
          <Text>Now</Text>
        </Button>
        <Button
          {...(this.state.activeTab == 1 && { active: true })}
          onPress={() => this.selectOrderPeriod(1, 'later')}>
          <Text>Later</Text>
        </Button>
      </Segment>
    );
  };

  selectStore() {
    let { stores, storeStatus, orderType, selectStoreId } = this.props;
    let {
      contactnumber,
      date,
      time,
      activeTab,
      showmode,
      showpicker
    } = this.state;
    let Today = new Date();
    let maxDate: any = Today.setDate(Today.getDate() + 1);
    return (
      <Grid>
        <Row>
          <Col>
            <List>
              <ListItem noBorder>
                {stores.length > 0 && (
                  <Text style={[styles.selecStore, { marginBottom: 15 }]}>
                    Select your store
                  </Text>
                )}
              </ListItem>
              {activeTab == 1 && stores.length > 0 && (
                <Form style={{ paddingHorizontal: 10 }}>
                  <Row>
                    <Col>
                      <Item stackedLabel>
                        <Label>Date</Label>
                        <Input
                          placeholder="select date"
                          value={date}
                          caretHidden={true}
                          onFocus={() => Keyboard.dismiss()}
                          onTouchStart={() => this.showDateTimePicker('date')}
                        />
                      </Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Item stackedLabel>
                        <Label>Time</Label>
                        <Input
                          placeholder="select time"
                          value={time}
                          caretHidden={true}
                          onFocus={() => Keyboard.dismiss()}
                          onTouchStart={() => this.showDateTimePicker('time')}
                        />
                      </Item>
                    </Col>
                  </Row>
                </Form>
              )}
              {stores.length > 0 ? (
                orderType == "Delivery" ? <ListItem
                  avatar
                  style={{ margin: 20 }}
                >
                  <Left>
                    <Icon name="building-o" type="FontAwesome" />
                  </Left>
                  <Body>
                    <Text style={{ marginBottom: 10 }}>
                      {stores[0].store_name}
                    </Text>
                    <Text note>{'\n'}{stores[0].address}</Text>
                  </Body>
                  <Right>
                    <Text note style={{ marginBottom: 10 }}>
                      {this.openingclosingtime(stores[0].store_id)}
                    </Text>
                    <Radio
                      color={'#bc2c3d'}
                      selectedColor={'#bc2c3d'}
                      selected={
                        stores[0].store_id == selectStoreId ? true : false
                      }
                    />
                    {storeStatus !== 0 && (
                      <Text style={styles.range}>Out of Range</Text>
                    )}
                  </Right>
                </ListItem>
                  :
                  stores.map((store, index) => {
                    return (
                      <ListItem
                        avatar
                        key={index}
                        style={{ margin: 20 }}
                        onPress={() => this.props.saveSelectStoreId(store.store_id)}>
                        <Left>
                          <Icon name="building-o" type="FontAwesome" />
                        </Left>
                        <Body>
                          <Text style={{ marginBottom: 10 }}>
                            {store.store_name}
                          </Text>
                          <Text note>{'\n'}{store.address}</Text>
                        </Body>
                        <Right>
                          <Text note style={{ marginBottom: 10 }}>
                            {this.openingclosingtime(store.store_id)}
                          </Text>
                          <Radio
                           onPress={() => this.props.saveSelectStoreId(store.store_id)}
                            color={'#bc2c3d'}
                            selectedColor={'#bc2c3d'}
                            selected={
                              store.store_id == selectStoreId ? true : false
                            }
                          />
                          {storeStatus !== 0 && (
                            <Text style={styles.range}>Out of Range</Text>
                          )}
                        </Right>
                      </ListItem>
                    );
                  })
              ) : (
                  <Form style={{ paddingHorizontal: 10 }}>
                    <Row>
                      <Col>
                        <Item stackedLabel>
                          <Label>Number <Text style={styles.noteText}>*</Text></Label>
                          <Input
                            keyboardType="phone-pad"
                            placeholder="Please enter your phone number"
                            autoCapitalize="none"
                            value={contactnumber}
                            onChangeText={(phone) =>
                              this.validatePhone(phone)
                            }
                          />
                        </Item>
                      </Col>
                    </Row>
                    <Row style={styles.note}>
                      <Col>
                        <Text note>
                          Note* Your location falls outside of the delivery area.
                          Please call (021) 111 626 626, our service agent will be able to
                          guide you on the options.
                      </Text>
                      </Col>
                    </Row>
                  </Form>
                )}
            </List>
            {showpicker == true && (
              <DateTimePicker
                testID="dateTimePicker"
                value={new Date()}
                mode={showmode}
                minimumDate={new Date()}
                maximumDate={maxDate}
                display="default"
                {...((showmode == 'date' && { onChange: this.setDate }) ||
                  (showmode == 'time' && { onChange: this.setTime }))}
              />
            )}
            <Button
              block
              large
              style={styles.saveButton}
              disabled={!this.isContactReady()}
              onPress={() => {
                stores.length > 0 ? this.saveStoreData() : this.submitContact();
              }}>
              <Text style={styles.saveAddress}>
                {stores.length > 0 ? 'CONTINUE' : 'Submit'}
              </Text>
            </Button>
          </Col>
        </Row>
      </Grid>
    );
  };
  render() {
    let { orderType, loading, navigation } = this.props;
    return (
      <StyleProvider style={getTheme(platform)}>
        <Container>
          {loading &&
            <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
              <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
              <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
            </View>
          }
          <Topbar navigation={navigation} {...(orderType == "Delivery" && { title: "Delivery Time" } || orderType == "Pickup" && { title: "Pickup Time" })} />
          {this.topButton()}
          <Content padder>{this.selectStore()}</Content>
        </Container>
       </StyleProvider>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    stores: state.customer.storesList,
    deliveryfee: state.customer.deliveryfee,
    storeStatus: state.customer.storeStatus,
    orderType: state.customer.orderType,
    selectStoreId: state.customer.selectStoreId,
    loading: state.customer.loading
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    saveStore: (store: any, navigation: any) => {
      dispatch(saveStore(store, navigation));
    },
    saveSelectStoreId: (storeId: any) => {
      dispatch(saveSelectStoreId(storeId));
    },
    saveDeliveryFee: (deliveryfee: any) => {
      dispatch(saveDeliveryFee(deliveryfee));
    },
    saveOrderPeriod: (orderperiod: any) => {
      dispatch(saveOrderPeriod(orderperiod));
    },
    saveDateTime: (datetime: any) => {
      dispatch(saveDateTime(datetime));
    },
    addContact: (obj: any) => {
      dispatch(addContact(obj))
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SelectStore);
