import React from 'react';
import styles from '../assets/css/order';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import { ActivityIndicator, FlatList, View } from 'react-native';
import {
  Container,
  Text,
  Content,
  Body,
  Icon,
  ListItem,
  Left,
  Thumbnail,
  Right,
  Card,
  CardItem,
  Button,
  List,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { OrderProps } from '../interfaces/customer';
import { connect } from 'react-redux';
import { orderDetail } from '../redux';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import { BASE_URL } from '../client-config';

class Order extends React.PureComponent<OrderProps, {}> {
  constructor(props: any) {
    super(props);
    this.saveOrderDetails = this.saveOrderDetails.bind(this);
    this.renderorderItems = this.renderorderItems.bind(this);
    this.orderDetailF = this.orderDetailF.bind(this);
  }
  renderorderItems({ item, index }: any) {
    return (
      <ListItem noBorder thumbnail key={index}>
        <Left>
          <Thumbnail
            resizeMode='contain'
            // source={require('../assets/images/krispy-saver-1.jpg')}
            source={{
              uri: `${BASE_URL}${item.image_url}`,
            }}
            style={styles.productThumbnail}
          />
        </Left>
        <Body>
          <Text style={styles.productTitle}>
            {item.order_item_quantity} x{' '}
            {item.item_name == null
              ? item.combo_name
              : item.item_name}
          </Text>
          {item.item_size && <Text note>{JSON.parse(item.item_size).size}</Text>}
        </Body>
      </ListItem>
    )
  }
  orderDetailF(order_id: any) {
    this.props.orderDetail(order_id, this.props.navigation);
  }
  saveOrderDetails({ item, index }: any) {
    return (
      <Row key={index}>
        <Col style={{ paddingHorizontal: 10 }}>
          <Card transparent>
            <CardItem header style={{ marginVertical: -10 }}>
              <Body>
                <Text style={styles.orderId}>#{item.order_id}</Text>
                <Text note>{moment(item.date_created).local().format('YYYY-MM-DD HH:mm')}</Text>
              </Body>
              <Right>
                <Text style={styles.orderPrice}>
                   {item.order_grossprice} PKR
                </Text>
              </Right>
            </CardItem>
            <CardItem>
              <Text style={styles.itemsQuantity}>
                {item.orderItems.length} Items
                          </Text>
            </CardItem>
            <List>
              <FlatList
                data={item.orderItems}
                renderItem={this.renderorderItems}
                keyExtractor={(item, index) => index.toString()}
              />
            </List>
            <CardItem footer style={styles.orderFooter}>
              <Left>
                <Button transparent>
                  {(() => {
                    if (
                      item.order_status_description == 'delivered' || item.order_status_description == 'picked'
                    ) {
                      return (
                        <Icon
                          name="check-circle-o"
                          type="FontAwesome"
                          style={{ color: '#31ab5f' }}
                        />
                      );
                    } else if (
                      item.order_status_description == 'cancelled'
                    ) {
                      return (
                        <Icon
                          name="close"
                          type="FontAwesome"
                          style={{ color: '#e0302d' }}
                        />
                      );
                    } else if (
                      item.order_status_description == 'received' || item.order_status_description == 'in kitchen' || item.order_status_description == 'ready for delivery'
                    ) {
                      return (
                        <Icon
                          name="clock-o"
                          type="FontAwesome"
                          style={{ color: 'lightblue' }}
                        />
                      );
                    }
                  })()}
                  {(() => {
                    if (
                      item.order_status_description == 'delivered' || item.order_status_description == 'picked'
                    ) {
                      return (
                        <Text style={styles.deliveredOrder}>
                          {item.order_status_description == 'delivered' && 'Delivered'}
                          {item.order_status_description == 'picked' && 'Picked'}
                        </Text>
                      );
                    } else if (
                      item.order_status_description == 'cancelled'
                    ) {
                      return (
                        <Text style={styles.cancelledOrder}>
                          Cancelled
                        </Text>
                      );
                    } else if (
                      item.order_status_description == 'received' || item.order_status_description == 'in kitchen' || item.order_status_description == 'ready for delivery'
                    ) {
                      return (
                        <Text style={styles.recievedOrder}>
                          {item.order_status_description == 'received' && 'Received'}
                          {item.order_status_description == 'in kitchen' && 'In Kitchen'}
                          {item.order_status_description == 'ready for delivery' && 'Ready for delivery'}
                        </Text>
                      );
                    }
                    //   else {
                    //     return (
                    //       <Text style={styles.trackOrder}>
                    //         Track Order
                    //       </Text>
                    //     );
                    //   }
                  }
                  )()}
                </Button>
              </Left>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.orderDetailF(item.order_id)
                  }}>
                  {(item.order_status_description == 'delivered' ||
                    item.order_status_description ==
                    'cancelled' || item.order_status_description == 'picked') && (
                      <>
                        <Icon
                          name="repeat"
                          type="FontAwesome"
                          style={{ color: '#bc2c3d' }}
                        />
                        <Text style={styles.trackOrder}>
                          Repeat Order
                                  </Text>
                      </>
                    )}
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Col>
      </Row>
    );
  };
  render() {
    let { loading, orders, navigation } = this.props;
    return (
      <Container>
        {loading &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <Topbar navigation={navigation} title="Orders" tabs="" />
        <Content padder>
          <Grid>
            <FlatList
              data={orders}
              initialNumToRender={10}
              renderItem={(item: any) => this.saveOrderDetails(item)}
              ListEmptyComponent={
                <Row>
                  <Col style={styles.group1}>
                    <FastImage
                      style={styles.foodImage}
                      source={require('../assets/images/food.png')}
                      resizeMode={FastImage.resizeMode.contain}
                    />
                    <Text style={styles.noOrderAva}>No Order Avaliable</Text>
                    <Text style={styles.goodfoodis}>
                      Good food is always cooking! Go ahead, order some yummy food
                      from the menu.
              </Text>
                  </Col>
                </Row>
              }
            />
          </Grid></Content>
        <Footer navigation={navigation} active="orders" />
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    orders: state.customer.ordersData,
    loading: state.customer.loading,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    orderDetail: function (orderId: any, navigation: any) {
      dispatch(orderDetail(orderId, navigation));
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Order);
