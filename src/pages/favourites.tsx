import React from 'react';
import styles from '../assets/css/favourites';
import Topbar from '../components/Topbar/notificationTopbar';
import Footer from '../components/footer';
import { ActivityIndicator, FlatList, TouchableOpacity, View } from 'react-native';
import {
  Container,
  Text,
  Content,
  Card,
  CardItem,
  Left,
  Icon,
  Right,
  ListItem,
  Body,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { favoritesList, deletefavourites } from '../redux';
import { FavState, FavProps } from '../interfaces/customer';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image';
import { BASE_URL } from '../client-config';
var jwtDecode = require('jwt-decode');
const numColumns = 2;

class Favourites extends React.Component<FavProps, FavState> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true,
      isLoggedIn: 'false',
    };
    this.deleteFavItem = this.deleteFavItem.bind(this);
    this.favourites = this.favourites.bind(this);
    this.renderFavItem = this.renderFavItem.bind(this);
    this.renderScreen = this.renderScreen.bind(this)
  }
  componentDidMount = async () => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      if (jwtDecode(token).exp >= Date.now() / 1000) {
        this.setState({
          isLoggedIn: 'true',
        });
        this.props.favoritesList();
        this.setState({ loading: false });
      } else {
        AsyncStorage.clear();
      }
    } else {
      this.props.favoritesList();
      this.setState({ loading: false });
    }
  };
  renderScreen(screen: any) {
    this.props.navigation.navigate(screen);
  }
  deleteFavItem(wish_id: any) {
    let { favItems } = this.props;
    this.props.deletefavourites(favItems, wish_id);
  };
  renderFavItem({ item, index }: any) {
    let id = item.menu_item_id == null ? { comboId: item.combo_id } : { itemId: item.menu_item_id }
    return (
      <Col key={index} style={{ padding: 5 }}>
        <Card>
          <CardItem cardBody>
            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.imgButton}
              onPress={() =>
                this.props.navigation.navigate("itemdetail", id)
              }>
              <FastImage
                style={styles.favImage}
                source={{
                  uri: `${BASE_URL}${item.image_url}`,
                  priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.contain}
              />
            </TouchableOpacity>
          </CardItem>
          <CardItem footer>
            <Body>
              <Text style={styles.favTitle}>
                {item.item_name == null
                  ? item.combo_name
                  : item.item_name}{' '}
              </Text>
              <ListItem icon noBorder>
                <Left style={{ marginLeft: -20 }}>
                  <Icon
                    name="star"
                    type="FontAwesome"
                    style={{ color: '#ffb135', fontSize: 14 }}
                  />
                  <Text style={styles.ratings}>
                    {item.stars}
                  </Text>
                </Left>
              </ListItem>
            </Body>
            <Right>
              <Icon
                name="heart"
                type="FontAwesome"
                style={styles.heartIcon}
                onPress={() => this.deleteFavItem(item.wish_id)}
              />
            </Right>
          </CardItem>
        </Card>
      </Col >
    );
  }
  favourites() {
    let { favItems } = this.props;
    let { isLoggedIn } = this.state;
    return (
      <Grid>
        <Row>
          {isLoggedIn == false ? (
            <Col style={styles.group1}>
              <FastImage
                style={styles.heartImage}
                source={require('../assets/images/heart.png')}
                resizeMode={FastImage.resizeMode.contain}
              />
              <Text style={styles.whereisthe}>Where is the Love</Text>
              <Text style={styles.onceyoufav}>
                Once you favourite a item, it will appear here.
              </Text>
            </Col>
          ) :
            <FlatList
              data={favItems}
              renderItem={this.renderFavItem}
              keyExtractor={(item, index) => index.toString()}
              numColumns={numColumns}
              ListEmptyComponent={
                <Col style={styles.group1}>
                  <FastImage
                    style={styles.heartImage}
                    source={require('../assets/images/heart.png')}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                  <Text style={styles.whereisthe}>Where is the Love</Text>
                  <Text style={styles.onceyoufav}>
                    Once you favourite a item, it will appear here.
              </Text>
                </Col>
              }
            />
          }
        </Row>
      </Grid>
    );
  };

  render() {
    let { loading } = this.state;
    let { navigation } = this.props;
    return (
      <Container>
        {loading &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <Topbar navigation={navigation} title="Favourites" />
        <Content padder>{this.favourites()}</Content>
        <Footer navigation={navigation} active="favourites" />
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    favItems: state.customer.favItemData,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    favoritesList: function () {
      dispatch(favoritesList());
    },
    deletefavourites: function (favData: any, id: any) {
      dispatch(deletefavourites(favData, id));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Favourites);
