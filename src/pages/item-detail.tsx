import React, { PureComponent } from 'react';
import styles from '../assets/css/dishdetails';
import { SliderBox } from "react-native-image-slider-box";
import {
  Container,
  Text,
  Content,
  Icon,
  ListItem,
  Left,
  List,
  Button,
  Badge,
  View,
  Body,
  Right,
  Radio,
  CheckBox,
  Item,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import {
  getItemDetail,
  getComboDetail,
  getCart,
  saveCart,
  handleModifiers,
  handleSize,
  updateModifiers,
} from '../redux';
import { ItemState, ItemProps } from '../interfaces/cart';
import { YellowBox, Alert, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import NumericInput from 'react-native-numeric-input';
import { BASE_URL } from '../client-config';
class DishDetail extends PureComponent<ItemProps, ItemState> {
  constructor(props: any) {
    super(props);
    this.state = {
      width: '',
      prevModPrice: 0,
      prevMod: "",
      prevIndex: 0,
    };
    this.isEmpty = this.isEmpty.bind(this);
    this.renderItemPrice = this.renderItemPrice.bind(this);
    this.renderIngredients = this.renderIngredients.bind(this);
    this.handleChangeChk = this.handleChangeChk.bind(this);
    this.handleRadio = this.handleRadio.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.renderImages = this.renderImages.bind(this);
    this.itemDetailFun = this.itemDetailFun.bind(this);
    this.backButton = this.backButton.bind(this);
    this.renderSizeArray = this.renderSizeArray.bind(this);
    this.renderModifiers = this.renderModifiers.bind(this);
    this.onLayout = this.onLayout.bind(this);
  }
  componentDidMount() {
    let { route } = this.props;
    const { itemId, comboId } = route.params;
    this.props.getCart();
    if (itemId) {
      // console.log("itemId",itemId)
      this.props.getItemDetail(itemId);
    } else if (comboId) {
      // console.log("comboId",comboId)
      this.props.getComboDetail(comboId);
    }
  }
  isEmpty(obj: any) {
    if (obj === null) {
      return true;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
      return false;
    }
    if (obj && Object.keys(obj).length === 0) {
      return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  renderIngredients() {
    let { comboData } = this.props;
    let ingredientList: any = [];
    if (comboData && comboData.combo_ingredients.length > 0) {
      let data = JSON.parse(comboData.combo_ingredients);
      data.map((comboItem: any, index: any) => {
        ingredientList.push(comboItem.quantity + ' ' + comboItem.itemName);
      });
    }
    return ` ${ingredientList.join(' + ')} \n ${comboData.combo_description}`;
  };
  renderItemPrice(item: any) {
    this.props.handleSize(item);
    let { modifiers } = this.props;
    !this.isEmpty(modifiers) && (
      Object.keys(modifiers).map((key) => {
        modifiers[key].map((item: any, index: any) => {
          item.selected = false;
          this.props.updateModifiers(modifiers)
        })
      }));
  };
  handleChangeChk(key: any, modName: any) {
    //To check/uncheck the modifier checkbox
    let { modifiers, itemPrice, actualPrice } = this.props;
    modifiers[key].map((item: any, index: any) => {
      if (item.modifier_name == modName) {
        if (item.selected == false) {
          item.selected = !item.selected;
          if (item.modifier_sale_price === 0) {
            this.props.handleModifiers(itemPrice + (0.01 * item.quantity), actualPrice + item.modifier_sale_price, modifiers)
          } else {
            this.props.handleModifiers(parseInt(itemPrice) + (item.modifier_sale_price * item.quantity), actualPrice + item.modifier_sale_price, modifiers)
          }
        } else {
          item.selected = !item.selected;
          if (item.modifier_sale_price === 0) {

            this.props.handleModifiers(itemPrice - (0.01 * item.quantity), actualPrice - item.modifier_sale_price, modifiers)
          } else {
            this.props.handleModifiers(parseInt(itemPrice) - (item.modifier_sale_price * item.quantity), actualPrice - item.modifier_sale_price, modifiers)
          }
        }
      }
    });
  };

  handleCheckQuantity(key: any, modName: any, value: any) {
    let { modifiers, itemPrice, actualPrice } = this.props;
    modifiers[key].map((item: any, index: any) => {
      if (item.modifier_name == modName) {
        if (value >= item.quantity) {
          item.quantity = 1 * value;
          this.props.handleModifiers(parseInt(itemPrice) + item.modifier_sale_price, actualPrice + item.modifier_sale_price, modifiers)
        } else {
          item.quantity = 1 * value;
          this.props.handleModifiers(parseInt(itemPrice) - item.modifier_sale_price, actualPrice - item.modifier_sale_price, modifiers)
        }
      }
    });

  }


  handleRadio(key: any, modName: any) {
    let { modifiers, itemPrice, actualPrice } = this.props;
    let { prevIndex, prevMod, prevModPrice } = this.state;
    modifiers[key].map((item: any, index: any) => {
      if (item.modifier_name == modName) {
        if (prevMod !== "") {
          if (modName !== prevMod) {
            item.selected = true;
            modifiers[key][prevIndex].selected = false;
            if (item.modifier_sale_price === 0) {
              this.props.handleModifiers(itemPrice - prevModPrice, actualPrice + item.modifier_sale_price, modifiers)
            } else {
              this.props.handleModifiers((parseInt(itemPrice) - prevModPrice) + item.modifier_sale_price, (actualPrice - prevModPrice) + item.modifier_sale_price, modifiers)
            }
            this.setState({
              prevMod: modName,
              prevModPrice: item.modifier_sale_price,
              prevIndex: index
            });
          }
        } else {
          item.selected = true;
          if (item.modifier_sale_price === 0) {
            this.props.handleModifiers(itemPrice - prevModPrice, actualPrice + item.modifier_sale_price, modifiers)
          } else {
            this.props.handleModifiers((parseInt(itemPrice) - prevModPrice) + item.modifier_sale_price, (actualPrice - prevModPrice) + item.modifier_sale_price, modifiers)
          }
          this.setState({
            prevMod: modName,
            prevModPrice: item.modifier_sale_price,
            prevIndex: index
          });
        }
      }

    });
  };
  handleAdd(item: any, combo: any) {
    let { selectedStore, comboData, orderType, itemPrice, actualPrice, selectedsize, modifiers, navigation } = this.props;
    if (orderType == 'Pickup' && selectedStore == '') {
      Alert.alert(
        'Please select your store firstly to pickup order from home screen',
      );
    } else {
      let cart = [];
      let data: any = this.props.cart;
      if ((data && data !== undefined) && (data.length !== 0)) {
        cart = JSON.parse(data);
      }
      // console.log(cart)
      if (!this.isEmpty(item)) {
        //if Item is menu_item
        if (cart.length > 0) {
          let found: any = '';
          cart.forEach((element: any) => {
            if (element['item']) {
              if (element['item'].menu_item_id == item.menu_item_id) {
                if (selectedsize.size == element.selectedsize.size) {
                  let cartmodifiers = element['item'].modifiers;
                  let modifier_comparison: any = true;
                  if (cartmodifiers !== undefined) {
                    Object.keys(cartmodifiers).map(key => {
                      cartmodifiers[key].map((obj: any, index: any) => {
                        Object.keys(modifiers).map(key => {
                          modifiers[key].map((item: any, index: any) => {
                            if (obj.modifier_name == item.modifier_name) {
                              if (obj.selected == item.selected) {
                                return
                              }
                              else {
                                return modifier_comparison = false;
                              }
                            }
                          });
                        });
                      })
                    })
                    if (modifier_comparison) {
                      Object.keys(cartmodifiers).map(key => {
                        cartmodifiers[key].map((obj: any, index: any) => {
                          Object.keys(modifiers).map(key => {
                            modifiers[key].map((item: any, index: any) => {
                              if (obj.modifier_name == item.modifier_name) {
                                if (obj.selected == item.selected) {
                                  obj.quantity += item.quantity;
                                }
                              }
                            });
                          });
                        })
                      });
                      element.quantity = element.quantity + 1;
                      element.totalItemPrice = itemPrice + element.totalItemPrice;
                      element.subTotal = actualPrice + element.totalItemPrice;
                      return (found = 1);
                    }
                  }
                }
              }
            }
          });
          if (found == '') {
            const itemcart = {
              item: item,
              quantity: 1,
              selectedsize: selectedsize,
              price: (selectedsize.discount_price && selectedsize.order_channel == "all" || selectedsize.order_channel == "mobile") ? Math.round(selectedsize.discount_price) : Math.round(selectedsize.mrp),
              totalItemPrice: Math.round(itemPrice),
              actualPrice: Math.round(actualPrice),
              subTotal: Math.round(actualPrice),
              image: item.images.length > 0 ? item.images[0].image_url : '',
            };
            cart.push(itemcart);
          }
        } else {
          const itemcart = {
            item: item,
            quantity: 1,
            selectedsize: selectedsize,
            price: (selectedsize.discount_price && selectedsize.order_channel == "all" || selectedsize.order_channel == "mobile") ? Math.round(selectedsize.discount_price) : Math.round(selectedsize.mrp),
            totalItemPrice: Math.round(itemPrice),
            actualPrice: Math.round(actualPrice),
            subTotal: Math.round(actualPrice),
            image: item.images.length > 0 ? item.images[0].image_url : '',
          };
          cart.push(itemcart);
        }
      } else if (!this.isEmpty(combo)) {
        //if Item is combo item
        if (cart.length > 0) {
          let found: any = '';

          cart.forEach((element: any) => {
            // console.log(element['combo'].combo_id)
            if (element['combo']) {
              if (element['combo'].combo_id == combo.combo_id) {
                let cartmodifiers: any = element['combo'].modifiers;
                let modifier_comparison = true;
                if (cartmodifiers !== undefined) {
                  Object.keys(cartmodifiers).map(key => {
                    cartmodifiers[key].map((obj: any, index: any) => {
                      Object.keys(modifiers).map(key => {
                        modifiers[key].map((item: any, index: any) => {
                          if (obj.modifier_name == item.modifier_name) {
                            if (obj.selected === item.selected) {
                              return
                            } else {
                              return modifier_comparison = false;
                            }
                          }
                        });
                      });
                    })
                  });
                  if (modifier_comparison) {
                    Object.keys(cartmodifiers).map(key => {
                      cartmodifiers[key].map((obj: any, index: any) => {
                        Object.keys(modifiers).map(key => {
                          modifiers[key].map((item: any, index: any) => {
                            if (obj.modifier_name == item.modifier_name) {
                              if (obj.selected == item.selected) {
                                obj.quantity += item.quantity;
                              }
                            }
                          });
                        });
                      })
                    });
                    element.quantity = element.quantity + 1;
                    element.totalItemPrice = Math.round(itemPrice + element.totalItemPrice);
                    element.subTotal = actualPrice + element.totalItemPrice;
                    return (found = 1);
                  }
                }
              }
            }
          });
          if (found == '') {
            const itemcart = {
              combo: combo,
              quantity: 1,
              price: (comboData.discount_price && comboData.order_channel == "all" || comboData.order_channel == "mobile") ? Math.round(comboData.discount_price) : Math.round(comboData.combo_mrp_price),
              totalItemPrice: Math.round(itemPrice),
              actualPrice: Math.round(actualPrice),
              subTotal: Math.round(actualPrice),
              image: combo.images.length > 0 ? combo.images[0].image_url : '',
            };
            cart.push(itemcart);
          }
        } else {
          const itemcart = {
            combo: combo,
            quantity: 1,
            price: (comboData.discount_price && comboData.order_channel == "all" || comboData.order_channel == "mobile") ? Math.round(comboData.discount_price) : Math.round(comboData.combo_mrp_price),
            totalItemPrice: Math.round(itemPrice),
            actualPrice: Math.round(actualPrice),
            subTotal: Math.round(actualPrice),
            image: combo.images.length > 0 ? combo.images[0].image_url : '',
          };
          cart.push(itemcart);
        }
      } else {
        // console.log('error');
      }
      this.props.saveCart(cart);
      navigation.navigate("cart");
    }
  };
  renderSizeArray({ item, index }: any) {
    let { selectedsize } = this.props;
    return (
      <ListItem key={index} onPress={() => this.renderItemPrice(item)}>
        <Left>
          <Text>
            {item.size}{' '}
            <Text
              style={{
                color: '#2c2627',
                fontFamily: 'ProximaNova-Semibold',
                fontSize: 16,
                fontWeight: '400',
                lineHeight: 25,
              }}>
              +
              <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid', fontSize: 13 }}>
                {(item.discount_price && (item.order_channel == 'all' || item.order_channel == 'mobile')) && `  ${Math.round(item.mrp)}`}
              </Text>
              {(item.discount_price && (item.order_channel == 'all' || item.order_channel == 'mobile')) ? ` ${Math.round(item.discount_price)} PKR` : ` ${Math.round(item.mrp)} PKR`}
            </Text>
          </Text>
        </Left>
        <Right>
          <Radio
            color={'#bc2c3d'}
            selectedColor={'#bc2c3d'}
            selected={item.size == selectedsize.size}
          />
        </Right>
      </ListItem>
    );
  };
  renderModifiers({ item, index }: any, key: any) {
    if (item.modifier_type == "multiple") {
      return (
        <View style={{ paddingVertical: 10 }}>
          <ListItem icon key={index} onPress={() =>
            this.handleChangeChk(key, item.modifier_name)
          }>
            <Left>
              <CheckBox
                onPress={() =>
                  this.handleChangeChk(key, item.modifier_name)
                }
                checked={item.selected}
                style={styles.modifiers}
                color={'#bc2c3d'}
              />
            </Left>
            <Body>
              <Text>
                {item.modifier_name}{' '}
                <Text
                  style={styles.modifierText}>
                  {item.modifier_sale_price === 0 ? null : `+ ${item.modifier_sale_price} PKR`}
                </Text>
              </Text>
            </Body>
            {item.selected === true &&
              <Right>
                <NumericInput
                  value={item.quantity}
                  totalWidth={100}
                  totalHeight={25}
                  valueType="real"
                  initValue={item.quantity}
                  minValue={1}
                  rounded
                  editable={false}
                  textColor="#bc2c3d"
                  containerStyle={{ marginTop: 10 }}
                  onChange={(value) =>
                    this.handleCheckQuantity(key, item.modifier_name, value)
                  }
                />
              </Right>
            }
          </ListItem>
        </View>
      )
    } else {
      return (
        <ListItem key={index} onPress={() =>
          this.handleRadio(key, item.modifier_name)
        }>
          <Radio
            onPress={() =>
              this.handleRadio(key, item.modifier_name)
            }
            selected={item.selected}
            style={styles.modifiers}
            color={'#bc2c3d'}
            selectedColor={"#bc2c3d"}
          />
          <Body>
            <Text>
              {item.modifier_name}{' '}
              <Text
                style={styles.modifierText}>
                {item.modifier_sale_price === 0 ? null : `+ ${item.modifier_sale_price} PKR`}
              </Text>
            </Text>
          </Body>
        </ListItem>
      )
    }
  }
  itemDetailFun() {
    let { itemData, comboData, count, selectedStore, selectedsize, orderType, itemPrice, modifiers, sizearray } = this.props;
    return (
      <Grid>
        <Row>
          <Col style={{ marginTop: 10 }}>
            <List>
              <ListItem noBorder>
                <Left>
                  <Text
                    style={styles.itemName}>
                    {(!this.isEmpty(itemData) && itemData.item_name) ||
                      (!this.isEmpty(comboData) && comboData.combo_name)}
                  </Text>
                </Left>
                <Text style={styles.itemPrice}>
                  <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid' }}>
                    {(!this.isEmpty(comboData) && (comboData.order_channel == 'all' || comboData.order_channel == 'mobile') && comboData.discount_price) && comboData.combo_mrp_price ||
                      ((!this.isEmpty(itemData) && (selectedsize.order_channel == 'all' || selectedsize.order_channel == 'mobile') && selectedsize.discount_price) &&
                        selectedsize.mrp)
                    }
                  </Text>
                  {(!this.isEmpty(itemData) && ` ${Math.round(itemPrice)} PKR`) ||
                    (!this.isEmpty(comboData) &&
                      ` ${Math.round(itemPrice)} PKR`)}
                </Text>
              </ListItem>
              <ListItem noBorder style={{ marginTop: -20 }}>
                {!this.isEmpty(selectedStore) &&
                  <>
                    <Left>
                      <Text
                        style={{
                          color: '#999999',
                          fontFamily: 'ProximaNova-Regular',
                          fontSize: 15,
                          fontWeight: '400',
                        }}>
                        <Icon
                          name="map-marker"
                          type="FontAwesome"
                          style={{ color: '#999999' }}
                        />
                        {selectedStore ? ` ${selectedStore.address}` : ' store'}
                      </Text>
                    </Left>
                    <Button
                      rounded
                      transparent
                      style={{
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 5,
                        },
                        shadowOpacity: 0.34,
                        shadowRadius: 6.27,
                        elevation: 2,
                      }}>
                      <Icon
                        name="location-arrow"
                        type="FontAwesome"
                        style={{ color: '#bc2c3d', fontSize: 20 }}
                      />
                    </Button>
                  </>}
              </ListItem>
              {(orderType == 'Delivery' || orderType == 'Pickup') &&
                <ListItem noBorder>
                  <Left>
                    <Badge
                      style={{
                        backgroundColor: '#bc2c3d',
                        borderRadius: 20,
                      }}>
                      <Text
                        style={{
                          fontFamily: 'ProximaNova-Semibold',
                          fontSize: 9,
                          fontWeight: '400',
                          lineHeight: 12,
                          color: '#fff',
                          marginHorizontal: 15,
                        }}>
                        {orderType == 'Pickup'
                          ? 'pick from store'
                          : 'delivery'}
                      </Text>
                    </Badge>
                  </Left>
                </ListItem>
              }
            </List>
            <Button
              block
              style={styles.addCart}
              onPress={() => this.handleAdd(itemData, comboData)}>
              <Left>
                <Text style={styles.addCartText}>Add to Cart</Text>
              </Left>
              <Right>
                <Text style={styles.addCartText}>
                  {(!this.isEmpty(itemData) && `${Math.round(itemPrice)} PKR`) ||
                    (!this.isEmpty(comboData) &&
                      `${Math.round(itemPrice)} PKR`)}
                </Text>
              </Right>
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <View
                    style={[styles.counterStyle, { backgroundColor: '#ffb135' }]}>
                    <Icon
                      name="star-o"
                      type="FontAwesome"
                      style={styles.iconStyle}
                    />
                  </View>
                </Left>
                <Body>
                  <Text>{count.Rating}</Text>
                  <Text note>Rating</Text>
                </Body>
              </ListItem>
            </List>
          </Col>
          <Col>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <View
                    style={[
                      styles.counterStyle,
                      {
                        backgroundColor: '#bc2c3d',
                      },
                    ]}>
                    <Icon
                      name="heart-o"
                      type="FontAwesome"
                      style={styles.iconStyle}
                    />
                  </View>
                </Left>
                <Body>
                  <Text>{count.Favourite}</Text>
                  <Text note>Favourite</Text>
                </Body>
              </ListItem>
            </List>
          </Col>
        </Row>
        {/* <Row>
          <Col>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <View
                    style={[
                      styles.counterStyle,
                      {
                        backgroundColor: '#2c2627',
                      },
                    ]}>
                    <Icon
                      name="image"
                      type="FontAwesome"
                      style={styles.iconStyle}
                    />
                  </View>
                </Left>
                <Body>
                  <Text>{count.Photo}</Text>
                  <Text note>Photo</Text>
                </Body>
              </ListItem>
            </List>
          </Col>
        </Row> */}
        <Row>
          <Col>
            <Content padder>
              <Text style={styles.detailHead}>Details</Text>
              <Text style={styles.details}>
                {(!this.isEmpty(itemData) && itemData.item_description) ||
                  (!this.isEmpty(comboData) && this.renderIngredients())}
              </Text>
            </Content>
          </Col>
        </Row>
        {(!this.isEmpty(itemData) && sizearray.length > 1) && (
          <Row>
            <Col>
              <Content padder>
                <Text style={styles.detailHead}>Select Size</Text>
                <FlatList
                  data={sizearray}
                  renderItem={(item: any) => this.renderSizeArray(item)}
                />
              </Content>
            </Col>
          </Row>
        )}
        {((!this.isEmpty(itemData) || !this.isEmpty(comboData)) && modifiers) && (
          <Row>
            <Col>
              <Content padder>
                {Object.keys(modifiers).map((key) => {
                  return (
                    <View key={key}>
                      <Text style={styles.detailHead}>{key}</Text>
                      <FlatList
                        data={modifiers[key]}
                        renderItem={(item: any) => this.renderModifiers(item, key)}
                      />
                    </View>
                  );
                })}
              </Content>
            </Col>
          </Row>
        )}
      </Grid>
    );
  };
  backButton() {
    return (
      <Item style={styles.backButtonStyle}>
        <Button transparent onPress={() => { this.props.navigation.goBack() }}>
          <Icon
            name="arrow-back"
            style={{ color: 'black' }}
          />
        </Button>
      </Item>
    )
  }
  onLayout(e: any) {
    this.setState({
      width: e.nativeEvent.layout.width
    });
  };
  renderImages(data: any) {
    if (!this.isEmpty(data)) {
      let Images: any = [];
      if (data.images.length > 0) {
        data.images.length > 0 && data.images.map((item: any, index: any) => {
          Images.push(`${BASE_URL}${item.image_url}`)
        })
        return (
          <View onLayout={this.onLayout}>
            <SliderBox images={Images}
              ImageComponent={FastImage}
              sliderBoxHeight={300}
              dotColor="#bc2c3d"
              autoplay
              circleLoop
              ImageComponentStyle={{
                position: 'relative',
                marginVertical: 40
              }}
              parentWidth={this.state.width}
              resizeMode={'contain'}
            />
          </View>
        );
      }
    }
  };
  render() {
    let { itemData, comboData } = this.props;
    return (
      <Container style={{ flex: 1 }}>
        <View style={styles.imagesView}>
          {this.renderImages(
            (!this.isEmpty(itemData) && itemData) ||
            (!this.isEmpty(comboData) && comboData),
          )}
          {this.backButton()}
        </View>
        <View style={styles.group1}>
          <Content padder>{this.itemDetailFun()}</Content>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    itemData: state.menu.itemData,
    comboData: state.menu.comboData,
    cart: state.cart.cartData,
    count: state.menu.countData,
    itemPrice: state.menu.itemPrice,
    actualPrice: state.menu.actualPrice,
    modifiers: state.menu.modifiers,
    selectedsize: state.menu.selectedsize,
    sizearray: state.menu.sizearray,
    selectedStore: state.customer.selectedStore,
    orderType: state.customer.orderType,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    getItemDetail: function (id: any) {
      dispatch(getItemDetail(id));
    },
    getComboDetail: function (id: any) {
      dispatch(getComboDetail(id));
    },
    getCart: function () {
      dispatch(getCart());
    },
    saveCart: function (cart: any[]) {
      dispatch(saveCart(cart));
    },
    handleModifiers: (itemPrice: any, actualPrice: any, modifiers: any) => {
      dispatch(handleModifiers(itemPrice, actualPrice, modifiers))
    },
    updateModifiers: (modifiers: any) => {
      dispatch(updateModifiers(modifiers))
    },
    handleSize: (item: any) => {
      dispatch(handleSize(item))
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DishDetail);
