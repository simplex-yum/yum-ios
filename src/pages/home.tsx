import React, { PureComponent } from 'react';
import styles from '../assets/css/home';
// import SplashScreen from 'react-native-splash-screen';
import Footer from '../components/footer';
import { HomeState, HomeProps } from '../interfaces/menu';
import { SliderBox } from "react-native-image-slider-box";
// import Geolocation from 'react-native-geolocation-service';
import {
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  FlatList,
  Platform,
  PermissionsAndroid,
  ActivityIndicator
} from 'react-native';
import {
  Container,
  Text,
  Content,
  Icon,
  Right,
  Card,
  CardItem,
  Body,
  ListItem,
  Left,
  Item,
  Button,
  Badge,
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  menuCombosList,
  favouriteAdd
} from '../redux';
import { connect } from 'react-redux';
var jwtDecode = require('jwt-decode');
import AsyncStorage from '@react-native-community/async-storage';
// import Geocoder from 'react-native-geocoding';
import { BASE_URL, GOOGLE_API } from '../client-config';
import FastImage from 'react-native-fast-image';
// Geocoder.init(GOOGLE_API);
const numColumns = 2;
class Home extends PureComponent<HomeProps, HomeState> {
  //React.PureComponent is used to prevent extra rendering
  // It re-renders only if there is shallow change in props or state.
  constructor(props: any) {
    super(props);
    this.state = {
      group_id: "",
      Address: null,
      searchText: '',
      width: ''
    };
    this.isEmpty = this.isEmpty.bind(this);
    this.notfound = this.notfound.bind(this);
    this.renderIngredients = this.renderIngredients.bind(this);
    this.combosListing = this.combosListing.bind(this);
    this.groupListing = this.groupListing.bind(this);
    this.renderGroups = this.renderGroups.bind(this);
    this.selectStore = this.selectStore.bind(this);
    this.renderCartButton = this.renderCartButton.bind(this);
    this.topRightBar = this.topRightBar.bind(this);
    this.clickGroup = this.clickGroup.bind(this);
    this.renderScreen = this.renderScreen.bind(this);
    // this.handleHeroItemsClick = this.handleHeroItemsClick.bind(this);
  }
  componentDidMount = async () => {
    // SplashScreen.hide();
    this.props.menuCombosList();
  };
  renderScreen(screen: any) {
    this.props.navigation.navigate(screen);
  }
  isEmpty(obj: any) {
    if (obj === null) {
      return true;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
      return false;
    }
    if (obj && Object.keys(obj).length === 0) {
      return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  notfound() {
    return (
      <Content padder>
        <Grid>
          <Row>
            <Col>
              <Text style={styles.notfound}>
                Not Found! Please Select your store first.
              </Text>
            </Col>
          </Row>
        </Grid>
      </Content>
    );
  };
  addFav = async (combo_id: any, menu_item_id: any, topDeal?: any) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      if (jwtDecode(token).exp >= Date.now() / 1000) {
        let { selectedStore } = this.props;
        let param1: any = {};
        let param2: any = {};
        if (selectedStore !== '') {
          param2 = {
            brand_id: selectedStore.brand_id,
            state_id: selectedStore.state_id
          }
        }
        if (combo_id !== "") {
          param1 = {
            combo_id: combo_id,
          };
          if (topDeal) {
            param1.topDeal = true
          }
        } else if (menu_item_id !== "") {
          param1 = {
            menu_item_id: menu_item_id,
            group_id: this.state.group_id
          };
        }
        this.props.favouriteAdd(param1, param2);
      } else {
        AsyncStorage.removeItem('token');
      }
    } else {
      Alert.alert(
        'Please Sign in',
        'Just sign in or register with your personal account',
        [
          { text: 'Cancel' },
          {
            text: 'OK',
            onPress: () => {
              this.renderScreen("signin")
            },
          },
        ],
      );
    }
  };
  renderIngredients(combo_ingredients: any) {
    let ingredientList: any = [];
    if (combo_ingredients && combo_ingredients.length > 0) {
      let data = JSON.parse(combo_ingredients);
      data.map((comboItem: any, index: any) => {
        ingredientList.push(comboItem.quantity + '  ' + comboItem.itemName);
      });
    }
    return (
      <Text style={styles.ingredients}>
        {ingredientList.join('+').length > 20
          ? ingredientList.join('+').substring(0, 20) + '...'
          : ingredientList.join('+')}
      </Text>
    );
  };
  clickGroup(index: any) {
    this.props.navigation.navigate("groups", {
      page: index
    });
  }
  combosListing({ item }: any) {
    return (
      <Grid>
        <Row>
          <Col style={{ paddingHorizontal: 5 }}>
            <Card>
              <CardItem cardBody>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'stretch',
                  }}
                  onPress={() =>
                    this.props.navigation.navigate("itemdetail", {
                      comboId: item.combo_id
                    })
                  }>
                  <FastImage
                    style={styles.itemImage}
                    source={{
                      uri: `${BASE_URL}${item.image_url}`,
                      priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </TouchableOpacity>
              </CardItem>
              <CardItem footer>
                <Body>
                  <Text>{item.combo_name}</Text>
                  {this.renderIngredients(item.combo_ingredients)}
                  <ListItem icon noBorder>
                    <Left style={{ marginBottom: 20, marginLeft: -20 }}>
                      {/* <Icon
                        name="star"
                        type="FontAwesome"
                        style={{ color: '#ffb135', fontSize: 14 }}
                      /> */}
                      <Text style={styles.rating}>
                        {item.stars} ({item.ratings} ratings)
                            </Text>
                    </Left>
                  </ListItem>
                </Body>
                <Right>
                  <Icon
                    style={{ color: '#bc2c3d', fontSize: 25 }}
                    name={item.isFav == true ? 'heart' : 'heart-o'}

                    type="FontAwesome"
                    onPress={() => {
                      item.isFav !== true && this.addFav(item.combo_id, "", this.props.topDeals.length > 0 && true);
                    }}
                  />
                  <Text style={styles.itemPrice}>
                    <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid', fontSize: 12 }}>
                      {((item.order_channel == 'all' || item.order_channel == 'mobile') && item.discount_price) && item.combo_mrp_price}
                    </Text>
                    {((item.order_channel == 'all' || item.order_channel == 'mobile') && item.discount_price) ? ` ${Math.round(item.discount_price)} PKR` : `${Math.round(item.combo_mrp_price)} PKR`}</Text>
                </Right>
              </CardItem>
            </Card>
          </Col>
        </Row>
      </Grid >
    )
  }
  renderGroups({ item, index }: any) {
    return (
      <Col style={{ padding: 5 }} key={index}>
        <Text style={styles.menuHeadings}>{item.group_name.indexOf('/') > 0 ? item.group_name.split('/')[0] : item.group_name}</Text>
        <Card>
          <CardItem cardBody>
            <TouchableOpacity
              activeOpacity={0.5}
              style={{
                flexDirection: 'row',
              }}
              onPress={() =>
                this.clickGroup(index)
              }>
              <FastImage
                style={styles.groupThumbnail}
                source={{
                  uri: `${BASE_URL}${item.group_image}`,
                  priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.contain}
              />
            </TouchableOpacity>
          </CardItem>
        </Card>
      </Col>
    )
  }
  groupListing() {
    let { groups } = this.props;
    return (
      <View style={{ paddingHorizontal: 10 }}>
        <Item style={{ borderBottomWidth: 0, marginVertical: 20 }}>
          <Text style={styles.menuHeadings}>EXPLORE MENU</Text>
          <Right>
            <Text style={styles.menuHeadings} onPress={() =>
              this.props.navigation.navigate("groups", {
                page: 0
              })}>
              VIEW ALL
            </Text>
          </Right>
        </Item>
        <Grid>
          <Row>
            <FlatList
              data={groups}
              renderItem={this.renderGroups}
              keyExtractor={(item, index) => index.toString()}
              numColumns={numColumns}
            />
          </Row>
        </Grid>
      </View>
    );
  };
  selectStore() {
    return (
      <Item onPress={() => this.saveLocation()} style={styles.selectLocationStyle}>
        <Icon name="map-marker"
          type="FontAwesome"
          style={{ color: '#999999' }} />
        <Text>Select Location</Text>
      </Item>
    )
  }
  renderCartButton(data: any) {
    if ((data && data !== undefined) && (data.length !== 0)) {
      let cart = JSON.parse(data);
      let totalItemQuantity = 0;
      cart.map((item:any) => {
        totalItemQuantity += item.quantity; 
      })
      if (cart.length > 0) {
        return (
          <Button
            transparent
            style={styles.cartButton}
            onPress={() => {
              this.renderScreen("cart")
            }}>
            <Icon
              type="FontAwesome"
              name="shopping-cart"
              style={{ color: 'black' }}
            />
            <Badge
              style={{
                marginLeft: 30,
                width: 35,
                height: 23,
                position: 'absolute',
              }}>
              <Text style={{ color: '#fff' }}>{totalItemQuantity > 9 ? `${9}+` : totalItemQuantity}</Text>
            </Badge>
          </Button>
        );
      } else {
        return (
          <Button
            transparent
            style={styles.cartButton}
            onPress={() => {
              this.renderScreen("cart")
            }}>
            <Icon
              type="FontAwesome"
              name="shopping-cart"
              style={{ color: 'black' }}
            />
          </Button>
        );
      }
    } else {
      return (
        <Button
          transparent
          style={styles.cartButton}
          onPress={() => {
            this.renderScreen("cart")
          }}>
          <Icon
            type="FontAwesome"
            name="shopping-cart"
            style={{ color: 'black' }}
          />
        </Button>
      );
    }
  };
  topRightBar() {
    const { cart, notifications } = this.props;
    return (
      <View style={styles.topRightCorner}>
        <Button transparent style={{ right: 6 }} onPress={() => {
          this.renderScreen("notification")
        }}>
          <Icon name="bell-o" type="FontAwesome" style={{ color: 'black' }} />
          {notifications.length > 0 && <Badge
            style={{
              marginLeft: 30,
              width: notifications.length + 25,
              height: 23,
              position: 'absolute',
            }}>
            <Text style={{ color: '#fff' }}>{notifications.length}</Text>
          </Badge>
          }
        </Button >
        {this.renderCartButton(cart)}
      </View>
    )
  }
  saveLocation = async () => {
    let { deliveryaddress } = this.props;
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }
    if (deliveryaddress !== "") {
    //   Geocoder.from(deliveryaddress)
    //     .then((json: any) => {
    //       var location = json.results[0].geometry.location;
    //       this.props.navigation.navigate("confirmlocation", {
    //         currentlat: location.lat,
    //         currentlng: location.lng
    //       })
    //     })
    //     .catch((error: any) => {
    //       // console.warn(error));
    //     })
    } else {
    //   Geolocation.getCurrentPosition(
    //     (position: any) => {
    //       this.props.navigation.navigate("confirmlocation", {
    //         currentlat: position.coords.latitude,
    //         currentlng: position.coords.longitude
    //       })
    //     },
    //     (error: any) => {
    //       this.props.navigation.navigate("confirmlocation", {
    //         currentlat: "",
    //         currentlng: ""
    //       })
    //     },
    //     {
    //       enableHighAccuracy: true,
    //       timeout: 4000,
    //       maximumAge: 1000,
    //     },
    //   );
    }
};
  handleHeroItemsClick(heroIndex: any) {
    let { heroItems } = this.props;
    let heroItem: any = heroItems.find((obj, index) => {
      return index == heroIndex
    })
    if (heroItem.menu_item_id) {
      this.props.navigation.navigate("itemdetail", {
        itemId: heroItem.menu_item_id
      })
    } else if (heroItem.combo_id) {
      this.props.navigation.navigate("itemdetail", {
        comboId: heroItem.combo_id
      })
    }
  }
  onLayout = (e: any) => {
    this.setState({
      width: e.nativeEvent.layout.width
    });
  };
  render() {
    let { selectedStore, loading, combos, topDeals, heroItems, navigation } = this.props;
    let Images: any = [];
    let bestSeller: any = topDeals.length > 0 ? topDeals : combos
    heroItems.length > 0 && heroItems.map((item: any, index: any) => {
      Images.push(`${BASE_URL}${item.image}`)
    });
    return (
      <Container style={{ flex: 1 }}>
        {loading &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <ScrollView>
         <View style={{ flex: 1, height: 250,marginTop:10 }} onLayout={this.onLayout}>  
            <SliderBox images={Images}
              sliderBoxHeight={300}
              ImageComponent={FastImage}
              dotColor="#bc2c3d"
              autoplayInterval={15000}
              onCurrentImagePressed={(index: any) => this.handleHeroItemsClick(index)}
              autoplay
              circleLoop
              ImageComponentStyle={{
                position: 'relative',
              }}
              parentWidth={this.state.width}
              resizeMode={'contain'}
            />
            {this.selectStore()}
            {this.topRightBar()}
          </View>
          {selectedStore !== '' ? (
            <Content padder>
              {(combos.length > 0 || topDeals.length > 0) && <View style={{ paddingHorizontal: 15, marginTop: 15 }}>
                <Item style={{ borderBottomWidth: 0 }}>
                  <Text style={styles.menuHeadings}>BESTSELLERS</Text>
                </Item>
                <SafeAreaView>
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={bestSeller}
                    keyExtractor={(item, index) => index.toString()}
                    initialNumToRender={bestSeller.length}
                    renderItem={this.combosListing}
                  />
                </SafeAreaView>
              </View>}
              {this.groupListing()}
            </Content>

          ) : (
              this.notfound()
            )}
        </ScrollView>
        <Footer navigation={navigation} active="home" />
      </Container>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    combos: state.menu.menuCombosData,
    topDeals: state.menu.topDeals,
    heroItems: state.menu.heroItems,
    groups: state.menu.homeGroupsData,
    selectedStore: state.customer.selectedStore,
    cart: state.cart.cartData,
    notifications: state.customer.notificationList,
    deliveryaddress: state.customer.deliveryaddress,
    loading: state.menu.loading
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    menuCombosList: function () {
      dispatch(menuCombosList());
    },
    favouriteAdd: function (param1: any, param2: any) {
      dispatch(favouriteAdd(param1));
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
