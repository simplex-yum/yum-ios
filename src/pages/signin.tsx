import React from 'react';
import styles from '../assets/css/signin';
import { ActivityIndicator, Alert, Linking, TouchableOpacity, View } from 'react-native';
import Topbar from '../components/Topbar/simpleTopbar';
import { Container, Button, Text, Content, Form, Item, Input } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { loginCustomer } from '../redux';
// import {
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
//   LoginManager,
// } from 'react-native-fbsdk';
// import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { SigninState, SigninProps } from '../interfaces/customer';
import FastImage from 'react-native-fast-image';

class SignIn extends React.PureComponent<SigninProps, SigninState> {
  constructor(props: any) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: true,
      visible: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.getInfoFromToken = this.getInfoFromToken.bind(this);
    // this.loginWithFacebook = this.loginWithFacebook.bind(this);
    this.setVisibility = this.setVisibility.bind(this);
    this.renderScreen = this.renderScreen.bind(this);
    this.signIn = this.signIn.bind(this);
  }
  componentDidMount() {
    //initial configuration
    // GoogleSignin.configure({
    //   //It is mandatory to call this method before attempting to call signIn()
    //   scopes: ['https://www.googleapis.com/auth/drive.readonly'],
    //   // Repleace with your webClientId generated from Firebase console
    //   webClientId:
    //     // '290697471358-etu6385sqjnlfhe2oh62je0g2v92rim2.apps.googleusercontent.com',  //webClientId for Simplex Yum
    //     '709322135566-9bfro8e2f74ipth5oaqe0ku20as5gb59.apps.googleusercontent.com', // webClientId for K2g

    //   offlineAccess: true,
    // });
    // //Check if user is already signed in
    this._isSignedIn();
  }
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return;
    };
  }
  renderScreen(screen: any) {
    this.props.navigation.navigate(screen);
  }
  //google login Methods
  _isSignedIn = async () => {
    // const isSignedIn = await GoogleSignin.isSignedIn();
    // if (isSignedIn) {
    //   //Get the User details as user is already signed in
    //   this._getCurrentUserInfo();
    // } else {
    //   // console.log('Please Login');
    // }
    this.setState({ loading: false });
  };
//   _getCurrentUserInfo = async () => {
//     try {
//       const userInfo = await GoogleSignin.signInSilently();
//       // console.log('User Info --> ', userInfo);
//       const data: any = {
//         login_name: userInfo.user.name,
//         email_address: userInfo.user.email,
//         profile_pic: userInfo.user.photo,
//         is_active: 1,
//       };
//       this.props.loginCustomerWithSocial(data, this.props.navigation);
//     } catch (error) {
//       if (error.code === statusCodes.SIGN_IN_REQUIRED) {
//         // console.log('User has not signed in yet');
//       } else {
//         // console.log("Something went wrong. Unable to get user's info");
//       }
//     }
//   };
//   _signIn = async () => {
//     //Prompts a modal to let the user sign in into your application.
//     try {
//       await GoogleSignin.hasPlayServices({
//         //Check if device has Google Play Services installed.
//         //Always resolves to true on iOS.
//         showPlayServicesUpdateDialog: true,
//       });
//       let userInfo: any = {};
//       try {
//         userInfo = await GoogleSignin.signIn();
//       } catch (e) {
//         console.log(e)
//       }
//       // console.log('User Info --> ', userInfo);
//       const min = 1;
//       const max = 100;
//       const rand = min + Math.random() * (max - min);
//       const data: any = {
//         login_name: userInfo.user.name,
//         email_address: userInfo.user.email,
//         profile_pic: userInfo.user.photo,
//         phone_number: rand,
//         is_active: 1,
//       };
//       this.props.loginCustomerWithSocial(data, this.props.navigation);
//     } catch (error) {
//       // console.log('Message', error.message);
//       if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//         // console.log('User Cancelled the Login Flow');
//       } else if (error.code === statusCodes.IN_PROGRESS) {
//         // console.log('Signing In');
//       } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//         // console.log('Play Services Not Available or Outdated');
//       } else {
//         // console.log('Some Other Error Happened');
//       }
//     }
//   };
  //Facebook Login Methods
//   getInfoFromToken(accessToken: any) {
//     const PROFILE_REQUEST_PARAMS = {
//       fields: {
//         string: 'id,name,email,first_name,last_name,picture.type(small)',
//       },
//     };
//     const profileRequest = new GraphRequest(
//       '/me',
//       { accessToken, parameters: PROFILE_REQUEST_PARAMS },
//       (error: any, user: any) => {
//         if (error) {
//           // console.log('login info has error: ' + JSON.stringify(error));
//         } else {
//           // console.log(user)
//           const min = 1;
//           const max = 100;
//           const rand = min + Math.random() * (max - min);
//           const data: any = {
//             first_name: user.first_name,
//             last_name: user.last_name,
//             login_name: user.name,
//             email_address: user.email,
//             profile_pic: user.picture.data.url,
//             phone_number: rand,
//             is_active: 1,
//           };
//           // console.log(data)
//           this.props.loginCustomerWithSocial(data, this.props.navigation);
//         }
//       },
//     );
//     new GraphRequestManager().addRequest(profileRequest).start();
//   };

//   loginWithFacebook() {
//     // Attempt a login using the Facebook login dialog asking for default permissions.
//     LoginManager.logInWithPermissions(['public_profile', 'email']).then(
//       (login) => {
//         if (login.isCancelled) {
//           // console.log('Login cancelled');
//         } else {
//           AccessToken.getCurrentAccessToken().then((data: any) => {
//             const accessToken = data.accessToken.toString();
//             this.getInfoFromToken(accessToken);
//           });
//         }
//       },
//       (error) => {
//         // console.log('Login fail with error: ' + error);
//       },
//     );
//   };
  ////
  setVisibility() {
    this.setState({ visible: !this.state.visible });
  }
  handleSubmit() {
    let { email, password } = this.state;
    this.props.loginCustomer(email, password, this.props.navigation);
  };

  signIn() {
    const icon = this.state.visible ? 'eye' : 'eye-slash';
    return (
      <Grid>
        <Row>
          <Col style={styles.group1}>
            <FastImage
              source={require('../assets/images/logok2g.png')}
              style={{ width: 200, height: 200 }}
              resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.welcomeBac}>Welcome Back</Text>
            <Text style={styles.signintoco}>Sign in to Continue</Text>
          </Col>
        </Row>
        <Row>
          <Col style={styles.group2}>
            <Form>
              <Item>
                <Input
                  autoCapitalize="none"
                  placeholder="Email"
                  value={this.state.email}
                  onChangeText={(email) => this.setState({ email })}
                />
              </Item>
              <Item>
                <Input
                  autoCapitalize="none"
                  secureTextEntry={!this.state.visible}
                  value={this.state.password}
                  placeholder="Password"
                  onChangeText={(password) => this.setState({ password })}
                />
                <Icon
                  name={icon}
                  size={20}
                  onPress={() => this.setVisibility()}></Icon>
              </Item>
            </Form>
            <Text
              style={styles.forgotPass}
              onPress={() => this.renderScreen("forgotpass")}>
              Forgot Password
            </Text>
          </Col>
        </Row>
        <Row>
          <Col style={styles.group3}>
            <Button
              block
              style={styles.SignInButton}
              onPress={this.handleSubmit}>
              <Text style={styles.signinText}>SIGN IN</Text>
            </Button>
            {/* <Button
              block
              style={styles.FacebookButton}
              onPress={this.loginWithFacebook}>
              <Icon name="facebook" color="#fff" size={20} />
              <Text style={styles.signinText}>CONTINUE WITH FACEBOOK</Text>
            </Button>
            <Button block style={styles.GoogleButton} onPress={this._signIn}>
              <Icon name="google" color="#bc2c3d" size={20} />
              <Text style={styles.googleText}>SIGN IN WITH GMAIL</Text>
            </Button> */}
            <Text style={styles.donthavean}>
              Don't have an account?{' '}
              <TouchableOpacity onPress={() => this.renderScreen("signup")}>
                <Text style={styles.linkText}> Sign Up</Text>
              </TouchableOpacity>
            </Text>
            <Text style={styles.byloggingin}>
              By Signing in,you agree with the{' '}
              {/* <Text style={styles.linkText}>Terms and Service</Text>, */}
              <TouchableOpacity onPress={() => Linking.openURL('https://www.krispy2go.com/privacy-policy')}><Text style={styles.linkText}>Privacy Policy & Content Policy</Text></TouchableOpacity>
              {/* <Text style={styles.linkText}></Text>. */}
            </Text>
          </Col>
        </Row>
      </Grid>
    );
  };
  render() {
    return (
      <Container>
        {this.state.loading &&
          <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
            <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
          </View>
        }
        <Topbar navigation={this.props.navigation} />
        <Content padder>{this.signIn()}</Content>
      </Container>
    );
  }
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    loginCustomer: function (email: any, password: any, navigation: any) {
      dispatch(loginCustomer(email, password, navigation));
    },
    // loginCustomerWithSocial: function (data: any, navigation: any) {
    //   dispatch(loginCustomerWithSocial(data, navigation));
    // },
  };
};
export default connect(null, mapDispatchToProps)(SignIn);
