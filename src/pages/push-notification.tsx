import React from 'react';
import { Container, Row, Col, Text, List, ListItem, Left, Icon, Body } from 'native-base';
import Topbar from '../components/Topbar/titleTopbar';
import Footer from '../components/footer';
import { connect } from 'react-redux';
import { NotificationList } from '../redux';
import styles from '../assets/css/notification'
import { FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';

class PushNotification extends React.Component<{ notifications: any, navigation: any, NotificationList: () => {} }, {}> {
    constructor(props: any) {
        super(props);
        this.isEmpty = this.isEmpty.bind(this);
        this.itemsListing = this.itemsListing.bind(this);
    }
    isEmpty(obj: any) {
        if (obj === null) {
            return true;
        }
        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj && Object.keys(obj).length && Object.keys(obj).length > 0) {
            return false;
        }
        if (obj && Object.keys(obj).length === 0) {
            return true;
        }
        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and toValue enumeration bugs in IE < 9
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    itemsListing({ item }: any) {
        let pushData: any = item && JSON.parse(item.notification_data);
        return (
            <Row>
                <Col>
                    <List>
                        <ListItem avatar key={item.id}>
                            <Left>
                                <Icon
                                    name='exclamation-circle'
                                    type="FontAwesome"
                                />
                            </Left>
                            <Body>
                                <Text style={styles.title}>{pushData.title && pushData.title}</Text>
                                <Text style={styles.body}>{pushData.body && pushData.body}</Text>
                            </Body>
                        </ListItem>
                    </List>
                </Col>
            </Row>

        )
    }
    render() {
        let { notifications, navigation } = this.props;
        return (
            <Container>
                <Topbar navigation={navigation} title="Notification" />
                <FlatList
                    data={notifications}
                    renderItem={this.itemsListing}
                    keyExtractor={(item, index) => index.toString()}
                    ListEmptyComponent={
                        <Row><Col style={styles.group1}>
                            <FastImage
                                style={styles.foodImage}
                                source={require('../assets/images/food.png')}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={styles.noOrderAva}>No Notifications</Text>
                            <Text style={styles.goodfoodis}>
                                It seems you have no notificatins yet.Go ahead, order some yummy food
                                  from the menu.</Text>
                        </Col>
                        </Row>
                    }
                />

                <Footer active="" />
            </Container>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        notifications: state.customer.notificationList
    };
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        NotificationList: function () {
            dispatch(NotificationList());
        }
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(PushNotification);