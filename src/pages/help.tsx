import React from 'react';
import Topbar from '../components/Topbar/notificationTopbar';
import {
    Container,
    Text,
    Content
} from 'native-base';
import { Col, Row } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { ActivityIndicator, View } from 'react-native';

class Help extends React.PureComponent<
    { loading: any,navigation:any },
    {}
    > {
    constructor(props: any) {
        super(props);
    }

    render() {
        let { loading ,navigation} = this.props;
        return (
            <Container>
                {loading &&
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', zIndex: 500, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator size={50} color="#c00a27"></ActivityIndicator>
                        <Text style={{ fontSize: 20, color: "white", marginTop: 10 }}>Loading</Text>
                    </View>
                }
                <Topbar navigation={navigation} title="Help" tabs="" />
                <Content padder>
                    <Row>
                        <Col>
                        <Text style={{fontWeight:'bold'}}>How does K2G crew facilitate me?</Text>
                        <Text style={{padding:5}}>The team of every K2G restaurant is sociable, friendly and devoted to serve you the best with can do attitude to ensure 100% satisfaction of all K2G customers.</Text>
                        <Text style={{fontWeight:'bold'}}>What’s the cleanliness and hygiene standard at K2G restaurants?</Text>
                        <Text style={{padding:5}}>K2G genuinely cares for your health and safety. We make sure to keep our restaurants’ kitchen, floor, furniture and washrooms clean and protected by frequent mopping and cleaning done by our efficient team.</Text>
                        <Text style={{fontWeight:'bold'}}>Do I have an option to celebrate a thematic birthday party?</Text>
                        <Text style={{padding:5}}>It is possible to celebrate your birthday party according to an on-going theme at K2G.</Text>
                        <Text style={{padding:5}}>K2G offers an exciting Thematic birthday party along with Thematic gifts for Birthday Kid, Participants &amp; Game Winners.</Text>
                        <Text style={{fontWeight:'bold'}}>How to get a job and be a part of K2G team?</Text>
                        <Text style={{padding:5}}>K2G is all about passion and we always welcome people in our team who consider themselves as food passionist and have customer centric attitude. We provide training to our employees for all internationally accredited standards for F&amp;B industry and we consider them as one of the most important asset. People willing to join our passionate team can submit their profile on "info@krispy2go.com".</Text>
                        <Text style={{fontWeight:'bold'}}>How can I make my child’s birthday a memorable one?</Text>
                        <Text style={{padding:5}}>Make your child’s birthday a memory to last forever by celebrating at K2G. Apart from standard packages we can do any customization to make your event memorable.</Text>
                        <Text style={{fontWeight:'bold'}}>Is K2G food halal?</Text>
                        <Text style={{padding:5}}>K2G Pakistan gives you 100% assurance of providing Halal and pure food and it is one of the fundamental principle of K2G Brand.</Text>
                        </Col>
                    </Row>
                </Content>
            </Container>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        loading: state.customer.loading
    };
};
export default connect(mapStateToProps)(Help);
